/*********************************************************
 *                                                       *
 *  Advanced Ring Buffer Redundant Application for CAN   *
 *                                                       *
 *********************************************************
 *                      ArbraCan.c                       *
 *********************************************************
 * This program is free software; you can redistribute   *
 * it and/or modify it under the terms of the GNU General*
 * Public License as published by the Free Software      *
 * Foundation; either version 2 of the License, or (at   *
 * your option) any later version.                       *
 *                                                       *
 * This program is distributed in the hope that it will  *
 * be useful, but WITHOUT ANY WARRANTY; without even the *
 * implied warranty of MERCHANTABILITY or FITNESS FOR A  *
 * PARTICULAR PURPOSE.  See the GNU General Public       *
 * License for more details.                             *
 *                                                       *
 * You should have received a copy of the GNU General    *
 * Public License along with this program; if not, write *
 * to 	The Free Software Foundation, Inc.               *
 *	675 Mass Ave                                     *
 *	Cambridge                                        *
 *	MA 02139                                         *
 * 	USA.                                             *
 *********************************************************
 *                                                       *
 *      Author: Edouard TISSERANT                        *
 *      Contact: edouard.tisserant@esstin.u-nancy.fr     *
 *      Version: 1.0                                     *
 *      Modification date:                               *
 *      Description:                                     *
 *-------------------------------------------------------*
 * This is the main functions of the ArbraCan layer      *
 *                                                       *
 *********************************************************/

#define ArbraCan_C
//
// Kernel includes
//
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/ioport.h>
#include <linux/fs.h>
//#include <linux/pci.h>
#include <asm/uaccess.h>
#include <asm/irq.h>
#include <asm/io.h>


#ifdef __RTL__
#include <rtl_sched.h>
#include <rtl_posixio.h>
#include <rtl_mutex.h>
#endif

//
// Other Includes
//
#include "Can.h"
#include "ArbraCanHW.h"
#include "ArbraCan.h"

//
// Compilation parameters
//
#define DEBUG
#undef DEBUG

//
// module parameters and descriptions
//
static char *canboard_name = NULL;
MODULE_PARM(canboard_name, "s");
MODULE_PARM_DESC(canboard_name, "Name of device");

static int can_rate=1000;
MODULE_PARM(can_rate, "1i");
MODULE_PARM_DESC(can_rate, "Speed (Kbauds)");

static int can_silent=0;
MODULE_PARM(can_silent, "1i");
MODULE_PARM_DESC(can_silent, "Be invisble on this bus");

static int can_nosort=0;
MODULE_PARM(can_nosort, "1i");
MODULE_PARM_DESC(can_nosort, "Don't sort paquet to respect CAL");

#ifdef DEBUG
static int can_debug=1;
MODULE_PARM(can_debug, "1i");
MODULE_PARM_DESC(can_debug, "Enable debugging messages");
#endif

MODULE_DESCRIPTION("canboard driver");
MODULE_AUTHOR("e.tisserant@parvex.com");

MODULE_LICENSE("GPL");
//
// Debugging Macros
//
#ifdef DEBUG
#define MSG(string, args...) /*if Debug_Cond*/ {printk("%s:%d:",__FILE__,__LINE__);printk(string, ##args);}
#else
#define MSG(string, args...)
#endif

#define Debug_Cond ((1<<bus_nb) & can_debug)

//
// Globals vars.
//
// IRQ Reminder
static int can_irq0 = 0;
static int can_irq1 = 0;

// Openner tables (because of Linux and RTLinux cohabitation)
#define NOT_OPENED 0
#define OPENED_USER 1
#define OPENED_RTL 2
int openner[CAN_BUS_COUNT] = {[0 ... CAN_BUS_COUNT-1] = NOT_OPENED};

// Half bus Mutex's
struct {
	spinlock_t spin;
	unsigned long irq_state;
	} bus_lock[CAN_BUS_COUNT][2] = {[0 ... CAN_BUS_COUNT-1]= { [0 ... 1] = {SPIN_LOCK_UNLOCKED, 0}}};

// Wait queues.
union dbl_wq{
	wait_queue_head_t l;
#ifdef __RTL__
	rtl_wait_t rt;
#endif
} can_wq[CAN_BUS_COUNT][2];

// Communication Object Priority stuff
const int COB_P[] = {
	0,	// 0-220 -> CMS obj priority 0
	1,	// 221-440 -> CMS obj priority 1
	2,
	3,
	4,
	5,
	6,
	7,	// 1541-1760 -> CMS obj priority 7
	7,	// 1760-
	7	//     -2200 -> NMT Node Guarding, LMT, DBT (arbitrary set to 7)
};

inline int abs(int a) {return a < 0 ? -a : a ;};
#define GET_PRIO(m) ((1 << bus_nb) & can_nosort) ? 0 : COB_P[abs((m).cob_id.w - 1) / 220]

// Buffer stuff
static Message x_buff[2][CAN_BUS_COUNT][PRIORITY_LEVELS][MESS_BUFF_SIZE];
static int x_buff_begin[2][CAN_BUS_COUNT][PRIORITY_LEVELS]={[0 ... 1]= {[0 ... CAN_BUS_COUNT-1]= {[0 ... PRIORITY_LEVELS-1] = 0}}};
static int x_buff_count[2][CAN_BUS_COUNT][PRIORITY_LEVELS]={[0 ... 1]= {[0 ... CAN_BUS_COUNT-1]= {[0 ... PRIORITY_LEVELS-1] = 0}}};

//
// OS dependent Declarations
//
#ifdef __RTL__		/* Rtlinux version of the driver*/

// we have to declare a special rtl_wait_sleep, where irq are not allowed, to be sure to catch irq when we need it.
int rtl_wait_sleep_irqrestore (rtl_wait_t *wait, spinlock_t *lock, unsigned long irq_state)
{
	pthread_t self = pthread_self();
	struct rtl_wait_struct wait_struct;

	self->abort = 0;
	self->abortdata = 0;

	wait_struct.magic = RTL_WAIT_MAGIC;
	wait_struct.waiter = self;
	wait_struct.next = wait->queue;
	wait->queue = &wait_struct;
	wait->p_lock = lock;

	RTL_MARK_SUSPENDED (self);

	rtl_spin_unlock_irqrestore(lock, irq_state);

	return rtl_schedule();
}

#define wait_event_rt(wq, condition)\
	do{\
		unsigned long irq_state;\
		rtl_spin_lock_irqsave(wq->p_lock, irq_state);\
		if(!condition){\
			do{\
				rtl_wait_sleep_irqrestore(wq, wq->p_lock, irq_state);\
				rtl_spin_lock_irqsave(wq->p_lock, irq_state);\
			}while(!condition);\
		}\
		rtl_spin_unlock_irqrestore(wq->p_lock, irq_state);\
	}while(0)

inline int Bus_Lock(int bus_nb, int dir){
	if(openner[bus_nb]==OPENED_RTL){
		rtl_spin_lock_irqsave(&(bus_lock[bus_nb][dir].spin), bus_lock[bus_nb][dir].irq_state);
		return 1;
		/*return rtl_spin_trylock(&(bus_lock[bus_nb][dir].spin));*/
	}else{
		spin_lock_irqsave(&(bus_lock[bus_nb][dir].spin), bus_lock[bus_nb][dir].irq_state);
		return 1;
	}
}

/*		rtl_spin_unlock(&(bus_lock[bus_nb][dir].spin));\*/
#define Bus_Unlock(bus_nb, dir)\
	if(openner[bus_nb]==OPENED_RTL){\
		rtl_spin_unlock_irqrestore(&(bus_lock[bus_nb][dir].spin), bus_lock[bus_nb][dir].irq_state);\
	}else{\
		spin_unlock_irqrestore(&(bus_lock[bus_nb][dir].spin), bus_lock[bus_nb][dir].irq_state);\
	}


// Wait For Event
#define Wait_For_Event(bus_nb, dir, condition)\
	if(openner[bus_nb]==OPENED_RTL){\
		wait_event_rt((&(can_wq[bus_nb][dir].rt)), condition);\
	}else{\
		wait_event_interruptible(can_wq[bus_nb][dir].l, condition);\
	}

#define Caught_Signal\
	openner[bus_nb]==OPENED_USER && signal_pending(current)

#define Wake_Up(bus_nb, dir)\
	if(openner[bus_nb]==OPENED_RTL){\
		rtl_wait_wakeup(&(can_wq[bus_nb][dir].rt));\
		rtl_schedule();\
	}else{\
		wake_up(&(can_wq[bus_nb][dir].l));\
	}

//IRQ stuff
#define OS_enable_irq(...) rtl_hard_enable_irq(__VA_ARGS__)
#define OS_disable_irq(...) rtl_hard_disable_irq(__VA_ARGS__)
static struct sigaction oldact;
inline int rtl_request_irq_sig(int irq,void (*function)(int)){
	struct sigaction act;
	act.sa_handler = function;
	act.sa_flags = SA_FOCUS;
	act.sa_focus = 1 << rtl_getcpuid();
	return sigaction (RTL_SIGIRQMIN + irq, &act, &oldact);
}
#define OS_request_irq(irq, function, ...) rtl_request_irq_sig(irq, function)
inline int rtl_free_irq_sig(int irq){
	return sigaction (RTL_SIGIRQMIN + irq, &oldact, NULL);
}
#define OS_free_irq(irq) rtl_free_irq_sig(irq)

#else			/* Normal Linux Kernel Driver*/

inline int Bus_Lock(int bus_nb, int dir){
	spin_lock_irqsave(&(bus_lock[bus_nb][dir].spin), bus_lock[bus_nb][dir].irq_state);
	return 1;
}
#define Bus_Unlock(bus_nb, dir)\
	spin_unlock_irqrestore(&(bus_lock[bus_nb][dir].spin), bus_lock[bus_nb][dir].irq_state)

// Wait For Event
#define Wait_For_Event(bus_nb, dir, condition)\
	wait_event_interruptible(can_wq[bus_nb][dir].l, condition)

#define Caught_Signal\
	signal_pending(current)

#define Wake_Up(bus_nb, dir)\
	wake_up(&(can_wq[bus_nb][dir].l))

//IRQ stuff
#define OS_enable_irq(...)
#define OS_disable_irq(...)
#define OS_request_irq(irq, function, ...) request_irq(irq, function,__VA_ARGS__)
#define OS_free_irq(irq) free_irq(irq,NULL)

#endif

//
// Set of inline functions to get buffers states
//
inline int Sum(int n, int *chose){
        int i;
        int t=0;
        for(i = 0; i < n; i++){
                t+=chose[i];
        }
        return t;
}

inline int Seat_Left_In_x(int dir, int bus_nb, int prio){
	int res;
	if(Bus_Lock(bus_nb, dir)){
		res = (x_buff_count[dir][bus_nb][prio] < MESS_BUFF_SIZE);
		Bus_Unlock(bus_nb, dir);
	} else {
		res = 0;
	}
	return res;
}

inline int Is_Out_Of_x(int dir, int bus_nb, int prio, int place){
	int res;
	if(Bus_Lock(bus_nb, dir)){
		res = (x_buff_begin[dir][bus_nb][prio] == ((place + 1) % MESS_BUFF_SIZE));
		Bus_Unlock(bus_nb, dir);
	} else {
		res = 0;
	}
	return res;
}

inline int Message_Pending(int dir, int bus_nb){
	int res;
	if(Bus_Lock(bus_nb, dir)){
		res = Sum(PRIORITY_LEVELS, x_buff_count[dir][bus_nb])>0;
		Bus_Unlock(bus_nb, dir);
	} else {
		res = 0;
	}
	return res;
}

//
// Put the message in the Tx Ring buffer and return where
// he have been placed if so.
//
int Put_In_x(int dir, int bus_nb, Message *arg)
{
	int prio = GET_PRIO(*arg);
	MSG("Entering Put_In_x(%d,%d,%d)\n", bus_nb, dir, prio);

	if(x_buff_count[dir][bus_nb][prio] < MESS_BUFF_SIZE) {
		int place = (x_buff_begin[dir][bus_nb][prio] + x_buff_count[dir][bus_nb][prio]++) % MESS_BUFF_SIZE;
		MSG("Prio: %d\n", prio);
		memcpy(
			&x_buff[dir][bus_nb][prio][place],
			(void *)arg, sizeof(Message));
		MSG("Exiting (success) Put_In_x\n");
		return place;
	}
	MSG("Exiting (fault) Put_In_x\n");
	return -EFAULT;
}

//
// Get the most important message (lowest priority number)
//
int Get_From_x(int dir, int bus_nb, Message *arg)
{
	int prio;
	MSG("Entering Get_From_x(%d,%d)\n", bus_nb, dir);

	// Seek to non-empty buffer
	for(prio=0;prio < PRIORITY_LEVELS ; prio++){
		if(x_buff_count[dir][bus_nb][prio] > 0){
			MSG("Prio: %d\n", prio);
			memcpy(
				arg,
				&x_buff[dir][bus_nb][prio][x_buff_begin[dir][bus_nb][prio]++],
				sizeof(Message));
			x_buff_begin[dir][bus_nb][prio] = x_buff_begin[dir][bus_nb][prio] % MESS_BUFF_SIZE;
			x_buff_count[dir][bus_nb][prio]--;
			MSG("Exiting (success) Get_From_x\n");
			return 0;
		}
	}
	MSG("Exiting (fault) Get_From_x\n");
	return -EFAULT;
}

//
// Put_In_x but get bus lock before and free it after
//
int Put_In_x_Lock(int dir, int bus_nb, Message *arg)
{
	int res;
	MSG("Entering Put_In_x_Lock(%d,%d)\n", bus_nb, dir);
	if(Bus_Lock(bus_nb, dir)){
		res = Put_In_x(dir, bus_nb, arg);
		Bus_Unlock(bus_nb, dir);
	} else {
		res = -EAGAIN ;
	}
	return res;
}

int Clear_x_Buff(int dir, int bus_nb)
{
	int prio;
	MSG("Entering Clear_x_Buff(%d,%d)\n", bus_nb, dir);

	for(prio=0;prio < PRIORITY_LEVELS ; prio++){
		x_buff_begin[dir][bus_nb][prio]=0;
		x_buff_count[dir][bus_nb][prio]=0;
	}
	MSG("Exiting (ok) Cleat_x_Buff\n");
	return 0;
}

int Clear_x_Buff_Lock(int dir, int bus_nb)
{
	int res;
	MSG("Entering Clear_x_Buff_Lock(%d,%d)\n", bus_nb, dir);
	if(Bus_Lock(bus_nb, dir)){
		res=Clear_x_Buff(dir, bus_nb);
		Bus_Unlock(bus_nb, dir);
	} else {
		res = -EAGAIN ;
	}
	return res;
}



//
// Get_From_x but get bus lock before and free it after
//
int Get_From_x_Lock(int dir, int bus_nb, Message *arg)
{
	int res;
	MSG("Entering Get_From_x_Lock(%d,%d)\n", bus_nb, dir);
	if(Bus_Lock(bus_nb, dir)){
		res = Get_From_x(dir, bus_nb, arg);
		Bus_Unlock(bus_nb, dir);
	} else {
		res = -EAGAIN ;
	}
	return res;
}

//
// Get from Rx buffer of controller
// and put it in the fifo
//
void Update_Board_Rx(int bus_nb)
{
	Message m;
	memset(&m,0,sizeof(m));
	if(Bus_Lock(bus_nb, Rx)){
		MSG("Update_Board_Rx(%d)\n",bus_nb);
		while(CAN_RX_MSG_READY(bus_nb)){
			CAN_GET(bus_nb, &m);
			Put_In_x(Rx, bus_nb,&m);
			Wake_Up(bus_nb, Rx);
		}
		Bus_Unlock(bus_nb, Rx);
		MSG("Fin Update_Board_Rx(%d)\n",bus_nb);
	}
}

//
// Get a message in the fifo and
// put it in Tx buffer of controller
//
void Update_Board_Tx(int bus_nb)
{
	Message m;

	if(Bus_Lock(bus_nb, Tx)){
		MSG("Update_Board_Tx(%d)\n",bus_nb);
		if(CAN_TX_BUFF_FREE(bus_nb)){ /* && CAN_TX_FREE(bus_nb)*/
			if(!Get_From_x(Tx, bus_nb, &m)){
				CAN_PUT(bus_nb, &m);
				Wake_Up(bus_nb, Tx);
			}
		}
		Bus_Unlock(bus_nb, Tx);
		MSG("Fin Update_Board_Tx(%d)\n",bus_nb);
	}
}

//
// The read function (non blocking)
//
static int canboard_read(int bus_nb, Message *m)
{
	MSG("CANBOARD_READ (%d)\n",bus_nb);
	Update_Board_Rx(bus_nb);
	if(Get_From_x_Lock(Rx, bus_nb, m)){
		return -EFAULT;		// No more seat left
	}
	return 0;
}

//
// The read function (blocking)
//
static int canboard_read_block(int bus_nb, Message *m)
{
	MSG("CANBOARD_READ_BLOCK (%d)\n",bus_nb);
	// Warning loop undefinitely when it can not get lock
	while(Get_From_x_Lock(Rx, bus_nb, m)){
		MSG("CANBOARD_READ_BLOCK (%d) -- dodo\n",bus_nb);
		Wait_For_Event(bus_nb, Rx, Message_Pending(Rx, bus_nb));
		MSG("CANBOARD_READ_BLOCK (%d) -- reveil\n",bus_nb);
		if(Caught_Signal)
			return -EFAULT;
		Update_Board_Rx(bus_nb);
	}
	return 0;
}

//
// The write function (non blocking)
//
static int canboard_write(int bus_nb, Message *m)
{
	MSG("CANBOARD_WRITE (%d)\n",bus_nb);
	if(Put_In_x_Lock(Tx, bus_nb, m) < 0){
		return -EFAULT;
	}
	Update_Board_Tx(bus_nb);
	return 0;
}

//
// The write function (blocking)
//
static int canboard_write_block(int bus_nb, Message *m)
{
	int place;
	int prio;
	MSG("CANBOARD_WRITE_BLOCK (%d)\n",bus_nb);
	prio = GET_PRIO(*m);
	while(((place = Put_In_x_Lock(Tx, bus_nb, m)) <0)){
		MSG("CANBOARD_WRITE_BLOCK (%d) -- dodo\n",bus_nb);
		// block if no seat left in FIFO
		Wait_For_Event(bus_nb, Tx, Seat_Left_In_x(Tx, bus_nb, prio));
		MSG("CANBOARD_WRITE_BLOCK (%d) -- reveil\n",bus_nb);
		if(Caught_Signal)
			return -EFAULT;
		Update_Board_Tx(bus_nb);
	}

	Update_Board_Tx(bus_nb);

	// block while not send
	MSG("CANBOARD_WRITE_BLOCK (%d) -- dodo (wait to be sent)\n", bus_nb);
	Wait_For_Event(bus_nb, Tx, Is_Out_Of_x(Tx, bus_nb, prio, place));
	MSG("CANBOARD_WRITE_BLOCK (%d) -- reveil\n",bus_nb);

	return 0;
}

//
// The status function
// (if someone want to use it...)
//
static int canboard_status(int bus_nb)
{
	return CAN_STATUS(bus_nb);
}

static int canboard_set_baudrate(int bus_nb, int baudrate)
{
	return sja1000_set_baudrate(bus_nb, baudrate);
}

static int canboard_command(int bus_nb, int command)
{
	return sja1000_command(bus_nb, command);
}

static int canboard_set_acceptance_filter(int bus_nb, AcceptanceFilter*accFilter)
{
	return sja1000_set_acceptance_filter(bus_nb,
					     accFilter->acceptanceCode,
					     accFilter->acceptanceMask);
}

//
// The open function
//
static int canboard_open(int bus_nb)
{
	MSG("canboard_open\n");

	/* increment usage count */
	MOD_INC_USE_COUNT;

	if((1 << bus_nb) & can_silent){
		CAN_INIT_SILENT(bus_nb, can_rate);
	} else {
		CAN_INIT(bus_nb, can_rate);
	}

	return 0;
}


//
// The close function
//
static int canboard_close(int bus_nb)
{
	MSG("canboard_close\n");

	MOD_DEC_USE_COUNT;

	openner[bus_nb]=NOT_OPENED;

	CAN_UNINIT(bus_nb);

	return 0;
}

//
// Linux POSIX registered functions
//
// IOCTL
static int canboard_ioctl_linux(struct inode *inode, struct file *file,
		       unsigned int cmd, unsigned long arg)
{
	int bus_nb = MINOR(inode->i_rdev);
	Message m;
	int baudrate;
	AcceptanceFilter accFilter;

	if(bus_nb >= CAN_BUS_COUNT)
		return -ENOTTY;

	/* make sure that the command is really one of canboard's */
	if (_IOC_TYPE(cmd) != CANBOARD_IOCTL_BASE)
		return -ENOTTY;
	
	switch (cmd) {
	case CANBOARD_READ_BLOCK: {
		int res = canboard_read_block(bus_nb, &m);
		copy_to_user((void*) arg, &m, sizeof(Message));
		return res;
	}
		
	case CANBOARD_READ: {
		int res = canboard_read(bus_nb, &m);
		copy_to_user((void*) arg, &m, sizeof(Message));
		return res;
	}
		
	case CANBOARD_WRITE_BLOCK: {
		copy_from_user(&m,(void *)arg, sizeof(Message));
		m.cob_id.w &= 0x07ff; //Filter for crazy users
		return canboard_write_block(bus_nb, &m);
	}

	case CANBOARD_WRITE: {
		copy_from_user(&m,(void *)arg, sizeof(Message));
		m.cob_id.w &= 0x07ff; //Filter for crazy users
		return canboard_write(bus_nb, &m);
	}

	case CANBOARD_STATUS: {
		int status = canboard_status(bus_nb);
		put_user(status,(int *)arg);
		return 0;
	}
	case CANBOARD_GET_CONTROL: {
		int status = sja1000_get_control(bus_nb);
		put_user(status,(int *)arg);
		return 0;
	}
	case CANBOARD_SET_CONTROL: {
		int ctl;
		get_user(ctl,(int *)arg);
		return sja1000_set_control(bus_nb,ctl);
		return 0;
	}
	case CANBOARD_COMMAND: {
		int command;
		get_user(command,(int *)arg);
		return canboard_command(bus_nb,command);
		return 0;
	}
	case CANBOARD_SET_BAUDRATE: {
		copy_from_user(&baudrate,(void *)arg, sizeof(int));
		return canboard_set_baudrate(bus_nb, baudrate);
		return 0;
	}
	case CANBOARD_SET_ACCEPTANCE_FILTER: {
		copy_from_user(&accFilter,(void *)arg,
			       sizeof(AcceptanceFilter));
		return canboard_set_acceptance_filter(bus_nb, &accFilter);
		return 0;
	}
	case CANBOARD_CLEAR_TX_BUFFER: {
		Clear_x_Buff_Lock(Tx,bus_nb);
		return 0;
	}
	case CANBOARD_CLEAR_RX_BUFFER: {
		Clear_x_Buff_Lock(Rx,bus_nb);
		return 0;
	}
	default: {
		return -ENOTTY;
	}
	}

	/* to keep gcc happy */
	return 0;
}


// OPEN
static int canboard_open_linux(struct inode *inode, struct file *file)
{
	int bus_nb = MINOR(inode->i_rdev);

	// Because of the new syntax of kernel
	// we have to declare an init a wait queue.
	// and copy it to our wait table.

	if(openner[bus_nb]==NOT_OPENED)
	{
		init_waitqueue_head(&can_wq[bus_nb][Rx].l);
		init_waitqueue_head(&can_wq[bus_nb][Tx].l);

		openner[bus_nb]=OPENED_USER;

		return canboard_open(bus_nb);
	}
	return -EFAULT;
}

// CLOSE
static int canboard_close_linux(struct inode *inode, struct file *file)
{
	int bus_nb = MINOR(inode->i_rdev);
	return canboard_close(bus_nb);
}

// FOPS struct for registration
static struct file_operations canboard_fops = {
	ioctl: canboard_ioctl_linux,
	open: canboard_open_linux,
	release: canboard_close_linux
};

#ifdef __RTL__		/* Rtlinux version of the driver*/
//
// RTLinux POSIX registered functions
//
// IOCTL
static int canboard_ioctl_rtl(struct rtl_file *rtfile, unsigned int cmd, unsigned long arg)
{
	int bus_nb = rtfile->f_minor;
	Message m;

	if(bus_nb >= CAN_BUS_COUNT)
		return -ENOTTY;

	/* make sure that the command is really one of canboard's */
	if (_IOC_TYPE(cmd) != CANBOARD_IOCTL_BASE)
		return -ENOTTY;

	switch (cmd) {
		case CANBOARD_READ_BLOCK: {
			int res = canboard_read_block(bus_nb, &m);
			memcpy((void*) arg, &m, sizeof(Message));
			return res;
		}

		case CANBOARD_READ: {
			int res = canboard_read(bus_nb, &m);
			memcpy((void*) arg, &m, sizeof(Message));
			return res;
		}

		case CANBOARD_WRITE_BLOCK: {
			memcpy(&m,(void *)arg, sizeof(Message));
			m.cob_id.w &= 0x07ff; //Filter for crazy users
			return canboard_write_block(bus_nb, &m);
		}

		case CANBOARD_WRITE: {
			memcpy(&m,(void *)arg, sizeof(Message));
			m.cob_id.w &= 0x07ff; //Filter for crazy users
			return canboard_write(bus_nb, &m);
		}

		case CANBOARD_STATUS: {
			int status = canboard_status(bus_nb);
			(int)arg = status;
			return 0;
		}
		default: {
			return -ENOTTY;
		}
	}

	/* to keep gcc happy */
	return 0;
}

// OPEN
static int canboard_open_rtl(struct rtl_file *rtfile)
{
	int bus_nb = rtfile->f_minor;

	if(openner[bus_nb]==NOT_OPENED)
	{
		rtl_wait_init(&can_wq[bus_nb][Rx].rt);
		rtl_wait_init(&can_wq[bus_nb][Tx].rt);

		openner[bus_nb] = OPENED_RTL;

		return canboard_open(bus_nb);
	}
	return -EFAULT;
}

// CLOSE
static int canboard_close_rtl(struct rtl_file *rtfile)
{
	int bus_nb = rtfile->f_minor;
	return canboard_close(bus_nb);
}

// FOPS struct for registration
struct rtl_file_operations canboard_rtl_fops = {
	ioctl: canboard_ioctl_rtl,
	open: canboard_open_rtl,
	release: canboard_close_rtl
};
#endif

//
// The IRQ Handlers
//
#ifdef __RTL__
void canboard_interrupt_10(int sig)
#else
static void canboard_interrupt_10(int irq, void *dev_id, struct pt_regs *regs)
#endif
{
	int bus_nb=0;
	char it;

//	for(bus_nb=0;bus_nb<CAN_BUS_COUNT;bus_nb++){
	it = CAN_IR(bus_nb);
	if(openner[bus_nb]!=NOT_OPENED){
		MSG("Entering interrupt 10 for (%d)\n", bus_nb);
		MSG("(%d)IR: %x\n", bus_nb,it);
		
		if(it & RECEIVE_IT)
			Update_Board_Rx(bus_nb);

		if(it & TRANSMIT_IT)
			Update_Board_Tx(bus_nb);

		MSG("Exiting interrupt\n");
	}
//	}
	OS_enable_irq(can_irq);
	#ifdef __RTL__
	pthread_kill(pthread_linux(), RTL_LINUX_MIN_SIGNAL+can_irq);
	#endif
}

#ifdef __RTL__
void canboard_interrupt_11(int sig)
#else
static void canboard_interrupt_11(int irq, void *dev_id, struct pt_regs *regs)
#endif
{
	int bus_nb=1;
	char it;

//	for(bus_nb=0;bus_nb<CAN_BUS_COUNT;bus_nb++){
	it = CAN_IR(bus_nb);
	if(openner[bus_nb]!=NOT_OPENED){
		MSG("Entering interrupt 11 for (%d)\n", bus_nb);
		MSG("(%d)IR: %x\n", bus_nb,it);

		if(it & RECEIVE_IT)
			Update_Board_Rx(bus_nb);

		if(it & TRANSMIT_IT)
			Update_Board_Tx(bus_nb);

		MSG("Exiting interrupt\n");
	}
//	}
	OS_enable_irq(can_irq);
	#ifdef __RTL__
	pthread_kill(pthread_linux(), RTL_LINUX_MIN_SIGNAL+can_irq);
	#endif
}

//
// Module Initialisation
//
#undef Debug_Cond
#define Debug_Cond (can_debug != 0)
int init_module(void)
{
	int res;
/*	struct pci_dev *dev=NULL;

	if((dev=pci_find_device(CAN_PCI_VENDOR_ID, CAN_PCI_DEVICE_ID, dev))) {

	  MSG("Carte trouv�e\n");
	  MSG("Name : %s\n", dev->name);
	  MSG("Slot_Name : %s\n", dev->slot_name);
	  MSG("IRQ : %d\n", dev->irq);
	  // enable I/O area if not enabled
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,3,13)
	  if(pci_enable_device(dev))
	    {
	      MSG("Couldn't enable pci dev");
	      return 1;
	    }
#else
	  {
	    short tmp;
	    pci_read_config_word(dev,PCI_COMMAND,&tmp);
	    if (tmp & PCI_COMMAND_IO) {
	      tmp |= PCI_COMMAND_IO;
	      pci_write_config_word(dev,PCI_COMMAND,tmp);
	    }
	  }
#endif

	  if(CAN_BOARD_INIT(dev))
	    return 1;

	} else {
	  MSG("No can card found !\n");
	  return 1;
	}*/

	MSG("z1\n");
	if(CAN_BOARD_INIT())
	{
		MSG("zx1\n");
		return 1;
	}
	MSG("z2\n");

	/* If no name have been given set it to default */
	if (canboard_name == NULL)
		canboard_name = "can";

	/* Get the IRQ */
/*	can_irq = dev->irq;
	
  	OS_disable_irq(can_irq);
*/	
	/* Check parameters in request_irq for shared IRQ*/
	/* in case of linux-only driver */
/*	if(can_irq == 0 || OS_request_irq(can_irq, &canboard_interrupt, 0, canboard_name, NULL)){
		MSG("Bad IRQ (%d)\n", can_irq);
		OS_enable_irq(can_irq);
		return -EAGAIN;
	}

  	OS_enable_irq(can_irq);*/

	can_irq0=10;
	OS_disable_irq(can_irq0);
	
	/* Check parameters in request_irq for shared IRQ*/
	/* in case of linux-only driver */
	if(can_irq0 == 0 || OS_request_irq(can_irq0, &canboard_interrupt_10, 0, canboard_name, NULL)){
		MSG("Bad IRQ (%d)\n", can_irq0);
		OS_enable_irq(can_irq0);
		return -EAGAIN;
	}

  	OS_enable_irq(can_irq0);

	can_irq1=11;
	OS_disable_irq(can_irq1);
	
	/* Check parameters in request_irq for shared IRQ*/
	/* in case of linux-only driver */
	if(can_irq1 == 0 || OS_request_irq(can_irq1, &canboard_interrupt_11, 0, canboard_name, NULL)){
		MSG("Bad IRQ (%d)\n", can_irq1);
		OS_enable_irq(can_irq1);
		return -EAGAIN;
	}

	OS_enable_irq(can_irq1);

	
	/* register device with kernel */
#ifdef __RTL__
	if ((res=rtl_register_chrdev(SCHAR_MAJOR, canboard_name, &canboard_rtl_fops))) {
		MSG("can't register device with kernel\n");
		return res;
	}
#endif

	if ((res=register_chrdev(SCHAR_MAJOR, canboard_name, &canboard_fops))) {
		MSG("can't register device with kernel\n");
		return res;
	}

	MSG("module loaded\n");
	return 0;
}

//
// Module Cleanup
//
void cleanup_module(void) {

	unregister_chrdev(SCHAR_MAJOR,canboard_name);
#ifdef __RTL__
	rtl_unregister_chrdev(SCHAR_MAJOR,canboard_name);
#endif
	OS_free_irq(can_irq0);
	OS_free_irq(can_irq1);

	CAN_BOARD_CLOSE();
	MSG("CAN driver unloaded\n");
	return;
}
#undef Debug_Cond

#undef ArbraCan_C
