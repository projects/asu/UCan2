/*********************************************************
 *                                                       *
 *  Advanced Ring Buffer Redundant Application for CAN   *
 *                                                       *
 *********************************************************
 *                      ArbraCan.c                       *
 *********************************************************
 * This program is free software; you can redistribute   *
 * it and/or modify it under the terms of the GNU General*
 * Public License as published by the Free Software      *
 * Foundation; either version 2 of the License, or (at   *
 * your option) any later version.                       *
 *                                                       *
 * This program is distributed in the hope that it will  *
 * be useful, but WITHOUT ANY WARRANTY; without even the *
 * implied warranty of MERCHANTABILITY or FITNESS FOR A  *
 * PARTICULAR PURPOSE.  See the GNU General Public       *
 * License for more details.                             *
 *                                                       *
 * You should have received a copy of the GNU General    *
 * Public License along with this program; if not, write *
 * to 	The Free Software Foundation, Inc.               *
 *	675 Mass Ave                                     *
 *	Cambridge                                        *
 *	MA 02139                                         *
 * 	USA.                                             *
 *********************************************************
 *                                                       *
 *      Author: Edouard TISSERANT                        *
 *      Contact: edouard.tisserant@esstin.u-nancy.fr     *
 *      Version: 1.0                                     *
 *      Modification date:                               *
 *      Description:                                     *
 *-------------------------------------------------------*
 * Hardware dependant function for SJA1000 philips CAN   *
 * controler. For use with ArbraCan                      *
 *                                                       *
 *********************************************************/

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
//#include <linux/pci.h>
#include <asm/io.h>


//#include <linux/delay.h>
#include <linux/sched.h>

#include "Can.h"
#include "sja1000.h"
#include "ArbraCan.h"

#define DEBUG
#undef DEBUG

#ifdef DEBUG
static char can_debug = 1;
#define MSG(string, args...) printk("%s:%d:",__FILE__,__LINE__);printk(string, ##args)
#else
#define MSG(string, args...)
#endif

unsigned int PLX_9050_BASE;
unsigned int SJA_BASE[CAN_BUS_COUNT];

unsigned int SJA_RANGE=0x200;

static DECLARE_WAIT_QUEUE_HEAD(wqueue);


// initialisation de la carte PCI 7841 qui contient le SJA1000
int initBoardSJA1000(struct pci_dev *dev)
{
/*#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,3,13)
	PLX_9050_BASE = dev->resource[1].start & PCI_BASE_ADDRESS_IO_MASK;
#else
	PLX_9050_BASE = dev->base_address[1] & PCI_BASE_ADDRESS_IO_MASK;
#endif

	if(check_region(PLX_9050_BASE, 80)) {
		MSG("Check region failure\n");
		return 1;
	}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,3,13)
	SJA_BASE[0] = dev->resource[2].start & PCI_BASE_ADDRESS_IO_MASK;
#else
	SJA_BASE[0] = dev->base_address[2] & PCI_BASE_ADDRESS_IO_MASK;
#endif
	SJA_BASE[1] = SJA_BASE[0] + 128;*/

	SJA_BASE[0]=0xdf000;
	SJA_BASE[1]=SJA_BASE[0]+SJA_RANGE;
	
	if(request_mem_region(SJA_BASE[0], SJA_RANGE,"Fastwel/SJA1000(1)")){
		MSG("request region ok\n");
	}
	else{
		MSG("Check region failure\n");
		return 1;
	}

	if(request_mem_region(SJA_BASE[1], SJA_RANGE,"Fastwel/SJA1000(2)")) {
		MSG("request region ok\n");
	}
	else{
		MSG("Check region failure\n");
		return 1;
	}

	return 0;
}

void closeBoardSJA1000()
{
	release_mem_region(SJA_BASE[0], SJA_RANGE);
	release_mem_region(SJA_BASE[1], SJA_RANGE);
}

unsigned char canRd(int channel,int offset)
{
	return *(unsigned char*)phys_to_virt(SJA_BASE[channel]+offset);
}

void canWr(int channel,int offset,unsigned char byte)
{
	*(unsigned char*)phys_to_virt(SJA_BASE[channel]+offset)=byte;
}


// Initialisation du SJA10000
void initSJA1000(
	int bus_nb,				// numero du port
	unsigned char acc_code,			// acceptance code
	unsigned char acc_mask,			// acceptance mask
	int overrun_it,				// activation IT overrun
	int error_it,				// activation IT error
	int transmit_it,			// activation IT transmit
	int receive_it,				// activation IT receive
	int rate,
	int silent)
{
	unsigned char it;
	unsigned char v;
        wait_queue_t wait;
	
// on passe en mode reset en desactivant toutes les it
//???	canWr(bus_nb, CONTROL, RESET_MODE);


	v=canRd(bus_nb,CONTROL);
	if(v&0x01)
	{
		canWr(bus_nb,CONTROL,0x60);
		if((v=canRd(bus_nb,CONTROL))&0x01)
		{
			MSG("init error\n");
			return;
		}
	}
	MSG("X1: %x\n",v);
	// reset something, i don't know what
	canWr(bus_nb, 0x100, canRd(bus_nb, 0x100));
//	mdelay(1000);

	init_waitqueue_entry(&wait, current);
	add_wait_queue(&wqueue, &wait);
	set_current_state(TASK_INTERRUPTIBLE);

	schedule_timeout(100);

        set_current_state(TASK_RUNNING);
	remove_wait_queue(&wqueue, &wait);
	
	if(!((v=canRd(bus_nb,CONTROL))&0x01))
	{
			MSG("init error 0x10\n");
			return;
	}
	MSG("X2: %x\n",v);

	// on force le mode BasicCAN
//	canWr(bus_nb, CLOCK_DIVIDER,0x48);
	
	// Identification du node and timing fixing
	canWr(bus_nb, ACCEPTANCE_CODE,acc_code);
	canWr(bus_nb, ACCEPTANCE_MASK,acc_mask);
	if(silent)
		canWr(bus_nb, OUTPUT_CONTROL,0x02);
	else
		canWr(bus_nb, OUTPUT_CONTROL,0xFA);
		
        switch (rate) {
                case 1000: {
                        MSG("Setting 1000 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x00);
                        canWr(bus_nb,BUS_TIMING_1,0x14);
                        break;
                }
                case 500: {
                        MSG("Setting 500 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x40);
                        canWr(bus_nb,BUS_TIMING_1,0x3a);
                        break;
                }
                case 250: {
                        MSG("Setting 250 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x41);
                        canWr(bus_nb,BUS_TIMING_1,0x3a);
                    
                }
                case 125: {
                        MSG("Setting 125 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x43);
                        canWr(bus_nb,BUS_TIMING_1,0x3a);
                        break;
                }
                case 100: {
                        MSG("Setting 100 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x83);
                        canWr(bus_nb,BUS_TIMING_1,0x4d);
                        break;
                }
                case 50: {
                        MSG("Setting 50 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x87);
                        canWr(bus_nb,BUS_TIMING_1,0x4d);
                        break;
                }
                case 25: {
                        MSG("Setting 25 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x8f);
                        canWr(bus_nb,BUS_TIMING_1,0x4d);
                        break;
                }
                case 10: {
                        MSG("Setting 10 kbps.\n");
                        canWr(bus_nb,BUS_TIMING_0,0xa7);
                        canWr(bus_nb,BUS_TIMING_1,0x4d);
                        break;
                }
                default: {
                        MSG("Baud rate not supported default to 1000.\n");
                        canWr(bus_nb,BUS_TIMING_0,0x00);
                        canWr(bus_nb,BUS_TIMING_1,0x14);
			break;
                }
        }

	it = 	(overrun_it<<4) +
		(error_it<<3) +
		(transmit_it<<2) + 
		(receive_it << 1);
			
	// switch to operating mode and activate irq's
//	canWr(bus_nb, CONTROL,it);

	v=canRd(bus_nb,CONTROL);
	canWr(bus_nb,CONTROL,v|0x06);

	v=canRd(bus_nb,CONTROL);
	MSG("Before opmode (%i): %x\n",bus_nb,v);
	if(v&1)
	{
		canWr(bus_nb,CONTROL,v&0xfe);
	}
	v=canRd(bus_nb,CONTROL);
	MSG("After setting opmode (%i): %x\n",bus_nb,v);

	canWr(bus_nb, COMMAND, CLEAR_DATA_OVERRUN);
}

int sja1000_set_baudrate(int bus_nb, int baudrate)
{
	char isResetMode=canRd(bus_nb,CONTROL)&0x01;
	sja1000_set_reset_mode(bus_nb);

        switch (baudrate) {
	case 1000: {
		MSG("Setting 1000 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x00);
		canWr(bus_nb,BUS_TIMING_1,0x14);
		break;
	}
	case 500: {
		MSG("Setting 500 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x40);
		canWr(bus_nb,BUS_TIMING_1,0x3a);
		break;
                }
	case 250: {
		MSG("Setting 250 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x41);
		canWr(bus_nb,BUS_TIMING_1,0x3a);
	}
	case 125: {
		MSG("Setting 125 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x43);
		canWr(bus_nb,BUS_TIMING_1,0x3a);
		break;
	}
	case 100: {
		MSG("Setting 100 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x83);
		canWr(bus_nb,BUS_TIMING_1,0x4d);
		break;
	}
	case 50: {
		MSG("Setting 50 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x87);
		canWr(bus_nb,BUS_TIMING_1,0x4d);
		break;
	}
	case 25: {
		MSG("Setting 25 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0x8f);
		canWr(bus_nb,BUS_TIMING_1,0x4d);
		break;
	}
	case 10: {
		MSG("Setting 10 kbps.\n");
		canWr(bus_nb,BUS_TIMING_0,0xa7);
		canWr(bus_nb,BUS_TIMING_1,0x4d);
		break;
	}
	default: {
		MSG("Baud rate not supported.\n");
		sja1000_set_operating_mode(bus_nb);
		return -1;
		break;
	}
        }

	if(!isResetMode)
	{
		sja1000_set_operating_mode(bus_nb);
	}
	return 0;
}

int sja1000_set_reset_mode(int bus_nb)
{
	int v;
	v=canRd(bus_nb,CONTROL);
	canWr(bus_nb,CONTROL,v|0x01);
	return 0;
}

int sja1000_set_operating_mode(int bus_nb)
{
	int v;
	v=canRd(bus_nb,CONTROL);
	canWr(bus_nb,CONTROL,v&0xfe);
	return 0;
}

int sja1000_set_acceptance_filter(int bus_nb,
				  unsigned long acc_code,
				  unsigned long acc_mask)
{
	char isResetMode=canRd(bus_nb,CONTROL)&0x01;

	sja1000_set_reset_mode(bus_nb);
	canWr(bus_nb, ACCEPTANCE_CODE,acc_code);
	canWr(bus_nb, ACCEPTANCE_MASK,acc_mask);
	if(!isResetMode)
	{
		sja1000_set_operating_mode(bus_nb);
	}
	return 0;
}

int sja1000_command(int bus_nb, int command)
{
	canWr(bus_nb,COMMAND,command);
	
	return 0;
}

// envoi d'une frame
Message* sja1000_send_frame(int bus_nb, Message *mes)
{
	int dlc, i;
	unsigned char v;

	// bytes number
	dlc = mes->len & 0x0f;

	// COB_ID, RTR and DLC
	canWr(bus_nb, TRANSMIT_ID_0,((mes->cob_id.b.b1 & 0x07) << 5) | ((mes->cob_id.b.b0 & 0xf8) >>3) );
	canWr(bus_nb, TRANSMIT_ID_1,((mes->cob_id.b.b0 & 0x07) << 5) | ((mes->rtr & 0x01) << 4) | dlc );

	// fill transmit buffer
/*	for(i=0; i < dlc; i++) {
		outb(mes->data[i], TRANSMIT_DATA+i);
	}*/
	for(i=0; i < dlc; i++) {
		canWr(bus_nb, 12+i,mes->data[i]);
	}
	
	// send
	canWr(bus_nb, COMMAND, TRANS_REQUEST);

	v=canRd(bus_nb,2);
	MSG("transmit status (%i): %x\n",bus_nb,v);
	
	return mes;
}

// reception d'une frame
Message* sja1000_rcv_frame(int bus_nb, Message *mes) 
{
	int i;
	char b0, b1;
	
	b0 = canRd(bus_nb, RECEIVE_ID_0);
	b1 = canRd(bus_nb, RECEIVE_ID_1);
	mes->cob_id.b.b1 = (b0 & 0xe0) >> 5;   //MSB
	mes->cob_id.b.b0 = ((b0 & 0x1f) << 3) | ((b1 & 0xe0) >> 5);  //LSB
	mes->rtr = b1 & 0x10 ? 0x01 : 0x00;
	mes->len = b1 & 0x0f;
	
	for(i=0; i < mes->len; i++) {
		mes->data[i] = canRd(bus_nb, RECEIVE_DATA+i);
	}
	
	// on acquitte la reception
	canWr(bus_nb, COMMAND, RELEASE_RCV_BUFFER/*|0x08*/);
	
	return mes;
}

char	sja1000_status(int bus_nb)
{
	return canRd(bus_nb,STATUS);
}

char	sja1000_get_control(int bus_nb)
{
	return canRd(bus_nb,CONTROL);
}

int sja1000_set_control(int bus_nb, int ctl)
{
	canWr(bus_nb,CONTROL,ctl);
	
	return 0;
}

char	sja1000_ir(int bus_nb)
{
	return canRd(bus_nb,INTERRUPT);
}

char	plx_9050_ir()
{
	return inb(PLX_INTCSR);
}
