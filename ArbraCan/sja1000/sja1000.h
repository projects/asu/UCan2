/*********************************************************
 *                                                       *
 *  Advanced Ring Buffer Redundant Application for CAN   *
 *                                                       *
 *********************************************************
 *                      ArbraCan.h                       *
 *********************************************************
 * This program is free software; you can redistribute   *
 * it and/or modify it under the terms of the GNU General*
 * Public License as published by the Free Software      *
 * Foundation; either version 2 of the License, or (at   *
 * your option) any later version.                       *
 *                                                       *
 * This program is distributed in the hope that it will  *
 * be useful, but WITHOUT ANY WARRANTY; without even the *
 * implied warranty of MERCHANTABILITY or FITNESS FOR A  *
 * PARTICULAR PURPOSE.  See the GNU General Public       *
 * License for more details.                             *
 *                                                       *
 * You should have received a copy of the GNU General    *
 * Public License along with this program; if not, write *
 * to 	The Free Software Foundation, Inc.               *
 *	675 Mass Ave                                     *
 *	Cambridge                                        *
 *	MA 02139                                         *
 * 	USA.                                             *
 *********************************************************
 *                                                       *
 *      Author: Edouard TISSERANT                        *
 *      Contact: edouard.tisserant@esstin.u-nancy.fr     *
 *      Version: 1.0                                     *
 *      Modification date:                               *
 *      Description:                                     *
 *-------------------------------------------------------*
 * This is the header file for memory mapping definition *
 * of the SJA1000 and compilance with ArbraCan project.  *
 *********************************************************/

#define CAN_PCI_VENDOR_ID 0x144A 	// ADLINK vednor id
#define CAN_PCI_DEVICE_ID 0x7841 	// PCI 7841 device ID

// Registre d'interruption du PLX9050 (interface PCI)
#define PLX_INTCSR		PLX_9050_BASE+0x4C

// Registres de controle du SJA dans Operating mode
#define CONTROL			0x00
#define COMMAND			0x01
#define STATUS			0x02
#define INTERRUPT		0x03

// Registres de controle dans Reset mode
#define ACCEPTANCE_CODE		0x04
#define ACCEPTANCE_MASK		0x05
#define BUS_TIMING_0		0x06
#define BUS_TIMING_1		0x07
#define OUTPUT_CONTROL		0x08

// Transmit buffer
#define TRANSMIT_ID_0		0x0A
#define TRANSMIT_ID_1		0x0B
#define TRANSMIT_DATA		0x0C	// debut de la zone transmit data

// Receive buffer
#define RECEIVE_ID_0		0x14
#define RECEIVE_ID_1		0x15
#define RECEIVE_DATA		0x16	// debut de la zone receive data

// Miscellaneous
#define CLOCK_DIVIDER		0x1F

// Control register CR
#define RESET_MODE		0x01		// passage en mode reset
#define OPERATING_MODE		0x00		// passage en mode operating (mode de travail avec le SLA)

// Command register CMR
#define GO_TO_SLEEP		0x10
#define CLEAR_DATA_OVERRUN	0x08
#define RELEASE_RCV_BUFFER	0x04
#define ABORT_TRANSMISSION	0x02
#define TRANS_REQUEST		0x01

// Status register
#define BUS_STATUS		0x80
#define ERROR_STATUS		0x40
#define TRANSMIT_STATUS		0x20
#define RECEIVE_STATUS		0x10
#define TRANS_COMPLETE		0x08
#define TRANS_BUFFER_STATUS	0x04
#define DATA_OVERRUN_STATUS	0x02
#define RCV_BUFFER_STATUS	0x01

// Definitions pour le bus timing
#define BRP			0x04		// Baud Rate Prescaler (sur 6 bits)
#define SJW			0x00		// Synchronization Jump Width (sur 2 bits)
#define SAM			0x00		// Sampling
#define TSEG1			0x0D		// Time Segment 1 (sur 4 bits)
#define TSEG2			0x04		// Time Segment 2 (sur 3 bits)


// Prototypes
int initBoardSJA1000();
void closeBoardSJA1000(void);
void initSJA1000(int bus_nb,
	unsigned char acc_code,
	unsigned char acc_mask,
	int overrun_it,
	int error_it,
	int transmit_it,
	int receive_it,
	int rate,
	int silent);
Message* sja1000_send_frame(int bus_nb, Message *mes);
Message* sja1000_rcv_frame(int bus_nb, Message *mes);
char sja1000_status(int bus_nb);
char sja1000_ir(int bus_nb);
char plx_9050_ir(void);

int sja1000_command(int bus_nb, int command);
int sja1000_set_baudrate(int bus_nb, int baudrate);
int sja1000_set_operating_mode(int bus_nb);
int sja1000_set_reset_mode(int bus_nb);
int sja1000_set_acceptance_filter(int bus_nb,
				  unsigned long acc_code,
				  unsigned long acc_mask);


char sja1000_get_control(int bus_nb);
int sja1000_set_control(int bus_nb, int ctl);


// Nombre de bus sur cette carte
#define CAN_BUS_COUNT		2

// Init du SJA. Ecoute tout (maitre), IT sur Rx et TX.
#define CAN_INIT(nb_bus, rate)	initSJA1000(nb_bus, 0xff, 0xff, 0, 0, 1, 1, rate, 0)

// Init du SJA. Ecoute tout (maitre) et silencieu, IT sur Rx.
#define CAN_INIT_SILENT(nb_bus, rate)	initSJA1000(nb_bus, 0xff, 0xff, 0, 0, 0, 1, rate, 1)

// UnInit du SJA. Sourd et sans IT
#define CAN_UNINIT(nb_bus)	initSJA1000(nb_bus, 0x00, 0x00, 0, 0, 0, 0, can_rate, 1)

// Init de la glue logic, allocation des zones d'IO
#define CAN_BOARD_INIT()	initBoardSJA1000()

// Free IO zone
#define CAN_BOARD_CLOSE()	closeBoardSJA1000()

// Lire le paquet dans le buffer Rx
#define CAN_GET(nb_bus, buff)	sja1000_rcv_frame(bus_nb, buff)

// Ecrit un paquet dans le buffer Tx et leve le bit d'emission
#define CAN_PUT(nb_bus, buff)	sja1000_send_frame(bus_nb, buff)

// Retourne le status
#define CAN_STATUS(nb_bus)	sja1000_status(nb_bus)

// Bit message present
#define CAN_RX_MSG_READY(nb_bus)	(sja1000_status(nb_bus) & RCV_BUFFER_STATUS)

// Bit Tx buffer libre
#define CAN_TX_BUFF_FREE(nb_bus)	(sja1000_status(nb_bus) & TRANS_BUFFER_STATUS)

// Bit Transmission PAS en cours
#define CAN_TX_FREE(nb_bus)	(sja1000_status(nb_bus) & TRANSMIT_STATUS)

// Bit transmission accomplie
#define CAN_TX_OK(nb_bus)	(sja1000_status(nb_bus) & TRANS_COMPLETE)

// Bits d'erreur
#define CAN_ERROR(nb_bus)	(sja1000_status(nb_bus) & ERROR_STATUS)

// Lecture du registre d'IT
// Sometimes sja1000 get crazy on adlink card and IR contains nothing !!
// So CAN_IR returns always OK then we force buffer checking in IRQ handler.
#define CAN_IR(nb_bus)	sja1000_ir(nb_bus)
//#define CAN_IR(nb_bus)	(RECEIVE_IT||TRANSMIT_IT)
#define RECEIVE_IT	0x01
#define TRANSMIT_IT	0x02


