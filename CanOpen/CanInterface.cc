// -------------------------------------------------------------------------
#include "CanInterface.h"
#include "CanExceptions.h"
#include "SimpleCanDeviceArbracan.h"
#include "SimpleCanDeviceCanbus.h"
#include "SimpleCanDevicePCI_1680.h"
#include "SimpleCanDeviceCan200.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace CanOpen;
// -------------------------------------------------------------------------
CanInterface::CanInterface( const std::string device, CanType type ):
candev(0)
{
	switch( type )
	{
		case ctArbra:
			candev = new SimpleCanDeviceArbracan(device);
		break;
		
		case ctPCI1680:
			candev = new SimpleCanDevicePCI_1680(device);
		break;
			
		case ctCanbus:
			candev = new SimpleCanDeviceCanbus(device);
		break;

		case ctCan200:
			candev = new SimpleCanDeviceCan200(device);
		break;

		default:
			throw CanException("Can`t create interface: Unknown type" + type );
		break; 
	}
	
	candev->resetController();
}
// -------------------------------------------------------------------------
CanInterface::~CanInterface()
{
	delete candev;
}
// -------------------------------------------------------------------------
void CanInterface::setWaiting( bool state )
{
	candev->setWaiting(state);
}
// -------------------------------------------------------------------------
int CanInterface::sendMessage( Message*msg )
{
	return candev->sendMessage(msg);
}
// -------------------------------------------------------------------------
int CanInterface::receiveMessage( Message*msg )
{
	return candev->receiveMessage(msg);
}
// -------------------------------------------------------------------------
void CanInterface::setAcceptanceFilter(int acceptanceCode, int acceptanceMask)
{
	candev->setAcceptanceFilter( acceptanceCode, acceptanceMask);
}
// -------------------------------------------------------------------------
void CanInterface::setBaudRate(int baudrate)
{
	candev->setBaudRate(baudrate);
}
// -------------------------------------------------------------------------
void CanInterface::setProtocol( SimpleCanDevice::CanProtocol prot )
{
	candev->setProtocol(prot);
}
// -------------------------------------------------------------------------
void CanInterface::resetController()
{
	candev->resetController();
}
// -------------------------------------------------------------------------
void CanInterface::setFrameFormat( SimpleCanDevice::CanFrameFormat fmt )
{
	candev->setFrameFormat(fmt);
}
// -------------------------------------------------------------------------
