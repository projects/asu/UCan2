#include <cstdlib>
#include <cstring>

#include "CanOpenDevice.h"
#include "ObjectDictionary.h"

#include "CanExceptions.h"

#include "ArbraCan.h"

#include "EDSParser.h"

#include "CanHelpers.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace CanOpen;

CanOpenDevice::CanOpenDevice(SimpleCanDevice*device):
simpleDevice(device),
NMTtBit(0),currentState(Initialising)
{
}

CanOpenDevice::~CanOpenDevice()
{
}

void CanOpenDevice::sendSDO(CanOpen::SDO sdo, int direction)
{
	Message msg;
	
	msg.cob_id.w=sdo.cobID.nodeID|(((direction==Rx)?FcSDOrx:FcSDOtx)<<7);
	msg.rtr=0x00;
//	msg.len=sdo.len;
	msg.len=8;
	memcpy(msg.data,&sdo.body,8);

	simpleDevice->sendMessage(&msg);
}

/*namespace CanOpen
{
	enum FunctionMask
	{
		PDOntx=0x03,
		PDOnrx=0x04
	};
};*/

void CanOpenDevice::sendPDO(CanOpen::PDO pdo, int pdoNo/*, int direction*/)
{
	Message msg;
	
//	msg.cob_id.w=(pdo.ID&0x7f)|
//		((((pdoNo&0x03)<<1)+((direction==Rx)?PDOnrx:PDOntx))<<7);
	msg.cob_id.w=pdo.cobID.cobID;//|(((direction==Rx)?PDOnrx:PDOntx)<<7);
//	printf("sendPDO: cobID: 0x%x\n",msg.cob_id.w);
	msg.rtr=0x00;
	msg.len=pdo.len;
//	printf("sendPDO: len: %i\n",msg.len);
	memcpy(msg.data,pdo.data,msg.len);

	simpleDevice->sendMessage(&msg);
}


void CanOpenDevice::sendEmergencyObject(CanOpen::EmergencyObject eObj)
{
	Message msg;
	
	msg.cob_id.w=eObj.cobID.nodeID|(FcEmergencyObject<<7);
	msg.rtr=0x00;
	msg.len=8;
	memcpy(msg.data,&eObj.body,8);

	simpleDevice->sendMessage(&msg);
}

void CanOpenDevice::sendNMT(CanOpen::NMTGuardMessage nmt)
{
	Message msg;

	msg.cob_id.w=nmt.cobID.cobID|(FcNodeGuard<<7);
	msg.rtr=nmt.rtr;
	msg.len=(nmt.rtr==0x00)?1:0;
	msg.data[0]=nmt.byte0;
	simpleDevice->sendMessage(&msg);
}


void CanOpenDevice::processingReceivedMessage()
{
	Message msg;
	CobID cobID;

	
	checkSDOTimeouts();
//	try
//	{
		simpleDevice->receiveMessage(&msg);
//	}
/*	catch(...)
	{
		// no messages;
		return;
	}*/
	cobID.cobID=(unsigned int)msg.cob_id.w;
	
	switch(cobID.cobID)
	{
	case 0x000:
		// NMT Module Control
		{
			NMTMessage nmt;
			nmt.cobID=cobID;
			nmt.cs=msg.data[0];
			nmt.nodeID=msg.data[1];
			processingNMT(nmt);
		}
		break;
	case 0x080:
		// SYNC
		break;
	case 0x100:
		// TIME STAMP
		break;
	default:
//		printf("Rcv: FC: %x\n",cobID.functionCode);
		switch(cobID.functionCode)
		{
		case FcEmergencyObject:
			// EMERGENCY
			{
				printf("EMERGENCY\n");
				CanOpen::EmergencyObject eObj;
				eObj.cobID.cobID=cobID.cobID;
				memcpy(&eObj.body,msg.data,8);
				processingEmergencyObject(&eObj);
			}
			break;
		case FcPDO1tx ... FcPDO4rx:
			// PDO
			{
				PDO pdo;
				pdo.cobID.cobID=cobID.cobID;
				pdo.len=msg.len;
				memcpy(pdo.data,msg.data,msg.len);
				processingReceivedPDO(&pdo);
			}
			break;
		case FcSDOtx:
		case FcSDOrx:
			// SDO (transmit/server)
			// SDO (receive/client)
			{
				SDO sdo;
				sdo.cobID.cobID=cobID.cobID;
//				sdo.len=msg.len;
				memcpy(&sdo.body,msg.data,8);
				processingReceivedSDO(&sdo);
			}
			break;
		case FcNodeGuard:
			{
//				printf("FcNodeGuard\n");
				// NMT (error control)
				NMTGuardMessage nmt;
				nmt.cobID=cobID;
				nmt.rtr=msg.rtr;
				nmt.byte0=msg.data[0];
				processingNMTNodeGuard(&nmt);
			}
			break;
		default:
			// Unknown message
			// assert(1==0);
			break;
		}
	}
}

void CanOpenDevice::loadEDS(string edsFile)
{
	EDSParser eds(edsFile);

	eds.fillOD(&od);
}

void CanOpenDevice::mapRxPDOtoOD(CanOpen::PDO*pdo,
				 CanOpen::ObjectDictionary*objD)
{
	int num,k,nbit,pdoNo;
	char data[16];
	PDOMapObject mObj;

	pdoNo=objD->findRxPDONoByCobID(pdo->cobID.cobID);

	num=objD->getRxPDOsNumberOfMappedObjects(pdoNo);
	for(k=0,nbit=0;k<num;k++)
	{
		mObj=objD->getRxPDOsMappedObject(pdoNo,k);
//		int dSize=objD->getDataSize(mObj.index,mObj.subIndex);
//		data=malloc((mObj.numBits+7)/8);
//		memset(data,0,(mObj.numBits+7)/8);
//		data=malloc(dSize);
		memset(data,0,16);
		CanHelpers::copyBits(data,0,
				     pdo->data,nbit,mObj.numBits);
//		printf("mapRxPDOtoOD: bitNo: %i, index: %x, subindex: %x; bitSize: %i, data: %i\n",
//		       nbit,mObj.index,mObj.subIndex,mObj.numBits,*(int*)data);
#warning Надо что-то более умное с сохранением значения придумать
		if(mObj.numBits==1)
		{
			setBitValue(mObj.index,mObj.subIndex,data[0]?1:0);
		}
		else if(mObj.numBits<=sizeof(int)*8)
		{
			int x;
			memcpy(&x,data,sizeof(int));
			setIntValue(mObj.index,mObj.subIndex,x);
		}
		else
		{ // изначально было только это, но для возможности
		  // overload'а сделал так.
			objD->setValue(mObj.index,mObj.subIndex,data); // maybe xpRnHeTCR
		}
//		free(data);
		nbit+=mObj.numBits;
	}
}

void CanOpenDevice::mapTxPDOtoOD(CanOpen::PDO*pdo,
				 CanOpen::ObjectDictionary*objD)
{
	int num,k,nbit,pdoNo;
//	void*data;
	char data[16];
	PDOMapObject mObj;

	pdoNo=objD->findTxPDONoByCobID(pdo->cobID.cobID);

	num=objD->getTxPDOsNumberOfMappedObjects(pdoNo);
	for(k=0,nbit=0;k<num;k++)
	{
		mObj=objD->getTxPDOsMappedObject(pdoNo,k);
//		int dSize=objD->getDataSize(mObj.index,mObj.subIndex);
//		data=malloc((mObj.numBits+7)/8);
//		memset(data,0,(mObj.numBits+7)/8);

//		data=malloc(dSize);
		memset(data,0,16);
		CanHelpers::copyBits(data,0,pdo->data,nbit,mObj.numBits);
		//printf("mapTxPDOtoOD: bitNo: %i, index: %x, subindex: %x; bitSize: %i, data: %i\n",
		//       nbit,mObj.index,mObj.subIndex,mObj.numBits,*(int*)data);
#warning Надо что-то более умное с сохранением значения придумать
		if(mObj.numBits==1)
		{
			setBitValue(mObj.index,mObj.subIndex,data[0]?1:0);
		}
		else if(mObj.numBits<=8*sizeof(int))
		{
			int x;
			memcpy(&x,data,sizeof(int));
			setIntValue(mObj.index,mObj.subIndex,x);
		}
		else
		{ // изначально было только это, но для возможности
		  // overload'а сделал так.
			objD->setValue(mObj.index,mObj.subIndex,data); // maybe xpRnHeTCR
		}
//		free(data);
		nbit+=mObj.numBits;
	}
}

void CanOpenDevice::assemblyRxPDO(int pdoNo,CanOpen::ObjectDictionary*objD,
				  PDO*pdo)
{
	int k,num,bitNo,bitSize;
	PDOMapObject mObj;
//	void*data;
	char data[16];

//	pdo->cobID.pdoCobID.pdoNo=pdoNo;
//	pdo->cobID.pdoCobID.pdoDirection=0;
	
	// assembly RxPDO from mapped objects of OD
	num=objD->getRxPDOsNumberOfMappedObjects(pdoNo);
	pdo->cobID.cobID=objD->getRxPDO_COB_ID(pdoNo);
	
	for(k=0,bitNo=0;k<num;k++)
	{
		mObj=objD->getRxPDOsMappedObject(pdoNo,k);
		bitSize=objD->getValueBitSize(mObj.index,mObj.subIndex);
//		data=malloc((mObj.numBits+7)/8);
//		data=malloc((bitSize+7)/8);
#warning Надо что-то более умное с получением значения придумать
		if(mObj.numBits==1)
		{
			data[0]=getBitValue(mObj.index,mObj.subIndex)?1:0;
		}
		else if(mObj.numBits<=8*sizeof(int))
		{
			int x;
			x=getIntValue(mObj.index,mObj.subIndex);
			memcpy(data,&x,sizeof(int));
		}
		else
		{ // изначально было только это, но для возможности
		  // overload'а сделал так.
			objD->getValue(mObj.index,mObj.subIndex,data);
		}
		CanHelpers::copyBits(pdo->data,bitNo,data,0,mObj.numBits);
//		free(data);
		bitNo+=mObj.numBits;
	}
	pdo->len=(bitNo+7)/8;
}

void CanOpenDevice::assemblyTxPDO(int pdoNo,CanOpen::ObjectDictionary*objD,
				  PDO*pdo)
{
	int k,num,bitNo,bitSize;
	PDOMapObject mObj;
//	void*data;
	char data[16];

//	pdo->cobID.pdoCobID.pdoNo=pdoNo;
//	pdo->cobID.pdoCobID.pdoDirection=1;
	
	// assembly TxPDO from mapped objects of OD
	num=objD->getTxPDOsNumberOfMappedObjects(pdoNo);
	pdo->cobID.cobID=objD->getTxPDO_COB_ID(pdoNo);

	for(k=0,bitNo=0;k<num;k++)
	{
		mObj=objD->getTxPDOsMappedObject(pdoNo,k);
		bitSize=objD->getValueBitSize(mObj.index,mObj.subIndex);
		//printf("AssemblyTxPDO: bitNo: %i, index: %x, subindex: %x; bitSize: %i, numBits: %i\n",
		//       bitNo,mObj.index,mObj.subIndex,bitSize,mObj.numBits);
//		printf("sizeof(int): %i\n",sizeof(int));
//		data=malloc((mObj.numBits+7)/8);
//		data=malloc((bitSize+7)/8);
#warning Надо что-то более умное с получением значения придумать
		if(mObj.numBits==1)
		{
			data[0]=getBitValue(mObj.index,mObj.subIndex)?1:0;
		}
		else if(mObj.numBits<=sizeof(int)*8)
		{
			int x;
//			printf("AssemblyTxPDO: getIntValue\n");
			x=getIntValue(mObj.index,mObj.subIndex);
			memcpy(data,&x,sizeof(int));
		}
		else
		{ // изначально было только это, но для возможности
		  // overload'а сделал так.
			objD->getValue(mObj.index,mObj.subIndex,data);
		}
		CanHelpers::copyBits(pdo->data,bitNo,data,0,mObj.numBits);
//		free(data);
		bitNo+=mObj.numBits;
	}
	pdo->len=(bitNo+7)/8;
//	printf("AssemblyTxPDO: len: %i; ok\n",pdo->len);
}

void CanOpenDevice::sendTxPDO(int pdoNo)
{
	PDO pdo;

	assemblyTxPDO(pdoNo,&od,&pdo);
	sendPDO(pdo, pdoNo/*, Tx*/);
}

void CanOpenDevice::sendRxPDO(int pdoNo)
{
	PDO pdo;

	assemblyRxPDO(pdoNo,&od,&pdo);
	sendPDO(pdo, pdoNo/*, Tx*/);
}


void CanOpenDevice::processingReceivedSDO(CanOpen::SDO*sdo)
{
//	assert((sdo->cobID.functionCode==FcSDOtx)||
//	       (sdo->cobID.functionCode==FcSDOrx));
	if( (sdo->cobID.functionCode!=FcSDOtx) && 
		(sdo->cobID.functionCode!=FcSDOrx))
		return;

	unsigned int sdoCobIDrx;
	unsigned int sdoCobIDtx;

//	printf("processingReceivedSDO\n");
	od.getValue(ObjectDictionary::SDOServerParm,0x01,&sdoCobIDrx);
//	printf("processingReceivedSDO1\n");
	od.getValue(ObjectDictionary::SDOServerParm,0x02,&sdoCobIDtx);
//	printf("processingReceivedSDO2\n");
	
	if(sdo->cobID.functionCode&0x01)
	{ // не принимаем сообщения от сервера (я не клиент)
		return;
		// все остальные чужие
	}
	else
	{
		printf("sdo->cobID.cobID: 0x%x; sdoCobIDrx: 0x%x\n",
		       sdo->cobID.cobID,sdoCobIDrx);
		if(sdo->cobID.cobID==sdoCobIDrx)
		{ // тут принимаем сообщения от клиентов (я сервер)
//			printf("My sdo, analizing...\n");
			switch(sdo->body.commandSpec.init.command)
			{
			case InitiateDomainUploadRequest: // и будет у нас только
							// expidited transfer
				{
//					printf("InitiateDomainUploadRequest\n");
					SDO txsdo;
					unsigned int len;
					
					txsdo.cobID.nodeID=sdo->cobID.nodeID;
					txsdo.body.init.index=
						sdo->body.init.index;
					txsdo.body.init.subindex=
						sdo->body.init.subindex;
					txsdo.body.commandSpec.init.command=
						InitiateDomainUploadAnswer;

					len=(od.getValueBitSize(sdo->body.init.index,
								sdo->body.init.subindex)+7)/8;

					if(len<=4)
					{
						txsdo.body.commandSpec.
							init.e=1;
						txsdo.body.commandSpec.
							init.n=4-len;
						txsdo.body.commandSpec.
							init.s=0;

						od.getValue(sdo->body.init.index,
							    sdo->body.init.subindex,
							    txsdo.body.init.data);

//						printf("InitiateDomainUploadRequest: send answer\n");
						sendSDO(txsdo,Tx);
					}
					else
					{
						// mandatory ojbects's length
						// must be less or equal 4 byte
						// так что перебьётесь
					}
				}
				break;
			case AbortDomainTransfer: // оно нам надо?
				// а на Abort мы ничего отвечать не будем
				break;
			default:
				// тут на все остальные сообщения надо бы
				// ошибки (Abort) посылать
				{
					SDO txsdo;
					txsdo.body.commandSpec.init.command=
						AbortDomainTransfer;
					txsdo.body.init.index=
						sdo->body.init.index;
					txsdo.body.init.subindex=
						sdo->body.init.subindex;
					*((unsigned int*)
					  (&txsdo.body.init.data))=
						InvalidCommandSpec;
					sendSDO(txsdo,Tx);
				}
				break;
			}
		}
	}
}

void CanOpenDevice::setNodeID(int id)
{
	od.setNodeID(id);
}

int CanOpenDevice::getNodeID()
{
	return od.getNodeID();
}

int CanOpenDevice::getIntValue(unsigned short index, unsigned char subindex)
{
	int value=0;
	DataType type=od.getDataType(index,subindex);
	
	switch(type)
	{
	case Integer8:
	case Integer16:
	case Integer32:
	case Integer24:
	case Unsigned8:
	case Unsigned16:
	case Unsigned32:
	case Unsigned24:
		od.getValue(index,subindex,&value);
		return value;
		break;
	default:
		// not integer type
		throw IncompatibleTypes(type, Integer32);
		break;
	}
	return 0;
}

void CanOpenDevice::setIntValue(unsigned short index, unsigned char subindex,
			    int value)
{
	DataType type=od.getDataType(index,subindex);
	
	switch(type)
	{
	case Integer8:
	case Integer16:
	case Integer32:
	case Integer24:
	case Unsigned8:
	case Unsigned16:
	case Unsigned32:
	case Unsigned24:
		od.setValue(index,subindex,&value);
		break;
	default:
		// not integer type
		throw IncompatibleTypes(type, Integer32);
		break;
	}
}

bool CanOpenDevice::getBitValue(unsigned short index, unsigned char subindex)
{
	char value;
	DataType type=od.getDataType(index,subindex);
	
	switch(type)
	{
	case Boolean:
		od.getValue(index,subindex,&value);
		return (value!=0);
		break;
	default:
		// not boolean type
		throw IncompatibleTypes(type, Boolean);
		break;
	}
	return 0;
}

void CanOpenDevice::setBitValue(unsigned short index, unsigned char subindex,
			    bool value)
{
	unsigned char zval=value?1:0;
	DataType type=od.getDataType(index,subindex);
	
	switch(type)
	{
	case Boolean:
		od.setValue(index,subindex,&zval);
		break;
	default:
		// not boolean type
		throw IncompatibleTypes(type, Boolean);
		break;
	}
}

CanOpen::DataType CanOpenDevice::getDataType(unsigned short index,
					     unsigned char subindex)
{
	return od.getDataType(index,subindex);
}


void CanOpenDevice::processingNMTNodeGuard(CanOpen::NMTGuardMessage*nmt)
{
//	printf("NodeID: my: %x; rcvCobID: %x\n",od.getNodeID(),
//	       nmt->cobID.cobID);
	if(nmt->cobID.nodeID!=od.getNodeID())
	{
		return;
	}

	if(nmt->rtr==0x00)
	{
	}
	else
	{
		NMTGuardMessage nmt;

		nmt.cobID.cobID=0;
		nmt.cobID.nodeID=od.getNodeID();
		nmt.rtr=0x00;
		nmt.t=NMTtBit;
		NMTtBit=~NMTtBit;
		nmt.state=currentState;

		sendNMT(nmt);
	}
}

void CanOpenDevice::setRxPDOCobID(int pdoNo,unsigned int cobID)
{
	od.setRxPDO_COB_ID(pdoNo,cobID);
}

void CanOpenDevice::setTxPDOCobID(int pdoNo,unsigned int cobID)
{
	od.setTxPDO_COB_ID(pdoNo,cobID);
}
