#include <cstdlib>
#include <cstring>

#include <assert.h>
#include "CanOpenDeviceWithSDO.h"
#include "CanOpenMessages.h"
//#include "Can.h"
#include "CanExceptions.h"
#include "ArbraCan.h"


using namespace CanOpen;

// SDO timeout будет 5 с, в описании не нашёл
const timeval CanOpenDeviceWithSDO::SDOTimeout={5,0};

CanOpenDeviceWithSDO::CanOpenDeviceWithSDO(SimpleCanDevice*device):
CanOpenDevice(device)
{
	prevSDOCheck.tv_sec=0;
	prevSDOCheck.tv_usec=0;
}

void CanOpenDeviceWithSDO::checkSDOTimeouts()
{
	const timeval rSDOTimeout={0,100000};
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv,&tz);
	if(CanHelpers::cmpTimeVals(rSDOTimeout,
				   CanHelpers::
				   subTimeVals(tv,prevSDOCheck))<0)
	{
		prevSDOCheck=tv;
		return;
	}
	prevSDOCheck=tv;
//	printf("Check clientSDOUpload\n");
	checkSDOTimeout(&clientSDOUpload);
//	printf("Check clientSDODownload\n");
	checkSDOTimeout(&clientSDODownload);

//	printf("Check serverSDOUpload\n");
	checkSDOTimeout(&serverSDOUpload);
//	printf("Check serverSDODownload\n");
	checkSDOTimeout(&serverSDODownload);
}

void CanOpenDeviceWithSDO::checkSDOTimeout(SDOTransfer*sTrans)
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv,&tz);

	if(CanHelpers::cmpTimeVals(SDOTimeout,
				   CanHelpers::
				   subTimeVals(tv,sTrans->lastMsgTime))<0)
	{
		if(sTrans->data!=NULL)
			free(sTrans->data);
		sTrans->data=NULL;
		sTrans->isActive=0;
//		printf("nodeID: 0x%02x; We kill it! :(\n",sTrans->nodeID);
#warning Надо бы подумать: посылать Abort или нет?
	}
}

void CanOpenDeviceWithSDO::checkSDOTimeout(map<int,SDOTransfer>*sTrans)
{
	map<int,SDOTransfer>::iterator stIt=sTrans->begin();
	
	for(;stIt!=sTrans->end();stIt++)
	{
		checkSDOTimeout(&(stIt->second));
	}
}


void CanOpenDeviceWithSDO::clientSdoInitiateDomainDownload(int nodeID,
						       unsigned short index,
						       unsigned char subindex,
						       unsigned int len,
						       void*data)
{
	SDO sdo;

	if(clientSDODownload.isActive)
	{ // in description (that I have) doesn't said anything about
		// simultaneous SDO data transfering
		throw SDOTransferActive("Download is already active");
	}

	sdo.cobID.nodeID=nodeID;
	sdo.body.init.index=index;
	sdo.body.init.subindex=subindex;
	sdo.body.commandSpec.init.command=InitiateDomainDownloadRequest;
	
	clientSDODownload.isActive=true;
	clientSDODownload.len=len;
	clientSDODownload.nodeID=nodeID;
	
	if(len<4)
	{ // expedited transfer
		sdo.body.commandSpec.init.e=1;
		sdo.body.commandSpec.init.n=4-len;
		sdo.body.commandSpec.init.s=0;
		memcpy(sdo.body.init.data,data,len);
		clientSDODownload.data=NULL;
		clientSDODownload.isSegmented=false;
	}
	else
	{ // normal (segmented) transfer
		sdo.body.commandSpec.init.e=0;
		sdo.body.commandSpec.init.n=0;
		sdo.body.commandSpec.init.s=1;
		memcpy(sdo.body.init.data,&len,4);
		
		clientSDODownload.isSegmented=true;
		clientSDODownload.data=malloc(len);
		memcpy(clientSDODownload.data,data,len);
		clientSDODownload.currentPosition=0;
		clientSDODownload.t=0;
	}

	sendSDO(sdo,Rx);

	struct timezone tz;
	gettimeofday(&clientSDODownload.lastMsgTime,&tz);
}


// эту функцию надо вызывать из processingReceivedSDO,
// после получения очередного подтверждения
void CanOpenDeviceWithSDO::clientSdoDownloadDomainSegment(SDO*rxSDO)
{
	if(!clientSDODownload.isActive)
	{
		printf("UnexpectedSDOCommand\n");
		throw UnexpectedSDOCommand(rxSDO->body.commandSpec.
					   segment.command);
	}
	
	SDO sdo;
	unsigned int length,xlen;
	
	sdo.cobID.nodeID=clientSDODownload.nodeID;

	sdo.body.commandSpec.segment.t=clientSDODownload.t;
	clientSDODownload.t=~clientSDODownload.t;
	sdo.body.commandSpec.segment.command=DownloadDomainSegmentRequest;
	
	length=clientSDODownload.len-clientSDODownload.currentPosition;
	xlen=(length>7)?7:length;
	
	sdo.body.commandSpec.segment.n=7-xlen;
	sdo.body.commandSpec.segment.c=(length>7)?0:1;

	
	memcpy(sdo.body.segment.data,(char*)clientSDODownload.data+
	       clientSDODownload.currentPosition,xlen);
	clientSDODownload.currentPosition+=xlen;
	if(sdo.body.commandSpec.segment.c)
	{
		free(clientSDODownload.data);
		clientSDODownload.data=NULL;
		clientSDODownload.isActive=false;
	}
//	printf("length: %x,xlen: %x,c: %x\n",length,xlen,sdo.body.commandSpec.segment.c);

	sendSDO(sdo,Rx);

	struct timezone tz;
	gettimeofday(&clientSDODownload.lastMsgTime,&tz);
}

void CanOpenDeviceWithSDO::serverSdoInitiateDomainDownload(SDO*sdo)
{
	SDO txSDO;
	unsigned int cobID;
	
	txSDO.cobID.cobID=od.getTxSDO_COB_ID();
	

	if(serverSDODownload.isActive)
	{
		// я занят, позвоните попозже
//#warning Здесь надо exception выбросить
		txSDO.body.commandSpec.init.command=AbortDomainTransfer;
		txSDO.body.init.index=sdo->body.init.index;
		txSDO.body.init.subindex=sdo->body.init.subindex;
		*((unsigned int*)(&txSDO.body.init.data))=CannotStoreData;
		sendSDO(txSDO,Tx);
		return;
	}
	
	if(sdo->body.commandSpec.init.e==1)
	{ // короткие данные
		unsigned int data;
		memcpy(&data,sdo->body.init.data,
		       4-sdo->body.commandSpec.init.n);
		try
		{
#warning Надо ещё атрибуты доступа проверять
			od.setValue(sdo->body.init.index,
				    sdo->body.init.subindex,
				    &data);
		}
		catch(ODEntryNotFound)
		{
			//надо бы что-то ответить, если в ОД ничего такого нет
			txSDO.body.commandSpec.init.command=
				AbortDomainTransfer;
			txSDO.body.init.index=sdo->body.init.index;
			txSDO.body.init.subindex=sdo->body.init.subindex;
			*((unsigned int*)(&txSDO.body.init.data))=
				ObjectDoesnotExist;
			sendSDO(txSDO,Tx);
			return;
		}
	}
	else
	{ // длинные данные
		if(sdo->body.commandSpec.init.s==0)
		{ // reserved by CiA for future use
			return;
		}
		serverSDODownload.isActive=1;
		serverSDODownload.isSegmented=1;
		serverSDODownload.index=sdo->body.init.index;
		serverSDODownload.subindex=sdo->body.init.subindex;
		memcpy(&serverSDODownload.len,
		       sdo->body.init.data,sizeof(unsigned int));
		serverSDODownload.currentPosition=0;
		serverSDODownload.t=1;
		serverSDODownload.data=malloc(serverSDODownload.len);
	}

	txSDO.body.commandSpec.init.command=InitiateDomainDownloadAnswer;
	sendSDO(txSDO,Tx);

	struct timezone tz;
	gettimeofday(&serverSDODownload.lastMsgTime,&tz);
}

void CanOpenDeviceWithSDO::serverSdoDownloadDomainSegment(SDO*sdo)
{
	SDO txSDO;
	
	txSDO.cobID.cobID=od.getTxSDO_COB_ID();
	
	if(!serverSDODownload.isActive)
	{
		printf("UnexpectedSDOCommand\n");
		throw UnexpectedSDOCommand(sdo->body.commandSpec.
					   segment.command);
	}

//	assert(serverSDODownload.data!=NULL);
	if( serverSDODownload.data==NULL )
		return;
	
	memcpy(((char*)serverSDODownload.data)+
	       serverSDODownload.currentPosition,
	       sdo->body.segment.data,
	       7-sdo->body.commandSpec.segment.n);
	serverSDODownload.currentPosition+=7-sdo->body.commandSpec.segment.n;
	if(serverSDODownload.t==sdo->body.commandSpec.segment.t)
	{ // error: ``t'' not changed!
		printf("abort\n");
		txSDO.body.commandSpec.init.command=
			AbortDomainTransfer;
		txSDO.body.init.index=serverSDODownload.index;
		txSDO.body.init.subindex=serverSDODownload.subindex;
		*((unsigned int*)(&txSDO.body.init.data))=
			TBitError;
		sendSDO(txSDO,Tx);
		return;
	}
	serverSDODownload.t=sdo->body.commandSpec.segment.t;
	if(sdo->body.commandSpec.segment.c)
	{ // last segment
//#warning SDO: Всё приняли, надо хотя бы память освободить
		if(!serverSDODownload.isUnstandard)
		{
			od.setValue(serverSDODownload.index,
				    serverSDODownload.subindex,
				    serverSDODownload.data);
		}
		free(serverSDODownload.data);
		serverSDODownload.data=NULL;
		serverSDODownload.isActive=false;
//		printf("Receive complete.\n");
	}

	txSDO.body.commandSpec.segment.t=serverSDODownload.t;
	txSDO.body.commandSpec.segment.command=DownloadDomainSegmentAnswer;

	sendSDO(txSDO,Tx);
	struct timezone tz;
	gettimeofday(&serverSDODownload.lastMsgTime,&tz);
//	printf("answer sended\n");
}


void CanOpenDeviceWithSDO::processingReceivedSDO(CanOpen::SDO*sdo)
{
	assert((sdo->cobID.functionCode==FcSDOtx)||
	       (sdo->cobID.functionCode==FcSDOrx));
	
	unsigned int sdoCobIDrx=0;
	unsigned int sdoCobIDtx=0;
	
	od.getValue(ObjectDictionary::SDOServerParm,0x01,&sdoCobIDrx);
	od.getValue(ObjectDictionary::SDOServerParm,0x02,&sdoCobIDtx);
//	printf("sdoCobIDrx: 0x%x; sdoCobIDtx: 0x%x\n",sdoCobIDrx,sdoCobIDtx);
	
//	printf("processingReceivedSDO: cobid: %x\n",sdo->cobID.cobID);
	if(sdo->cobID.functionCode&0x01)
	{ // принимаем сообщения от сервера (я клиент)
//		printf("SDO: message from server: upload[0x%02x]: %i\n",
//		       sdo->cobID.nodeID,clientSDOUpload[sdo->cobID.nodeID].isActive);
		if(clientSDOUpload[sdo->cobID.nodeID].isActive)
		{
//			printf("SDO: my nodeID; command: 0x%x\n",
//			       sdo->body.commandSpec.init.command);
			switch(sdo->body.commandSpec.init.command)
			{
			case InitiateDomainUploadAnswer:
//				printf("InitiateDomainUploadAnswer\n");
				clientSdoInitiateDomainUploadAnswer(sdo);
				break;
			case UploadDomainSegmentAnswer:
				if(clientSDOUpload[sdo->cobID.nodeID].len!=0)
				{
//					printf("UploadDomainSegmentAnswer\n");
					clientSdoUploadDomainSegment(sdo);
				}
				break;
			case AbortDomainTransfer:
//				printf("AbortDomainTransfer\n");
				break;
			default: // unexpected ;)
				printf("unexpected ;)\n");
				break;
			}
		}
		else if(clientSDODownload.isActive&&
			(sdo->cobID.nodeID==clientSDODownload.nodeID))
		{
			switch(sdo->body.commandSpec.init.command)
			{
			case InitiateDomainDownloadAnswer:
			case DownloadDomainSegmentAnswer:
				clientSdoDownloadDomainSegment(sdo);
				break;
			case AbortDomainTransfer:
				break;
			default: // unexpected ;)
				break;
			}
		}
		// все остальные чужие
	}
	else
	{
//		printf("SDO for server from client; sdoCobIDrx = 0x%x; command: 0x%x\n",
//		       sdoCobIDrx,sdo->body.commandSpec.init.command);
		if(sdo->cobID.cobID==sdoCobIDrx)
		{ // тут принимаем сообщения от клиентов (я сервер)
//			printf("SDO: my nodeID; command: 0x%x\n",
//			       sdo->body.commandSpec.init.command);
			switch(sdo->body.commandSpec.init.command)
			{
			case InitiateDomainDownloadRequest:
				// будем данные принимать
//				printf("InitiateDomainDownloadRequest\n");
				serverSdoInitiateDomainDownload(sdo);
				break;
			case DownloadDomainSegmentRequest:
//				printf("DownloadDomainSegmentRequest\n");
				serverSdoDownloadDomainSegment(sdo);
				break;

			case InitiateDomainUploadRequest:
//				printf("InitiateDomainUploadRequest\n");
				serverSdoInitiateDomainUpload(sdo);
				break;
			case UploadDomainSegmentRequest:
//				printf("UploadDomainSegmentRequest\n");
				serverSdoUploadDomainSegment(sdo);
				break;

			case AbortDomainTransfer:
//				printf("AbortDomainTransfer\n");
				if(serverSDOUpload.isActive&&
				   serverSDOUpload.nodeID==sdo->cobID.nodeID&&
				   serverSDOUpload.index==
				   sdo->body.init.index&&
				   serverSDOUpload.subindex==
				   sdo->body.init.subindex)
				{
					if(serverSDOUpload.data!=NULL)
					{
						free(serverSDOUpload.data);
						serverSDOUpload.data=NULL;
					}
					serverSDOUpload.isActive=0;
				}
				if(serverSDODownload.isActive&&
				   serverSDODownload.nodeID==sdo->cobID.nodeID&&
				   serverSDODownload.index==
				   sdo->body.init.index&&
				   serverSDODownload.subindex==
				   sdo->body.init.subindex)
				{
					if(serverSDODownload.data!=NULL)
					{
						free(serverSDODownload.data);
						serverSDODownload.data=NULL;
					}
					serverSDODownload.isActive=0;
				}
				break;

			case InitiateBlockDownloadRequest&EndBlockDownloadRequest:
				break;

			case InitiateBlockUploadRequest&EndBlockUploadAnswer:
				break;

			default:
				break;
			}
		}
	}
}

void CanOpenDeviceWithSDO::clientSdoInitiateDomainUpload(int nodeID,
						     unsigned short index,
						     unsigned char subindex)
{
	SDO sdo;

	if(clientSDOUpload[nodeID].isActive)
	{ // in description (that I have) doesn't said anything about
		// simultaneous SDO data transfering
		throw SDOTransferActive("Upload is already active");
	}

	sdo.cobID.nodeID=nodeID;
	sdo.body.init.index=index;
	sdo.body.init.subindex=subindex;
	sdo.body.commandSpec.init.command=InitiateDomainUploadRequest;
	
	clientSDOUpload[nodeID].isActive=1;
	clientSDOUpload[nodeID].isDataReceived=0;
	clientSDOUpload[nodeID].nodeID=nodeID;
	clientSDOUpload[nodeID].len=0;
	clientSDOUpload[nodeID].index=index;
	clientSDOUpload[nodeID].subindex=subindex;
	
//	printf("clientSdoInitiateDomainUpload: nID: %x; idx: %x; sidx:%x\n",nodeID,index,subindex);
	sendSDO(sdo,Rx);

	struct timezone tz;
	gettimeofday(&clientSDOUpload[nodeID].lastMsgTime,&tz);
}

void CanOpenDeviceWithSDO::clientSdoInitiateDomainUploadAnswer(CanOpen::SDO*sdo)
{
	SDO txSDO;

	int nodeID=sdo->cobID.nodeID;
	txSDO.cobID.cobID=clientSDOUpload[nodeID].nodeID;

//#warning Влепить проверку соответствия Index'а и Subindex'а

//	printf("nodeID: %x; rnID %x; My inds: %x, %x; recv inds: %x, %x\n",nodeID,clientSDOUpload[nodeID].nodeID,
//	       clientSDOUpload[nodeID].index,
//	       clientSDOUpload[nodeID].subindex,sdo->body.init.index,sdo->body.init.subindex);
	if(!((sdo->body.init.index==clientSDOUpload[nodeID].index)&&
	     (sdo->body.init.subindex==clientSDOUpload[nodeID].subindex)))
	{
		// Это не мой init
//		printf("alien sdo\n");
		return;
	}
	if(sdo->body.commandSpec.init.e==1)
	{ // короткие данные
//		unsigned int data=0;
		clientSDOUpload[nodeID].len=4-sdo->body.commandSpec.init.n;
		clientSDOUpload[nodeID].data=malloc(clientSDOUpload[nodeID].len);
		memcpy(clientSDOUpload[nodeID].data,sdo->body.init.data,
		       clientSDOUpload[nodeID].len);
//		printf("SDO: expidited data received: 0x%x\n",*(int*)clientSDOUpload[nodeID].data);
		clientSDOUpload[nodeID].isDataReceived=1;
		clientSdoOnDataUploaded(clientSDOUpload[nodeID].nodeID);
//#warning данные приняли, надо чего-то с ними сделать
	}
	else
	{ // длинные данные
		if(sdo->body.commandSpec.init.s==0)
		{ // reserved by CiA for future use
			printf("Неужели мы тут вылетели?!! :(\n");
			return;
		}
		clientSDOUpload[nodeID].isActive=1;
		clientSDOUpload[nodeID].isSegmented=1;
		clientSDOUpload[nodeID].index=sdo->body.init.index;
		clientSDOUpload[nodeID].subindex=sdo->body.init.subindex;
		memcpy(&clientSDOUpload[nodeID].len,
		       sdo->body.init.data,sizeof(unsigned int));
		clientSDOUpload[nodeID].currentPosition=0;
		clientSDOUpload[nodeID].data=malloc(clientSDOUpload[nodeID].len);
		clientSDOUpload[nodeID].t=0;

//		printf("clientSDOUpload.len: %x\n",clientSDOUpload[nodeID].len);
		
		txSDO.body.commandSpec.segment.t=clientSDOUpload[nodeID].t;
		txSDO.body.commandSpec.segment.command=UploadDomainSegmentRequest;

//	printf("init: before sendSDO(txSDO,Rx);\n");
		sendSDO(txSDO,Rx);
//	printf("init: before sendSDO(txSDO,Rx);\n");

		struct timezone tz;
		gettimeofday(&clientSDOUpload[nodeID].lastMsgTime,&tz);
	}
}


void CanOpenDeviceWithSDO::clientSdoUploadDomainSegment(CanOpen::SDO*rxSDO)
{
	SDO txSDO;

	int nodeID=rxSDO->cobID.nodeID;
	txSDO.cobID.nodeID=clientSDOUpload[nodeID].nodeID;
	
	if(!clientSDOUpload[nodeID].isActive)
	{
		printf("Unexpected command. (Not active)\n");
		throw UnexpectedSDOCommand(rxSDO->body.commandSpec.
					   segment.command);
	}

//	assert(clientSDOUpload[nodeID].data!=NULL);
	if( clientSDOUpload[nodeID].data==NULL )
		return;
	
//	printf("clientSDOUpload.data: %x; clientSDODownload.currentPosition: %x\n",
//	       clientSDOUpload[nodeID].data,
//	       clientSDOUpload[nodeID].currentPosition);
	memcpy(((char*)clientSDOUpload[nodeID].data)+
	       clientSDOUpload[nodeID].currentPosition,
	       rxSDO->body.segment.data,
	       7-rxSDO->body.commandSpec.segment.n);
	clientSDOUpload[nodeID].currentPosition+=
		7-rxSDO->body.commandSpec.segment.n;
	if(clientSDOUpload[nodeID].t!=rxSDO->body.commandSpec.segment.t)
	{ // error: ``t'' not changed!
		txSDO.body.commandSpec.init.command=
			AbortDomainTransfer;
		txSDO.body.init.index=clientSDOUpload[nodeID].index;
		txSDO.body.init.subindex=clientSDOUpload[nodeID].subindex;
		*((unsigned int*)(&txSDO.body.init.data))=
			TBitError;
		sendSDO(txSDO,Rx);
		if(clientSDOUpload[nodeID].data!=NULL)
		{
			free(clientSDOUpload[nodeID].data);
		}
		clientSDOUpload[nodeID].isActive=0;
		printf("t-error\n");
		return;
	}
	clientSDOUpload[nodeID].t=~clientSDOUpload[nodeID].t;
	if(rxSDO->body.commandSpec.segment.c)
	{ // last segment
//		clientSDOUpload.isActive=false;

//#warning SDO: Всё приняли, надо хотя бы память освободить

//		printf("Received data: %qx\n",
//		       *(long long*)clientSDOUpload[nodeID].data);

//		free(clientSDOUpload.data);
//		clientSDOUpload.data=NULL;
		clientSDOUpload[nodeID].isDataReceived=1;
		clientSdoOnDataUploaded(clientSDOUpload[nodeID].nodeID);
		
		return;
	}

	txSDO.body.commandSpec.segment.t=clientSDOUpload[nodeID].t;
	txSDO.body.commandSpec.segment.command=UploadDomainSegmentRequest;

//	printf("before sendSDO(txSDO,Rx);\n");
	sendSDO(txSDO,Rx);
//	printf("after sendSDO(txSDO,Rx);\n");

	struct timezone tz;
	gettimeofday(&clientSDOUpload[nodeID].lastMsgTime,&tz);
}

int CanOpenDeviceWithSDO::clientSdoIsDataUploaded(int nodeID)
{
	if(clientSDOUpload[nodeID].isDataReceived)
	{
		return clientSDOUpload[nodeID].len;
	}
	else
	{
		return 0;
	}
}

void CanOpenDeviceWithSDO::clientSdoGetUploadedData(int nodeID,void*data)
{
	if(!clientSDOUpload[nodeID].isActive)
	{
		throw SDOTransferActive("clientSdoGetUploadedData: transfer is NOT active!");
	}
	if(!clientSDOUpload[nodeID].isDataReceived)
	{
		throw SDOTransferActive("clientSdoGetUploadedData: data is not received!");
	}
	memcpy(data,clientSDOUpload[nodeID].data,clientSDOUpload[nodeID].len);
	free(clientSDOUpload[nodeID].data);
	clientSDOUpload[nodeID].data=NULL;
	clientSDOUpload[nodeID].isActive=0;
	clientSDOUpload[nodeID].isDataReceived=0;
//	printf("clientSdoGetUploadedData[%i]: clientSDOUpload.isActive: %i\n",nodeID,clientSDOUpload[nodeID].isActive);
}


void CanOpenDeviceWithSDO::clientUnstandardUploadSDO(CanOpen::SDO*sdo,void*data)
{
}

void CanOpenDeviceWithSDO::serverSdoInitiateDomainUpload(CanOpen::SDO*rxSDO)
{
	SDO sdo;
	unsigned int len;

//	printf("!!!!!!!!!\n");
/*	if(serverSDOUpload.isActive)
	{ // in description (that I have) doesn't said anything about
		// simultaneous SDO data transfering
//		throw SDOTransferActive("Server Upload is already active");
	}*/
//	printf("@@@@@@@@@@@@@@@@@\n");

	sdo.cobID.nodeID=rxSDO->cobID.nodeID;
	sdo.body.init.index=rxSDO->body.init.index;
	sdo.body.init.subindex=rxSDO->body.init.subindex;
	sdo.body.commandSpec.init.command=InitiateDomainUploadAnswer;

	SDOTransfer serverSDOUpl;

	try
	{
		len=(od.getValueBitSize(rxSDO->body.init.index,
					rxSDO->body.init.subindex)+7)/8;
	}
	catch(ObjectSizeIsVariable)
	{
		len=serverUnstandardSdoUpload(rxSDO->body.init.index,
					      rxSDO->body.init.subindex);
		serverSDOUpl.isUnstandard=true;
	}


	printf("len: %x\n",len);
	if(len==0)
		return;
/*	if(serverSDOUpload.isActive)
	{
		throw SDOTransferActive("Server Upload is already active");
	}*/

	serverSDOUpl.isActive=true;
	serverSDOUpl.len=len;
	serverSDOUpl.data=malloc(len);
	od.getValue(rxSDO->body.init.index,rxSDO->body.init.subindex,
		 serverSDOUpl.data);
	serverSDOUpl.nodeID=rxSDO->cobID.nodeID;
	serverSDOUpl.index=rxSDO->body.init.index;
	serverSDOUpl.subindex=rxSDO->body.init.subindex;
	
	serverSdoUploadDataAccepted(sdo.body.init.index,
				    sdo.body.init.subindex);

	if(serverSDOUpload.isActive)
	{
		serverSDOUpQueue.push(serverSDOUpl);
		throw SDOTransferActive("Server Upload is already active, request queued ");
	}
	else
	{
		serverSDOUpload=serverSDOUpl;
	}

	if(len<4)
	{ // expedited transfer
		sdo.body.commandSpec.init.e=1;
		sdo.body.commandSpec.init.n=4-len;
		sdo.body.commandSpec.init.s=0;
		memcpy(sdo.body.init.data,serverSDOUpload.data,len);
		serverSDOUpload.isSegmented=false;
		serverSdoUploaded(sdo.body.init.index,sdo.body.init.subindex);
		if(serverSDOUpload.data!=NULL)
			free(serverSDOUpload.data);
		serverSDOUpload.data=NULL;
		serverSDOUpload.isActive=false;
	}
	else
	{ // normal (segmented) transfer
		sdo.body.commandSpec.init.e=0;
		sdo.body.commandSpec.init.n=0;
		sdo.body.commandSpec.init.s=1;
		memcpy(sdo.body.init.data,&len,4);
		
		serverSDOUpload.isSegmented=true;
		serverSDOUpload.currentPosition=0;
		serverSDOUpload.t=0;
	}

	sendSDO(sdo,Tx);

	struct timezone tz;
	gettimeofday(&serverSDOUpload.lastMsgTime,&tz);

	if(serverSDOUpload.isActive==false)
	{
		initServerSDOUploadFromQueue();
	}
}

void CanOpenDeviceWithSDO::initServerSDOUploadFromQueue()
{
	SDO sdo;
	unsigned int len;
	
	if(serverSDOUpQueue.empty())
		return;
	
	SDOTransfer serverSDOUpl=serverSDOUpQueue.front();
	serverSDOUpQueue.pop();
	
	len=serverSDOUpl.len;
	serverSDOUpload=serverSDOUpl;
	serverSDOUpload.isActive=true;
	
	sdo.cobID.nodeID=serverSDOUpload.nodeID;
	sdo.body.init.index=serverSDOUpload.index;
	sdo.body.init.subindex=serverSDOUpload.subindex;
	sdo.body.commandSpec.init.command=InitiateDomainUploadAnswer;

	if(len<4)
	{ // expedited transfer
		sdo.body.commandSpec.init.e=1;
		sdo.body.commandSpec.init.n=4-len;
		sdo.body.commandSpec.init.s=0;
		memcpy(sdo.body.init.data,serverSDOUpload.data,len);
		serverSDOUpload.isSegmented=false;
		serverSdoUploaded(sdo.body.init.index,sdo.body.init.subindex);
		if(serverSDOUpload.data!=NULL)
			free(serverSDOUpload.data);
		serverSDOUpload.data=NULL;
		serverSDOUpload.isActive=false;
	}
	else
	{ // normal (segmented) transfer
		sdo.body.commandSpec.init.e=0;
		sdo.body.commandSpec.init.n=0;
		sdo.body.commandSpec.init.s=1;
		memcpy(sdo.body.init.data,&len,4);
		
		serverSDOUpload.isSegmented=true;
		serverSDOUpload.currentPosition=0;
		serverSDOUpload.t=0;
	}

	sendSDO(sdo,Tx);

	struct timezone tz;
	gettimeofday(&serverSDOUpload.lastMsgTime,&tz);

	if(serverSDOUpload.isActive==false)
	{
		initServerSDOUploadFromQueue();
	}
}


void CanOpenDeviceWithSDO::serverSdoUploadDomainSegment(CanOpen::SDO*rxSDO)
{
	if(!serverSDOUpload.isActive)
	{
		printf("UnexpectedSDOCommand\n");
		throw UnexpectedSDOCommand(rxSDO->body.commandSpec.
					   segment.command);
	}

	SDO sdo;
	unsigned int length,xlen;
	
	sdo.cobID.nodeID=serverSDOUpload.nodeID;

	sdo.body.commandSpec.segment.t=rxSDO->body.commandSpec.segment.t;//serverSDOUpload.t;
	serverSDOUpload.t=~rxSDO->body.commandSpec.segment.t;

	sdo.body.commandSpec.segment.command=UploadDomainSegmentAnswer;
	
	length=serverSDOUpload.len-serverSDOUpload.currentPosition;
	xlen=(length>7)?7:length;
	
	sdo.body.commandSpec.segment.n=7-xlen;
	sdo.body.commandSpec.segment.c=(length>7)?0:1;

	memcpy(sdo.body.segment.data,
	       (char*)serverSDOUpload.data+
	       serverSDOUpload.currentPosition,xlen);
	serverSDOUpload.currentPosition+=xlen;
//	printf("sdo.body.commandSpec.segment.c: %i\n",sdo.body.commandSpec.segment.c);
	if(sdo.body.commandSpec.segment.c)
	{
		serverSdoUploaded(serverSDOUpload.index,
				  serverSDOUpload.subindex);
		free(serverSDOUpload.data);
		serverSDOUpload.data=NULL;
		serverSDOUpload.isActive=false;
	}
	
//	printf("before sendSDO(sdo,Tx);\n");
	sendSDO(sdo,Tx);
//	printf("after sendSDO(sdo,Tx);\n");


	struct timezone tz;
	gettimeofday(&serverSDOUpload.lastMsgTime,&tz);

	if(serverSDOUpload.isActive==false)
	{
		initServerSDOUploadFromQueue();
	}
}
