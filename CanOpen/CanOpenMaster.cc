
#include "CanOpenMaster.h"
#include "EDSParser.h"
#include "CanOpenMessages.h"
#include "CanExceptions.h"


using namespace CanOpen;

CanOpenMaster::CanOpenMaster(SimpleCanDevice*device):
CanOpenDeviceWithSDO(device)
{
}

void CanOpenMaster::sendSync()
{
	Message msg;

	msg.cob_id.w=0x0080;
	msg.rtr=0x00;
	msg.len=0;
	
	simpleDevice->sendMessage(&msg);
}

void CanOpenMaster::sendNMT(int NodeID, NMTCommandSpecifier cs)
{
	Message msg;

	msg.cob_id.w=0x0000;
	msg.rtr=0x00;
	msg.len=2;
	msg.data[0]=cs;
	msg.data[1]=(char)NodeID;
	
	simpleDevice->sendMessage(&msg);
}

void CanOpenMaster::sendNodeGuard(int nodeID)
{
	Message msg;

	msg.cob_id.w=nodeID|(FcNodeGuard<<7);
	msg.rtr=0xff;
	msg.len=0;
	
	simpleDevice->sendMessage(&msg);
}

void CanOpenMaster::processingReceivedPDO(CanOpen::PDO*pdo)
{
//	map<unsigned char,CanOpen::ObjectDictionary>::iterator
//		slIt=slavesODs.begin();
	unsigned char nodeID;
//	unsigned int myCobID,k;

	if(pdo->cobID.pdoCobID.pdoDirection==1)
	{
		nodeID=myTxPDOCobID[pdo->cobID.cobID];
	}
	else
	{
		nodeID=myRxPDOCobID[pdo->cobID.cobID];
		if(nodeID==0)
		{
			int n=0;
			n=od.findRxPDONoByCobID(pdo->cobID.cobID);
			if(n!=0)
			{
				nodeID=od.getNodeID();
			}
		}
	}

	if(nodeID==0)
	{
		// alien's slave
		printf("alien's slave\n");
		return;
	}

/*	for(;slIt!=slavesODs.end();slIt++)
	{
		if(pdo->cobID.pdoCobID.nodeID==slIt->first)
		{
			nodeID=slIt->first;
			break;
		}
	}
	if(!nodeID)
	{
		for(slIt=slavesODs.begin();slIt!=slavesODs.end();slIt++)
		{
			for(k=1;k<=MaxPDONum;k++)
			{
				try
				{
					if(pdo->cobID.pdoCobID.pdoDirection==1)
					{
						myCobID=slIt->
							second.getTxPDO_COB_ID(k);
					}
					else
					{
						myCobID=slIt->
							second.getRxPDO_COB_ID(k);
					}
					if(pdo->cobID.cobID==myCobID)
					{
						nodeID=slIt->first;
						break;
					}
				}
				catch(CanException)
				{
					break;
				}
			}
			if(nodeID)
			{
				break;
			}
		}
	}

	if(slIt==slavesODs.end())
	{
		// alien's slave
		return;
	}*/
	
//	unsigned char nodeID=pdo->cobID.pdoCobID.nodeID;
	
	if(pdo->cobID.pdoCobID.pdoDirection==1)
	{
		mapTxPDOtoOD(pdo,&slavesODs[nodeID]);
	}
	else
	{
		if(nodeID==od.getNodeID())
		{
			mapRxPDOtoOD(pdo,&od);
		}
		else
		{
			mapRxPDOtoOD(pdo,&slavesODs[nodeID]);
		}
	}
}

void CanOpenMaster::addSlave(int nodeID,std::string filename)
{
	EDSParser eds(filename);
	ObjectDictionary sod;
	
	eds.fillOD(&sod);
	
	sod.setNodeID(nodeID);
	
	slavesODs[nodeID]=sod;
//	mySlaves.add(nodeID);

	int k;
	
	for(k=1;k<=MaxPDONum;k++)
	{
		try
		{
			myRxPDOCobID[sod.getRxPDO_COB_ID(k)]=nodeID;
		}
		catch(CanException)
		{
			break;
		}
	}
	for(k=1;k<=MaxPDONum;k++)
	{
		try
		{
			myTxPDOCobID[sod.getTxPDO_COB_ID(k)]=nodeID;
		}
		catch(CanException)
		{
			break;
		}
	}

}

int CanOpenMaster::getSlavesIntValue(int nodeID, unsigned short index,
				 unsigned char subindex)
{
	map<unsigned char,CanOpen::ObjectDictionary>::iterator
		slIt=slavesODs.begin();
	
	for(;slIt!=slavesODs.end();slIt++)
	{
		if(nodeID==slIt->first)
		{
			break;
		}
	}
	
	if(slIt==slavesODs.end())
	{
		// incorrect slave's No
		return -1;
	}
	
	int value=0;
	DataType type=slIt->second.getDataType(index,subindex);
	                                        
	switch(type)
	{
	case Integer8:
	case Integer16:
	case Integer32:
	case Integer24:
	case Unsigned8:
	case Unsigned16:
	case Unsigned32:
	case Unsigned24:
		slIt->second.getValue(index,subindex,&value);
		return value;
		break;
	default:
		// not integer type
		throw IncompatibleTypes(type, Integer32);
		break;
	}
	return 0;
}

void CanOpenMaster::setSlavesIntValue(int nodeID, unsigned short index,
				  unsigned char subindex, int value)
{
	map<unsigned char,CanOpen::ObjectDictionary>::iterator
		slIt=slavesODs.begin();
	
	for(;slIt!=slavesODs.end();slIt++)
	{
		if(nodeID==slIt->first)
		{
			break;
		}
	}
	
	if(slIt==slavesODs.end())
	{
		// incorrect slave's No
		return;
	}
	
	DataType type=slIt->second.getDataType(index,subindex);
	                                        
	switch(type)
	{
	case Integer8:
	case Integer16:
	case Integer32:
	case Integer24:
	case Unsigned8:
	case Unsigned16:
	case Unsigned32:
	case Unsigned24:
		slIt->second.setValue(index,subindex,&value);
		break;
	default:
		// not integer type
		throw IncompatibleTypes(type, Integer32);
		break;
	}
}

/*void CanOpenMaster::processingReceivedSDO(CanOpen::SDO*sdo)
{
}*/

bool CanOpenMaster::getSlavesBitValue(int nodeID, unsigned short index,
				  unsigned char subindex)
{
	map<unsigned char,CanOpen::ObjectDictionary>::iterator
		slIt=slavesODs.begin();
	
	for(;slIt!=slavesODs.end();slIt++)
	{
		if(nodeID==slIt->first)
		{
			break;
		}
	}
	
	if(slIt==slavesODs.end())
	{
		// incorrect slave's No
		return -1;
	}
	
	char value=0;
	DataType type=slIt->second.getDataType(index,subindex);
	                                        
	switch(type)
	{
	case Boolean:
		slIt->second.getValue(index,subindex,&value);
		return value;
		break;
	default:
		// not boolean type
		throw IncompatibleTypes(type, Boolean);
		break;
	}
	return 0;
}

void CanOpenMaster::setSlavesBitValue(int nodeID, unsigned short index,
				  unsigned char subindex, bool value)
{
	unsigned char zval=value?1:0;

	map<unsigned char,CanOpen::ObjectDictionary>::iterator
		slIt=slavesODs.begin();
	
	for(;slIt!=slavesODs.end();slIt++)
	{
		if(nodeID==slIt->first)
		{
			break;
		}
	}
	
	if(slIt==slavesODs.end())
	{
		// incorrect slave's No
		return;
	}
	
	DataType type=slIt->second.getDataType(index,subindex);
	                                        
	switch(type)
	{
	case Boolean:
		slIt->second.setValue(index,subindex,&zval);
		break;
	default:
		// not boolean type
		throw IncompatibleTypes(type, Boolean);
		break;
	}
}

std::list<int> CanOpenMaster::getSlavesNodeIDs()
{
	std::list<int> ids;

	map<unsigned char,CanOpen::ObjectDictionary>::const_iterator
		slIt=slavesODs.begin();
	
	for(;slIt!=slavesODs.end();slIt++)
	{
		ids.push_back(slIt->first);

	}
	
	return ids;
}

CanOpen::DataType CanOpenMaster::getSlavesDataType(int nodeID,
					       unsigned short index,
					       unsigned char subindex)
{
	map<unsigned char,CanOpen::ObjectDictionary>::iterator
		slIt=slavesODs.begin();
	
	for(;slIt!=slavesODs.end();slIt++)
	{
		if(nodeID==slIt->first)
		{
			break;
		}
	}
	
	if(slIt==slavesODs.end())
	{
		// incorrect slave's No
		return (DataType)-1;
	}
	
	return slIt->second.getDataType(index,subindex);
}

void CanOpenMaster::processingNMTNodeGuard(CanOpen::NMTGuardMessage*nmt)
{
	if(nmt->rtr==0x00)
	{
		struct timeval tv;
		struct timezone tz;
		gettimeofday(&tv,&tz);
		slavesStates[nmt->cobID.nodeID].timeStamp=tv;
		slavesStates[nmt->cobID.nodeID].state=nmt->state;
	}
	CanOpenDevice::processingNMTNodeGuard(nmt);
}

CanOpenMaster::SlaveState CanOpenMaster::getNodeState(int nodeID)
{
	map<unsigned char,SlaveState>::const_iterator ssIt=
		slavesStates.find(nodeID);
	if(ssIt==slavesStates.end())
	{
		throw CanException("Slave's state not found");
	}

	return ssIt->second;
}
