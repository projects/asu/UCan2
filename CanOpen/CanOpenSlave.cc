
#include "CanOpenSlave.h"

#include "CanHelpers.h"

#include "ArbraCan.h"
#include "CanOpenMessages.h"

using namespace CanOpen;

CanOpenSlave::CanOpenSlave(SimpleCanDevice*device):
CanOpenDevice(device),nmtTBit(false),currentState(Initialising)
{
}

CanOpenSlave::~CanOpenSlave()
{
}


void CanOpenSlave::processingReceivedPDO(CanOpen::PDO*pdo)
{
	int k,num,nbit;
	PDOMapObject mObj;
	void*data;

	if(pdo->cobID.pdoCobID.pdoDirection&0x01)
	{
		//txPDO
		// we does not want receive alien's txPDOs
		return;
	}

	//rxPDO
	for(k=1;k<5;k++)
	{
		if(pdo->cobID.cobID==od.getRxPDO_COB_ID(k))
		{
//			processingRxPDO(pdo);
			break;
		}
	}
	if(k<5)
	{
		mapRxPDOtoOD(pdo,&od);
	}
}

void CanOpenSlave::sendNodeGuard(int nodeID,NMTNodeState state)
{
	Message msg;

	msg.cob_id.w=nodeID|(FcNodeGuard<<7);
	msg.rtr=0x00;
	msg.len=1;
	msg.data[0]=state|(nmtTBit?0x80:0x00);
	
	simpleDevice->sendMessage(&msg);
}

void CanOpenSlave::processingNMT(CanOpen::NMTMessage nmt)
{
	if((nmt.nodeID==0)||
	   (nmt.nodeID==od.getNodeID()))
	{
		switch(nmt.cs)
		{
		case StartRemoteNode:
			currentState=Operational;
			break;
		case StopRemoteNode:
			currentState=Stopped;
			break;
		case EnterPreoperationalState:
			currentState=PreOperational;
			break;
		case ResetNode:
			currentState=Initialising;
			break;
		case ResetCommunication:
			break;
		default:
			break;
		}
	}
}

void CanOpenSlave::processingNMTNodeGuard(CanOpen::CobID cobID)
{
}	
