
#include "ConfParser.h"
#include "CanExceptions.h"

#include <errno.h>
//#include <error.h>
#include <string.h>

using namespace std;

ConfParser::ConfParser(string filename)
{
	confFile=fopen(filename.c_str(),"rb");
	if(confFile==NULL)
	{
		string strErr="Can't open conf file: "+filename+
			" [Error: "+strerror(errno)+"]";
		throw CanOpen::CanException(strErr.c_str());
	}
}

ConfParser::~ConfParser()
{
	fclose(confFile);
}

bool ConfParser::checkSignificanceOfLine(string str)
{
	if(str=="")
		return false;

	switch(str[0])
	{
	case ' ':
	case '\n':
	case '\r':
	case '\t':
	case ';':
		return false;
		break;
//		assert(3==4);
//		break;
	default:
		return true;
		break;
	}
	return false;
}

std::string ConfParser::getNextSignificantLine()
{
	char str[256];
	string result="";

	for(;!feof(confFile);)
	{
		fgets(str,255,confFile);
		if(checkSignificanceOfLine(str))
		{
			result=str;
			break;
		}
	}
	return result;
}


void ConfParser::parseKeyLine(std::string str,
			      std::string*key,std::string*value)
{
	int pos;
	pos=str.find('=');
	*key=str.substr(0,pos);
	*value=str.substr(pos+1);
}


ConfParser::StringType ConfParser::getNextString(std::string*str)
{
	string xstr=getNextSignificantLine();
	int pos;
	
	if(xstr=="")
		return EndOfFile;

	if(xstr[0]=='[')
	{
		*str=xstr.substr(1,xstr.find(']')-1);
		return Section;
	}
	
	for(pos=xstr.length()-1;pos;pos--)
	{
		if((xstr[pos]==' ')||(xstr[pos]=='\t')||(xstr[pos]=='\n')
		   ||(xstr[pos]=='\r'))
		{
		}
		else
		{
			break;
		}
	}
	
	*str=xstr.substr(0,pos+1);
	return KeyLine;
}
