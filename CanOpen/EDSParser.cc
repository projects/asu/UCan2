#include <cstdlib>
#include <cstring>

#include <assert.h>
#include "EDSParser.h"
//#include "Exceptions.h"

#include <errno.h>
//#include <error.h>
#include <string.h>
#include <ctype.h>

#include <iostream>

using namespace std;
using namespace CanOpen;

EDSParser::EDSParser(string filename):
ConfParser(filename),currentSection(""),currentSectionType(NoSection),
curOD(NULL)
{
	pFunctions["FileInfo"].secFunction=&EDSParser::processFileInfo;
	pFunctions["FileInfo"].keyFunction=&EDSParser::processFileInfoKey;

	pFunctions["DeviceInfo"].secFunction=&EDSParser::processDeviceInfo;
	pFunctions["DeviceInfo"].keyFunction=&EDSParser::processDeviceInfoKey;

	pFunctions["StandardDataTypes"].secFunction=
		&EDSParser::processStandardDataTypes;
	pFunctions["StandardDataTypes"].keyFunction=
		&EDSParser::processStandardDataTypesKey;
		
	pFunctions["DummyUsage"].secFunction=&EDSParser::processDummyUssage;
	pFunctions["DummyUsage"].keyFunction=
		&EDSParser::processDummyUssageKey;

	pFunctions["MandatoryObjects"].secFunction=
		&EDSParser::processMandatoryObjects;
	pFunctions["MandatoryObjects"].keyFunction=
		&EDSParser::processMandatoryObjectsKey;

	pFunctions["OptionalObjects"].secFunction=
		&EDSParser::processOptionalObjects;
	pFunctions["OptionalObjects"].keyFunction=
		&EDSParser::processOptionalObjectsKey;

	pFunctions["ManufacturerObjects"].secFunction=
		&EDSParser::processManufacturerObjects;
	pFunctions["ManufacturerObjects"].secFunction=
		&EDSParser::processManufacturerObjectsKey;
}

void EDSParser::fillOD(CanOpen::ObjectDictionary*od)
{
	string str;
	
	curOD=od;

	for(;;)
	{
		switch(getNextString(&str))
		{
		case Section:
			if(currentSectionType!=NoSection)
			{
				processEndOfSection();
			}
			processSectionName(str);
			break;
		case KeyLine:
			processKeyLine(str);
			break;
		case EndOfFile:
			if(currentSectionType!=NoSection)
			{
				processEndOfSection();
			}
			curOD=NULL;
			return;
			break;
		default:
			break;
		}
	}
	curOD=NULL;
}

void EDSParser::processKeyLine(std::string str)
{
	assert(currentSectionType!=NoSection);

//	printf("EDSParser::processKeyLine(%s)\n",str.c_str());
	switch(currentSectionType)
	{
	case StringSection:
		{
//			printf("EDSParser::currentSection: \'%s\'\n",
//			       currentSection.c_str());
			map<std::string,ParserFunctions>::const_iterator sfIt;
			sfIt=pFunctions.find(currentSection);
			assert(sfIt!=pFunctions.end());
			
			(this->*(sfIt->second.keyFunction))(str);
			
		}
		break;
	case IndexSection:
		processIndexSectionKey(str);
		break;
	case IndexSubindexSection:
		processIndexSubindexSectionKey(str);
		break;
	default:
		assert(str=="processKeyLine");
		break;
	}
}


void EDSParser::processSectionName(std::string sName)
{
	if(isdigit(sName[0]))
	{
		processIndexSection(sName);
		return;
	}
	
	map<std::string,ParserFunctions>::const_iterator sfIt;
	sfIt=pFunctions.find(sName);
	if(sfIt==pFunctions.end())
	{
		processUnknownSection(sName);
	}
	else
	{
#warning а это мы зачем закомментировали?
//		(this->*(sfIt->second.secFunction))(sName);
		currentSectionType=StringSection;
		currentSection=sName;
	}
}

void EDSParser::processEndOfSection()
{
	assert(curOD!=NULL);

//	printf("EDSParser::endOfSection\n");
	
	switch(currentSectionType)
	{
	case IndexSection:
		if(subNumber==0)
		{
			curOD->addEntry(*odEntry);
			if(isDefaultValue)
			{
				curOD->setValue(odEntry->index,odEntry->subindex,
						odEntry->defaultValue);
			}
		}
		{
			CanOpen::ObjectDescription odDesc;
			odDesc.index=odEntry->index;
			odDesc.subNumber=subNumber;
			odDesc.objectName=odEntry->objectName;
			odDesc.objectType=odEntry->objectType;
			odDesc.dataType=odEntry->dataType;
			odDesc.attr=odEntry->attr;
			odDesc.isPDOmap=odEntry->isPDOmap;
			curOD->setObjectDescription(odDesc.index,odDesc);
		}
		delete odEntry;
		break;
	case IndexSubindexSection:
		curOD->addEntry(*odEntry);
		if(isDefaultValue)
		{
			curOD->setValue(odEntry->index,odEntry->subindex,
					odEntry->defaultValue);
		}
		delete odEntry;
		break;
	default:
		break;
	}
}


void EDSParser::processIndexSection(std::string sName)
{
	int index,subindex;

	isDefaultValue=false;
	switch(sscanf(sName.c_str(),"%xsub%x",&index,&subindex))
	{
	case 1:
		odEntry=new ObjectDictionaryEntry;
		currentSectionType=IndexSection;
		subNumber=0;
		odEntry->index=index;
		odEntry->subindex=0;
		break;
	case 2:
		odEntry=new ObjectDictionaryEntry;
		currentSectionType=IndexSubindexSection;
		odEntry->index=index;
		odEntry->subindex=subindex;
		break;
	default:
		assert(sName=="processIndexSection");
		break;
	}
	currentSection=sName;
}

void EDSParser::processUnknownSection(std::string sName)
{
	cout<<"EDS: Unknown section: "<<sName<<endl;
//	assert(sName=="processUnknownSection");
}

void EDSParser::processFileInfoKey(std::string keyLine)
{
}

void EDSParser::processDeviceInfoKey(std::string keyLine)
{
}

void EDSParser::processStandardDataTypesKey(std::string keyLine)
{
}

void EDSParser::processDummyUssageKey(std::string keyLine)
{
}

void EDSParser::processMandatoryObjectsKey(std::string keyLine)
{
}

void EDSParser::processOptionalObjectsKey(std::string keyLine)
{
}

void EDSParser::processManufacturerObjectsKey(std::string keyLine)
{
}

void EDSParser::processIndexSectionKey(std::string keyLine)
{
	string key,value;
	
	parseKeyLine(keyLine,&key,&value);

	if(key=="SubNumber")
	{
		sscanf(value.c_str(),"%i",&subNumber);
	}
	else
	{
		processIndexSubindexSectionKey(keyLine);
	}
}

void EDSParser::processIndexSubindexSectionKey(std::string keyLine)
{
	string key,value;
	int n;
	
	parseKeyLine(keyLine,&key,&value);
	
	if(key=="ParameterName")
	{
		odEntry->objectName=value;
	}
	else if(key=="ObjectType")
	{
		n=sscanf(value.c_str(),"%i",&odEntry->objectType);
//		printf("ObjectType: n: %i\n",n);
		assert(n==1);
	}
	else if(key=="DataType")
	{
		n=sscanf(value.c_str(),"%i",&odEntry->dataType);
		assert(n==1);
	}
	else if(key=="LowLimit")
	{
		sscanf(value.c_str(),"%i",&odEntry->minimum);
//		setValue(value,odEntry->dataType,&odEntry->minimum);
	}
	else if(key=="HighLimit")
	{
		sscanf(value.c_str(),"%i",&odEntry->maximum);
	}
	else if(key=="DefaultValue")
	{
		if(setValue(value,odEntry->dataType,&odEntry->defaultValue))
		{
			isDefaultValue=true;
		}
	}
	else if(key=="AccessType")
	{
		if(value=="ro")
			odEntry->attr=ro;
		else if(value=="rw")
			odEntry->attr=rw;
		else if(value=="wo")
			odEntry->attr=wo;
		else if(value=="con")
			odEntry->attr=con;
	}
	else if(key=="PDOMapping")
	{
		int x;
		sscanf(value.c_str(),"%i",&x);
		odEntry->isPDOmap=x;
	}

}

int EDSParser::setValue(std::string str,CanOpen::DataType dType,void**value)
{
	assert(dType!=0);

	if(str.size()==0)
	{
		return 0;
	}
	
	switch(dType)
	{
	case Boolean:
	case Integer8:
	case Integer16:
	case Integer32:
	case Unsigned8:
	case Unsigned16:
	case Unsigned32:
	case Integer24:
	case Integer40:
	case Integer48:
	case Integer56:
	case Integer64:
	case Unsigned24:
	case Unsigned40:
	case Unsigned48:
	case Unsigned56:
	case Unsigned64:
		{
#warning Unportable variable definition & conversion in sscanf!
			long long x=0;
			sscanf(str.c_str(),"%qi",&x);
			*value=malloc(curOD->typeSize(dType));
//			printf("x: %x\n",x);
			memcpy(*value,&x,curOD->typeSize(dType));
		}
		break;
	case Real32:
	case Real64:
		return 0;
		break;
	case VisibleString:
		*value=malloc(strlen(str.c_str())+1);
		strcpy((char*)*value,str.c_str());
		break;
	case OctetString:
	case UnicodeString:
	case TimeOfDay:
	case TimeDifference:
	case BitString:
	case Domain:
		return 0;
		break;
	default:
		return 0;
		break;
	}
	return 1;
}

//////////////////////////////////////
// Unused functions
//////////////////////////////////////
void EDSParser::processFileInfo(std::string sName)
{
}

void EDSParser::processDeviceInfo(std::string sName)
{
}

void EDSParser::processStandardDataTypes(std::string sName)
{
}

void EDSParser::processDummyUssage(std::string sName)
{
}

void EDSParser::processMandatoryObjects(std::string sName)
{
}

void EDSParser::processOptionalObjects(std::string sName)
{
}

void EDSParser::processManufacturerObjects(std::string sName)
{
}
