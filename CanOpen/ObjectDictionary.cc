#include <cstdlib>
#include <cstring>

#include <assert.h>
#include "ObjectDictionary.h"
#include "CanExceptions.h"

using namespace CanOpen;

ObjectDictionary::ObjectDictionary()
{
	dataSize[Boolean]=OneBit;
	dataSize[Integer8]=1;
	dataSize[Integer16]=2;
	dataSize[Integer32]=4;
	dataSize[Unsigned8]=1;
	dataSize[Unsigned16]=2;
	dataSize[Unsigned32]=4;
	dataSize[Real32]=4;
	dataSize[VisibleString]=Variable;
	dataSize[OctetString]=Variable;
	dataSize[UnicodeString]=Variable;
	dataSize[TimeOfDay]=Unknown;
	dataSize[TimeDifference]=Unknown;
	dataSize[BitString]=Variable;
	dataSize[Domain]=Unknown;
	dataSize[Integer24]=3;
	dataSize[Real64]=8;
	dataSize[Integer40]=5;
	dataSize[Integer48]=6;
	dataSize[Integer56]=7;
	dataSize[Integer64]=8;
	dataSize[Unsigned24]=3;
	dataSize[Unsigned40]=5;
	dataSize[Unsigned48]=6;
	dataSize[Unsigned56]=7;
	dataSize[Unsigned64]=8;
	
}

int ObjectDictionary::typeSize(DataType dataType)
{
	return dataSize[dataType];
}

DataType ObjectDictionary::getDataType(unsigned short index,
				       unsigned char subindex)
{
	const ObjectDictionaryEntry* exEntry = getEntry(index,subindex);
	return exEntry->dataType;
}


void ObjectDictionary::addEntry(ObjectDictionaryEntry entry)
{
	ODIndices indices;

	indices.indices=0;
	indices.index=entry.index;
	indices.subindex=entry.subindex;
	
	const ObjectDictionaryEntry*exEntry;

	try
	{
		exEntry=getEntry(entry.index,entry.subindex);
		if(exEntry->value!=NULL)
		{
			free(exEntry->value);
		}
		if(exEntry->defaultValue!=NULL)
		{
			free(exEntry->defaultValue);
		}
	}
	catch(ODEntryNotFound)
	{
		// if OD entry already present we simply overwrite it.
		// is it correct? i don't know
	}

	lastIdx.indices=0;
	lastEntry=NULL;

	dictionary[indices.indices]=entry;
}

void ObjectDictionary::deleteEntry(unsigned short index,
				   unsigned char subindex)
{
	ODIndices indices;

	indices.indices=0;
	indices.index=index;
	indices.subindex=subindex;
	
	const ObjectDictionaryEntry*exEntry;

	exEntry=getEntry(index,subindex);
	
	if(exEntry->value!=NULL)
	{
		free(exEntry->value);
	}
	if(exEntry->defaultValue!=NULL)
	{
		free(exEntry->defaultValue);
	}

	lastIdx.indices=0;
	lastEntry=NULL;

	dictionary.erase(dictionary.find(indices.indices));
}

void ObjectDictionary::setValue(unsigned short index,
				unsigned char subindex,
				void*value,int size)
{
	int dSize;

//	assert(value!=NULL);
	if( value == NULL )
		return;
	
	ObjectDictionaryEntry*exEntry;
	exEntry=getEntry(index,subindex);

	try
	{
		dSize=(getValueBitSize(index,subindex)+7)>>3;
	}
	catch(ObjectSizeIsVariable)
	{
		assert(size!=0);

		dSize=size;
	}

	if(exEntry->value==NULL)
	{
		exEntry->value=malloc(dSize);
	}
	else if(dSize!=exEntry->size)
	{
		free(exEntry->value);
		exEntry->value=malloc(dSize);
	}
	exEntry->size=dSize;
	memcpy(exEntry->value,value,dSize);
}

void ObjectDictionary::getValue(unsigned short index, unsigned char subindex,
				void*value)
{
	int dSize;

//	assert(value!=NULL);
	if( value == NULL )
		return;
	
	const ObjectDictionaryEntry* exEntry = getEntry(index,subindex);

	if((dSize=exEntry->size)==0)
	{
		dSize=getValueBitSize(index,subindex);
		dSize=(dSize+7)>>3;
	}

	if(exEntry->value==NULL)
	{
		throw ODValueNotAssigned(index,subindex);
	}

	memcpy(value,exEntry->value,dSize);
}

int ObjectDictionary::getDataSize(unsigned short index,
				  unsigned char subindex)
{
	const ObjectDictionaryEntry*exEntry;
	exEntry=getEntry(index,subindex);

	return exEntry->size;
}


int ObjectDictionary::getValueBitSize(unsigned short index,
				      unsigned char subindex)
{
	int dSize;

	const ObjectDictionaryEntry*exEntry;
	exEntry=getEntry(index,subindex);
	
	dSize=dataSize[exEntry->dataType];
	
	switch(dSize)
	{
		case Variable:
		{
			if(exEntry->dataType==VisibleString)
				dSize=(strlen((char*)exEntry->value)+1)<<3;
			else
				throw ObjectSizeIsVariable();
		}
		break;

		case Unknown:
			assert(dSize!=Unknown);
		break;

		case OneBit:
			dSize=1;
		break;

		default:
			dSize<<=3;
			break;
	}

	return dSize;
}


void ObjectDictionary::setRxPDO_COB_ID(int pdoNo,unsigned int cobID)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));

	setValue(RxPDOParm+pdoNo-1,1,&cobID);
}

unsigned int ObjectDictionary::getRxPDO_COB_ID(int pdoNo)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));
	unsigned int cobID;
	
	getValue(RxPDOParm+pdoNo-1,1,&cobID);
	
	return cobID;
}

unsigned int ObjectDictionary::findRxPDONoByCobID(unsigned int cobID)
{
	int k;

	for(k=1;k<=MaxPDONum;k++)
	{
		if(cobID==getRxPDO_COB_ID(k))
		{
			return k;
		}
	}
	return 0;
}

unsigned int ObjectDictionary::findTxPDONoByCobID(unsigned int cobID)
{
	int k;

	for(k=1;k<=MaxPDONum;k++)
	{
		if(cobID==getTxPDO_COB_ID(k))
		{
			return k;
		}
	}
	return 0;
}

unsigned int ObjectDictionary::getRxPDOsNumberOfMappedObjects(int pdoNo)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));
	unsigned int num=0;
	
	getValue(RxPDOMapping+pdoNo-1,0,&num);
	
	return num;
}

PDOMapObject ObjectDictionary::getRxPDOsMappedObject(int pdoNo, int objNo)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));
	assert((objNo>=0)and(objNo<=254));
	PDOMapObject obj;
	
	getValue(RxPDOMapping+pdoNo-1,objNo+1,&obj);
	
	return obj;
}


void ObjectDictionary::setTxPDO_COB_ID(int pdoNo,unsigned int cobID)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));

	setValue(TxPDOParm+pdoNo-1,1,&cobID);
}

unsigned int ObjectDictionary::getTxPDO_COB_ID(int pdoNo)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));
	unsigned int cobID;
	
	getValue(TxPDOParm+pdoNo-1,1,&cobID);
	
	return cobID;
}

unsigned int ObjectDictionary::getTxPDOsNumberOfMappedObjects(int pdoNo)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));
	unsigned int num=0;
	
	getValue(TxPDOMapping+pdoNo-1,0,&num);
	
	return num;
}

PDOMapObject ObjectDictionary::getTxPDOsMappedObject(int pdoNo, int objNo)
{
	assert((pdoNo>0)and(pdoNo<=MaxPDONum));
	assert((objNo>=0)and(objNo<=254));
	PDOMapObject obj;
	
	getValue(TxPDOMapping+pdoNo-1,objNo+1,&obj);
	
	return obj;
}

unsigned int ObjectDictionary::getRxSDO_COB_ID() // client 2 server
{
	unsigned int cobID;
	
	getValue(SDOServerParm,1,&cobID);

	return cobID;
}

unsigned int ObjectDictionary::getTxSDO_COB_ID() // server 2 client
{
	unsigned int cobID;
	
	getValue(SDOServerParm,2,&cobID);

	return cobID;
}


void ObjectDictionary::setNodeID(int nodeID)
{
	unsigned int COBId=nodeID;
	unsigned char num=2;
	int k;
	ObjectDictionaryEntry odEntry;
	
	assert((nodeID>0)&&(nodeID<128));

//	printf("Add new entry\n");
	odEntry.index=NodeID;
	odEntry.subindex=0;
	odEntry.objectType=var;
	odEntry.dataType=Unsigned32;
	odEntry.attr=ro;
	odEntry.isPDOmap=false;
	odEntry.minimum=1;
	odEntry.maximum=127;
	odEntry.defaultValue=malloc(4);
	*(unsigned int*)odEntry.defaultValue=(unsigned int)1;
	odEntry.value=NULL;
	
	addEntry(odEntry);

	setValue(NodeID,0,&COBId);

	odEntry.index=SDOServerParm;
	odEntry.subindex=0;
	odEntry.objectType=var;
	odEntry.dataType=Unsigned8;
	odEntry.attr=ro;
	odEntry.isPDOmap=false;
	odEntry.value=NULL;
	
	addEntry(odEntry);
	COBId=2;
	setValue(SDOServerParm,0,&COBId);

	odEntry.index=SDOServerParm;
	odEntry.subindex=1;
	odEntry.objectType=var;
	odEntry.dataType=Unsigned32;
	odEntry.attr=ro;
	odEntry.isPDOmap=false;
//	odEntry.minimum=1;
//	odEntry.maximum=127;
//	odEntry.defaultValue=malloc(4);
//	*(unsigned int*)odEntry.defaultValue=(unsigned int)1;
	odEntry.value=NULL;
	
	addEntry(odEntry);

	COBId=nodeID+0x600;
	setValue(SDOServerParm,1,&COBId);

	odEntry.index=SDOServerParm;
	odEntry.subindex=2;
	odEntry.objectType=var;
	odEntry.dataType=Unsigned32;
	odEntry.attr=ro;
	odEntry.isPDOmap=false;
//	odEntry.minimum=1;
//	odEntry.maximum=127;
//	odEntry.defaultValue=malloc(4);
//	*(unsigned int*)odEntry.defaultValue=(unsigned int)1;
	odEntry.value=NULL;
	
	addEntry(odEntry);

	COBId=nodeID+0x580;
	setValue(SDOServerParm,2,&COBId);

	for(k=0;k<MaxPDONum;k++)
	{
		try
		{
			getEntry(RxPDOParm+k,0);
		}
		catch(...)
		{
			// если элемент OD для параметров PDO уже
			// зарезервирован (например, после загрузки EDS),
			// то автоматически сочиняем для него COBID
			if(k>3)
			{
				break;
			}
		}
		odEntry.index=RxPDOParm+k;
		odEntry.subindex=0; // number of entries
		odEntry.objectType=var;
		odEntry.dataType=Unsigned8;
		odEntry.attr=ro;
		odEntry.isPDOmap=false;
		odEntry.minimum=0;
		odEntry.maximum=0;
		odEntry.defaultValue=malloc(1);
		*(unsigned char*)odEntry.defaultValue=(unsigned char)2;
		odEntry.value=NULL;

		addEntry(odEntry);

		setValue(RxPDOParm+k,0,&num);

		odEntry.subindex=1; // COB-ID used by rxPDO1
		odEntry.dataType=Unsigned32;
		odEntry.attr=rw;

		addEntry(odEntry);

		// Нормальная формула такая:
		// COBId=nodeID+(((k<<1)+4)<<7);
		// а эта на случай, когда количество PDO
		// больше четырёх.
		// В результате NodeID разных станций могут
		// перекрываться, надо проверять!!!
#warning "NodeID's may overlap if number of RxPDOs more then 4!"
		COBId=nodeID+((((k&0x03)<<1)+4)<<7)+(k>>2);
		setValue(RxPDOParm+k,1,&COBId);
	}

	for(k=0;k<MaxPDONum;k++)
	{
		try
		{
			getEntry(TxPDOParm+k,0);
		}
		catch(...)
		{
			if(k>3)
			{
				break;
			}
		}
		odEntry.index=TxPDOParm+k;
		odEntry.subindex=0; // number of entries
		odEntry.objectType=var;
		odEntry.dataType=Unsigned8;
		odEntry.attr=ro;
		odEntry.isPDOmap=false;
		odEntry.minimum=0;
		odEntry.maximum=0;
		odEntry.defaultValue=malloc(1);
		*(unsigned char*)odEntry.defaultValue=(unsigned char)2;
		odEntry.value=NULL;
		
		addEntry(odEntry);

		setValue(TxPDOParm+k,0,&num);

		odEntry.subindex=1; // COB-ID used by txPDO1
		odEntry.dataType=Unsigned32;
		odEntry.attr=rw;

		addEntry(odEntry);

		// Нормальная формула такая:
		// COBId=nodeID+(((k<<1)+3)<<7);
		// а эта на случай, когда количество PDO
		// больше четырёх.
		// В результате NodeID разных станций могут
		// перекрываться, надо проверять!!!
#warning "NodeID's may overlap if number of TxPDOs more then 4!"
		COBId=nodeID+((((k&0x03)<<1)+3)<<7)+(k>>2);
		setValue(TxPDOParm+k,1,&COBId);
	}
}

int ObjectDictionary::getNodeID()
{
	unsigned int nodeID;
	
	getValue(NodeID,0,&nodeID);
	
	return nodeID;
}

void ObjectDictionary::printAll()
{
	map<unsigned int,ObjectDictionaryEntry>::const_iterator odIt=
		dictionary.begin();
	int k;

	for(k=0;odIt!=dictionary.end();odIt++,k++)
	{
		if(odIt->second.subindex==0)
		{
			printf("\n");
		}
		printf("%04i: %06x: ",k,odIt->first);
		if(odIt->second.subindex==0)
		{
			printf("\ni: %04x: ",
			       objDescription[odIt->second.index].index);
			printf("ObjType: %i; name: %s, subNum: %i\n\
dataType: %i, attr: %i, isMap: %i\n",
			       objDescription[odIt->second.index].objectType,
			       objDescription[odIt->second.index].objectName.c_str(),
			       objDescription[odIt->second.index].subNumber,
			       objDescription[odIt->second.index].dataType,
			       objDescription[odIt->second.index].attr,
			       objDescription[odIt->second.index].isPDOmap);
		}
		printf("i: %04x, si: %02x, ObjType: %i\nname: %s\n\
dataType: %i, attr: %i, isMap: %i\nmin: %i, max: %i\n",// def: %i\n",
		       odIt->second.index,
		       odIt->second.subindex,odIt->second.objectType,
		       odIt->second.objectName.c_str(),
		       odIt->second.dataType,odIt->second.attr,
		       odIt->second.isPDOmap,odIt->second.minimum,
		       odIt->second.maximum/*,odIt->second.defaultValue*/);
		if(odIt->second.dataType==CanOpen::VisibleString)
		{
			printf("value: %s, def: %s\n",
			       odIt->second.value,
			       odIt->second.defaultValue);
		}
		else
		{
			long long x1=0;
			long long x2=0;

			if(odIt->second.value!=NULL)
				memcpy(&x1,odIt->second.value,
				       dataSize[odIt->second.dataType]);
			if(odIt->second.defaultValue!=NULL)
				memcpy(&x2,odIt->second.defaultValue,
				       dataSize[odIt->second.dataType]);
			printf("value: %qi, def: %qi\n",x1,x2);
		}
	}
}

ObjectDictionaryEntry*ObjectDictionary::getEntry(unsigned short index,
						 unsigned char subindex)
{
	ODIndices indices;
	map<unsigned int,ObjectDictionaryEntry>::iterator odIt;

	indices.indices=0;
	indices.index=index;
	indices.subindex=subindex;
	
	if(indices.indices==lastIdx.indices)
	{
		// assert(lastEntry!=NULL);
		if( lastEntry==NULL )
			throw ODEntryNotFound(index,subindex);

		return lastEntry;
	}

	odIt=dictionary.find(indices.indices);
	
	if(odIt==dictionary.end())
	{
		// entry not found
		throw ODEntryNotFound(index,subindex);
	}

	lastIdx.indices=indices.indices;
	lastEntry=&(odIt->second);

	return &(odIt->second);
}

void ObjectDictionary::setObjectDescription(unsigned short index,
					    ObjectDescription desc)
{
	objDescription[index]=desc;
}
