
#include "SimpleCanDevice.h"
//#include "ObjectDictionary.h"

#include "CanExceptions.h"

#include "ArbraCan.h"

//#include "EDSParser.h"

#include "CanHelpers.h"

#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace CanOpen;

SimpleCanDevice::SimpleCanDevice(/*string device*/):
waiting(true),baudRate(1000),protocol(CAN_20A),ffmt(ffDef),
sendPause_usec(0)
{
	accFilter.acceptanceCode=0xffffffff;
	accFilter.acceptanceMask=0xffffffff;
}

SimpleCanDevice::~SimpleCanDevice()
{
}

void SimpleCanDevice::setWaiting(bool state)
{
	waiting=state;
}

void SimpleCanDevice::setBaudRate(int baudrate)
{
	baudRate=baudrate;
}

void SimpleCanDevice::setAcceptanceFilter(int acceptanceCode,
					  int acceptanceMask)
{
	accFilter.acceptanceCode=acceptanceCode;
	accFilter.acceptanceMask=acceptanceMask;
}

void SimpleCanDevice::setProtocol(CanProtocol prot)
{
	protocol=prot;
}

void SimpleCanDevice::setFrameFormat( CanFrameFormat f )
{
	ffmt = f;
}
