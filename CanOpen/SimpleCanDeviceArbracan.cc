
#include "SimpleCanDeviceArbracan.h"
//#include "ObjectDictionary.h"

#include "CanExceptions.h"

#include "ArbraCan.h"
#include "ArbraCanMsg.h"

//#include "EDSParser.h"

#include "CanHelpers.h"

#include <cstdlib>
#include <cstring>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace CanOpen;

SimpleCanDeviceArbracan::SimpleCanDeviceArbracan(string device):
SimpleCanDevice()
{
	fd=open(device.c_str(),O_RDWR);
	if(fd<0)
	{
		string strErr="Unable to open CAN device ("+device+
			")[Error: "+strerror(errno)+"]";
		throw CanException(strErr.c_str());
	}
}

SimpleCanDeviceArbracan::~SimpleCanDeviceArbracan()
{
	close(fd);
}

int SimpleCanDeviceArbracan::sendMessage(Message*m)
{
	int res;

//	int state=0x00;
	
//	if((msg->cob_id.w>>7)==0x01)
/*	{
		int k;
		printf("SndMessage:");
		for(k=0;k<sizeof(Message);k++)
		{
			printf(" %02x",((unsigned char*)(msg))[k]);
		}
		printf("\n");
	}*/

	ArbraCanMessage msg;

	#warning 29BitID (int) convert to 11bitID(short)
	msg.cob_id.w = m->cob_id.w;
	msg.rtr = m->rtr;
	msg.len = m->len;
	memcpy(msg.data,m->data,m->len);


	if(waiting)
		res = ioctl(fd,CANBOARD_WRITE_BLOCK,msg);
	else
		res = ioctl(fd,CANBOARD_WRITE,msg);
	
	if(res<0)
	{
		string strErr="";
		char strerrno[20];
		char strerrstr[256];
		strerrstr[0]='\0';
		sprintf(strerrno,"%i",errno);
//		strerror_r(errno,strerrstr,256);
		strErr=strErr+"CAN device write error #"+strerrno+" [Error: "+
			strerrstr+"]";
		throw CanException(strErr.c_str());
	}
	if( sendPause_usec > 0 )
		usleep(sendPause_usec);

	return res;
}

int SimpleCanDeviceArbracan::receiveMessage(Message*msg)
{
	int res;
	ArbraCanMessage _msg;

	if(waiting)
		res = ioctl(fd,CANBOARD_READ_BLOCK,&_msg);
	else
		res = ioctl(fd,CANBOARD_READ,&_msg);

	printf("waiting: %i; res: %i\n",waiting,res);
	if(res<0)
	{
		string strErr="";
		char strerrno[20];
		char strerrstr[256];
		strerrstr[0]='\0';
		sprintf(strerrno,"%i",errno);
//		strerror_r(errno,strerrstr,256);
		strErr=strErr+"CAN device read error #"+strerrno+" [Error: "+
			strerrstr+"]";
		throw CanException(strErr.c_str());
	}
//	memcpy(msg,&_msg,sizeof(Message));
	msg->cob_id.w = _msg.cob_id.w;
	msg->rtr = _msg.rtr;
	msg->len = _msg.len;
	memcpy(msg->data,&_msg.data,_msg.len);

//	if((msg->cob_id.w>>7)==0x01)
/*	{
		int k;
		printf("RcvMessage:");
		for(k=0;k<sizeof(Message);k++)
		{
			printf(" %02x",((unsigned char*)(msg))[k]);
		}
		printf("\n");
	}*/
	return res;
}

void SimpleCanDeviceArbracan::setBaudRate(int baudrate)
{
	int res;
	
	res = ioctl(fd,CANBOARD_SET_BAUDRATE,&baudrate);
	if(res<0)
	{
		string strErr;
		strErr=strErr+"CAN device: incorrect baud rate.";
		throw CanException(strErr.c_str());
	}
	SimpleCanDevice::setBaudRate(baudrate);
}

void SimpleCanDeviceArbracan::setAcceptanceFilter(int acceptanceCode, int acceptanceMask)
{
	int res;
	AcceptanceFilter acc_filter;
	
	acc_filter.acceptanceCode=acceptanceCode;
	acc_filter.acceptanceMask=acceptanceMask;
	
	res = ioctl(fd,CANBOARD_SET_ACCEPTANCE_FILTER,&accFilter);
	if(res<0)
	{
		string strErr;
		char strerrno[20];
		char strerrstr[256];
		strerrstr[0]='\0';
		sprintf(strerrno,"%i",errno);
//		strerror_r(errno,strerrstr,256);
		strErr=strErr+"CAN device ioctl error #"+strerrno+" [Error: "+
			strerrstr+"]";
		throw CanException(strErr.c_str());
	}
	SimpleCanDevice::setAcceptanceFilter(acceptanceCode,acceptanceMask);
}

void SimpleCanDeviceArbracan::resetController()
{
	clearTxBuffer();
	clearRxBuffer();
}


void SimpleCanDeviceArbracan::clearTxBuffer()
{
	int res;
	int notused;
	
	res = ioctl(fd,CANBOARD_CLEAR_TX_BUFFER,&notused);
	if(res<0)
	{
		string strErr;
		char strerrno[20];
		char strerrstr[256];
		strerrstr[0]='\0';
		sprintf(strerrno,"%i",errno);
//		strerror_r(errno,strerrstr,256);
		strErr=strErr+"CAN device ioctl error #"+strerrno+" [Error: "+
			strerrstr+"]";
		throw CanException(strErr.c_str());
	}
}

void SimpleCanDeviceArbracan::clearRxBuffer()
{
	int res;
	int notused;
	
	res = ioctl(fd,CANBOARD_CLEAR_RX_BUFFER,&notused);
	if(res<0)
	{
		string strErr;
		char strerrno[20];
		char strerrstr[256];
		strerrstr[0]='\0';
		sprintf(strerrno,"%i",errno);
//		strerror_r(errno,strerrstr,256);
		strErr=strErr+"CAN device ioctl error #"+strerrno+" [Error: "+
			strerrstr+"]";
		throw CanException(strErr.c_str());
	}
}
