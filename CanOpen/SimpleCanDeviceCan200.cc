// -------------------------------------------------------------------------
#include <cstdlib>
#include <cstring>

#include <sstream>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "can_codes.h"
#include "SimpleCanDeviceCan200.h"
#include "CanExceptions.h"
#include "CanHelpers.h"
// -------------------------------------------------------------------------
using namespace CanOpen;
using namespace std;
// -------------------------------------------------------------------------
SimpleCanDeviceCan200::SimpleCanDeviceCan200( string device ):
SimpleCanDevice(),
dev(device)
{
	rfd = open(dev.c_str(), O_RDWR | O_NONBLOCK);
	if( rfd < 0 )
	{
		ostringstream err;
		err << "Unable to open CAN device (" << dev
				<< ")[Error: " << strerror(errno) << "]";
		throw CanException(err.str());
	}
	wfd = open(dev.c_str(), O_RDWR);
	if( wfd < 0 )
	{
		ostringstream err;
		err << "Unable to open CAN device (" << dev
				<< ")[Error: " << strerror(errno) << "]";
		close(rfd);
		throw CanException(err.str());
	}
	init();
}
// -------------------------------------------------------------------------
SimpleCanDeviceCan200::~SimpleCanDeviceCan200()
{
	close(wfd);
	close(rfd);
}
// -------------------------------------------------------------------------
int SimpleCanDeviceCan200::sendMessage( Message* msg )
{
	CAN_data data;
	int res;

	if( msg->len>8 )
		msg->len=8;

	data.type = PeliCAN;
	data.id = msg->cob_id.w;
	data.rtr = (msg->rtr&0x01)?1:0;
	data.dlc = msg->len;
	memcpy(data.data, msg->data, data.dlc);

	res = write(wfd, &data, sizeof(data));
	int write_errno = errno;

	if( res < 0 )
	{
		CAN_status_t status;
		getStatus(&status.byte);
		if( status.bit.bs )
			setBusOn();

		if( write_errno == EAGAIN )
			throw CanTimeOut();

		ostringstream err;
		err << " CAN device write error #" << write_errno
				<< " [Error: " << strerror(write_errno) << "]";
		throw CanException(err.str());
	}

	if( res != sizeof(data) )
		throw CanException(" Not all data are sent");

	return res;
}
// -------------------------------------------------------------------------
int SimpleCanDeviceCan200::receiveMessage( Message* msg )
{
	CAN_data readBuf[256];
	int res = 0;

	if (buffer.empty())
	{
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(rfd, &fds);

		if( waiting )
		{
			res = select(rfd + 1, &fds, NULL, NULL, NULL);
		}
		else
		{
			struct timeval tv = {0, 0};
			res = select(rfd + 1, &fds, NULL, NULL, &tv);
		}

		if( res < 0 )
		{
			ostringstream err;
			err << " CAN device select error #" << errno
					<< " [Error: " << strerror(errno) << "]";
			throw CanException(err.str());
		}

		if( FD_ISSET(rfd, &fds) )
		{
			res = readData(readBuf, sizeof(readBuf)/sizeof(CAN_data));
			try
			{
				for( int k = 0; k < res/(int)sizeof(CAN_data); ++k )
					buffer.push(readBuf[k]);
			}
			catch( exception& ex )
			{
				throw CanException(ex.what());
			}
		}
		else
		{
			throw CanTimeOut();
		}
	}

	if (!buffer.empty())
	{
		CAN_data data = buffer.front();
		buffer.pop();
		msg->cob_id.w = data.id;
		msg->rtr = data.rtr;
		msg->len = data.dlc;
		if (msg->len > 8)
			msg->len = 8;
		memcpy(msg->data, data.data, msg->len);
		res = sizeof(CAN_data);
	}

	return res;
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::setBaudRate( int baudrate )
{
	ioctl(wfd, CAN200_SETCANSPEED, &baudrate);
	SimpleCanDevice::setBaudRate(baudrate);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::setAcceptanceFilter( int acceptanceCode,
						int acceptanceMask )
{
	p_filter_t peliCanAccFilter;

	peliCanAccFilter.mode = 1;  // single filter mode (see SJA1000 datasheet)
	peliCanAccFilter.mask[0] = acceptanceMask >> 24;
	peliCanAccFilter.mask[1] = acceptanceMask >> 16;
	peliCanAccFilter.mask[2] = acceptanceMask >> 8;
	peliCanAccFilter.mask[3] = acceptanceMask;
	peliCanAccFilter.filter[0] = acceptanceCode >> 24;
	peliCanAccFilter.filter[1] = acceptanceCode >> 16;
	peliCanAccFilter.filter[2] = acceptanceCode >> 8;
	peliCanAccFilter.filter[3] = acceptanceCode;
	ioctl(rfd, CAN200_P_SETFILTER, &peliCanAccFilter);
	SimpleCanDevice::setAcceptanceFilter(acceptanceCode, acceptanceMask);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::resetController()
{
	ioctl(wfd, CAN200_HARDRESET);
	init();
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::setFrameFormat( SimpleCanDeviceCan200::CanFrameFormat ffmt )
{
	if( ffmt == ffStd )
		throw CanException("ffStd not supported");
	SimpleCanDevice::setFrameFormat(ffmt);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::getConfig( CAN_info_t *info )
{
	ioctl(rfd, CAN200_GETCONFIG, info);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::getStatus( unsigned char *status )
{
	ioctl(rfd, CAN200_GETSTATUS, status);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::setBusOn()
{
	ioctl(wfd, CAN200_SETBUSON);
}
// -------------------------------------------------------------------------
int SimpleCanDeviceCan200::readData( CAN_data *data, unsigned int count )
{
	int res = read(rfd, data, count * sizeof(CAN_data));
	int read_errno = errno;
	bool bus_on = false;

	if( res <= 0 )
		bus_on = true;

	for( int k = 0; !bus_on && k < res/(int)sizeof(CAN_data); ++k )
		if( data[k].type == ErrorCAN )
			bus_on = true;

	if( bus_on )
	{
		CAN_status_t status;
		getStatus(&status.byte);
		if( status.bit.bs )
			setBusOn();
	}

	if( !res )
		throw CanTimeOut();

	if( res < 0 )
	{
		if( read_errno == EAGAIN)
			throw CanTimeOut();

		ostringstream err;
		err << " CAN device read error #" << read_errno
				<< " [Error: " << strerror(read_errno) << "]";
		throw CanException(err.str());
	}

	return res;
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCan200::init()
{
	unsigned char mode = PeliCAN;
	ioctl(wfd, CAN200_SETWORKMODE, &mode);
	ioctl(wfd, CAN200_SETCANSPEED, &baudRate);
	ioctl(wfd, CAN200_SETDRIVERMODE);
	setAcceptanceFilter(accFilter.acceptanceCode, accFilter.acceptanceMask);
}
// -------------------------------------------------------------------------
