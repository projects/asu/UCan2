// -------------------------------------------------------------------------
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <canbus4linux/canbus4linux.h>
#include "SimpleCanDeviceCanbus.h"
#include "CanExceptions.h"
#include "CanHelpers.h"
// -------------------------------------------------------------------------
using namespace CanOpen;
using namespace std;
// -------------------------------------------------------------------------
SimpleCanDeviceCanbus::SimpleCanDeviceCanbus( string device ):
SimpleCanDevice(),
fd(-1),
dev(device),
wait(true)
{
	reopen();
	sendPause_usec = 0;
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::reopen()
{
	if( fd>=0 )
		close(fd);

	fd = open(dev.c_str(),O_RDWR);
	if(fd<0)
	{
		string strErr="Unable to open CAN device ("+dev+")[Error: "+strerror(errno)+"]";
		throw CanException(strErr.c_str());
	}

//	fcntl(fd,F_SETOWN,getpid());
	fcntl(fd,F_SETFL, FASYNC | fcntl(fd,F_GETFL));
	ioctl(fd, CANBUS4LINUX_INIT, 0);

//	setFrameFormat(ffmt);
//	setProtocol(protocol);
//	setAcceptanceFilter(0xffffffff,0xffffffff);
	resetController();
}
// -------------------------------------------------------------------------
SimpleCanDeviceCanbus::~SimpleCanDeviceCanbus()
{
	if( fd >=0 )
		close(fd);
}
// -------------------------------------------------------------------------
int SimpleCanDeviceCanbus::sendMessage( Message* msg )
{
	if( fd < 0 )
		reopen();
		
	int res = EINVAL;
	if( msg->len>8 ) // ???
		msg->len=8;

	struct canbus_transmit_data data;
	memset(&data,0,sizeof(data));

	data.fmt 		= this->ff; // CANBUS_TRANS_FMT_DEFAULT; //
	data.identifier = msg->cob_id.w;
	data.rtr 		= (msg->rtr&0x01)?1:0;
	data.dlc 		= msg->len;

//	printf("send: cob.id=%d data len: %d rtr=%d\n",data.identifier,data.dlc,data.rtr);
//	for( int i=0;i<data.dlc;i++ )
//		data.msg[i]=msg->data[i];
	memcpy(data.msg,msg->data,data.dlc);

	if( waiting )
	{
		setBlocking(true);
		res = ioctl(fd, CANBUS4LINUX_WRITE_TRANSMIT_DATA, &data);
		setBlocking(false);
	}
	else
	{
		setBlocking(false);
		res = ioctl(fd, CANBUS4LINUX_WRITE_TRANSMIT_DATA, &data);
	}

	int write_errno = errno;

//	printf(" CAN device write error #%d [Error: %s ]\n",errno,strerror(errno));
	if( res < 0 )
	{
		if( write_errno == EAGAIN )
			throw CanTimeOut();

		ostringstream err;
		err << " CAN device write error #" << write_errno
			<< " [Error: " << strerror(write_errno) << "]";
		throw CanException(err.str());
	}
	
	if( sendPause_usec > 0 )
		usleep(sendPause_usec);
	
	return res;
}
// -------------------------------------------------------------------------
int SimpleCanDeviceCanbus::receiveMessage( Message* msg )
{
	/*! \todo maxread - надо вынести в настройки */
	static const int maxread = 2000;
	int res = 0;

	if( buffer.empty() )
	{
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		if( waiting )
		{
			setBlocking(true);
			res = select(fd + 1, &fds, NULL, NULL, NULL);
		}
		else
		{
			setBlocking(true);
			struct timeval tv = {0, 0};
			res = select(fd + 1, &fds, NULL, NULL, &tv);
		}

		if( res < 0 )
		{
			ostringstream err;
			err << " CAN device select error #" << errno
					<< " [Error: " << strerror(errno) << "]";
			throw CanException(err.str());
		}

		if( !FD_ISSET(fd, &fds) )
			throw CanTimeOut();

		char rbuf[100];
		unsigned int udata[8];
		long unsigned int ucob_id;
		int urtr, ulen;
		setBlocking(false);

		for( int i=0; i<maxread; i++ )
		{
			unsigned long t1;
			unsigned long t2;
	  //	unsigned long lost;

			res = read(fd,&rbuf,sizeof(rbuf));
			if( res < 0 )
			{
				if( !buffer.empty() )
					break;
				
				if( errno == EAGAIN )
					throw CanTimeOut();;
		
				ostringstream err;
				err << " CAN device read error #" << errno
					<< " [Error: " << strerror(errno) << "]";
				throw CanException(err.str());
			}

			if( !res )
			{
				if( !buffer.empty() )
					break;

				throw CanTimeOut();
			}
			
			res = sscanf(rbuf,"received\t%lu:%lu\t%08lx\t%d\t%d\t%02x\t%02x\t%02x\t%02x\t%02x\t%02x\t%02x\t%02x",&t1,&t2,
				&ucob_id,&urtr,&ulen,
				&udata[0],&udata[1],&udata[2],
				&udata[3],&udata[4],&udata[5],
				&udata[6],&udata[7] /*,&lost */
			);
			
//			printf("**rbuf: %s\n",rbuf);
			if( res < 5 )
				continue;
			
			memset(msg,0,sizeof(Message));
			msg->rtr = urtr;
			msg->len = ulen;
			msg->cob_id.w = ucob_id;
			if( msg->len > 8 )
				msg->len = 8;
			for (int i=0; i < msg->len; i++)
				msg->data[i] = udata[i];
			
			buffer.push( *msg );
		}
	} // if buffer empty

	if( !buffer.empty() )
	{
		(*msg) = buffer.front();
		buffer.pop();
		return 1;
	}

//	throw CanTimeOut();
	memset(msg,0,sizeof(Message));
	return 0;
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::setBaudRate( int baudrate )
{
	ioctl(fd, CANBUS4LINUX_SET_BAUDRATE,baudrate*1000);
	SimpleCanDevice::setBaudRate(baudrate);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::setAcceptanceFilter(int acceptanceCode,
						  int acceptanceMask)
{
	int ret;
	struct canbus_acceptance_filter d;

	ret = ioctl(fd, CANBUS4LINUX_READ_ACCEPTANCE_FILTER, &d);
	if( ret < 0 ) 
	{
		printf("no canbus set acceptance filter c=%d m=%d\n",acceptanceCode,acceptanceMask);
		return; // ret;
	}

	d.code = acceptanceCode;
	d.mask = acceptanceMask;
	ret = ioctl(fd, CANBUS4LINUX_WRITE_ACCEPTANCE_FILTER, &d);

	SimpleCanDevice::setAcceptanceFilter(acceptanceCode,acceptanceMask);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::setBlocking( bool blocking )
{
	if( wait == blocking )
		return;
	
	wait = blocking;
	
	if( blocking )
		fcntl(fd,F_SETFL,0);
	else
		fcntl(fd,F_SETFL,O_NONBLOCK);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::resetController()
{
	ioctl(fd, CANBUS4LINUX_SET_COMMAND,CANBUS_CMD_CLEAR_OVERRUN);
	setAcceptanceFilter(accFilter.acceptanceCode,accFilter.acceptanceMask);
	setFrameFormat(ffmt);
	setProtocol(protocol);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::setProtocol( CanProtocol prot )
{
	int mode = CANBUS_FORMAT_CAN_2_0_A;
	switch( prot )
	{
		case CAN_20A:
			mode = CANBUS_FORMAT_CAN_2_0_A;
		break;

		case CAN_20B:
			mode = CANBUS_FORMAT_CAN_2_0_B;
		break;

		default:
		break;
	}

	ioctl(fd, CANBUS4LINUX_SET_CAN_MODE,mode);
	SimpleCanDevice::setProtocol(prot);
}
// -------------------------------------------------------------------------
void SimpleCanDeviceCanbus::setFrameFormat( SimpleCanDeviceCanbus::CanFrameFormat ffmt )
{
	int f = CANBUS_TRANS_FMT_DEFAULT;

	switch(ffmt)
	{
		case ffStd:
			f = CANBUS_TRANS_FMT_STD;
		break;

		case ffExt:
			f = CANBUS_TRANS_FMT_EXT;
		break;
	
		case ffDef:
		default:
			f = CANBUS_TRANS_FMT_DEFAULT;
			break;
	}

	ioctl(fd, CANBUS4LINUX_SET_DEFAULT_FRAME_FORMAT, f);
	this->ff = f;

	SimpleCanDevice::setFrameFormat(ffmt);
}
// -------------------------------------------------------------------------
