
#include "SimpleCanDevicePCI_1680.h"
//#include "ObjectDictionary.h"

#include "CanExceptions.h"
#include "CanHelpers.h"

#include "advcan4linux.h"

//#include "EDSParser.h"

#include <cstdlib>
#include <cstring>

#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace CanOpen;

SimpleCanDevicePCI_1680::SimpleCanDevicePCI_1680(string device):
SimpleCanDevice()
{
	fd=open(device.c_str(),O_RDWR);
//	printf("open: fd: %i\n",fd);
	if(fd<0)
	{
		string strErr="Unable to open CAN device ("+device+
			")[Error: "+strerror(errno)+"]";
		throw CanException(strErr.c_str());
	}
//	resetController();
}

SimpleCanDevicePCI_1680::~SimpleCanDevicePCI_1680()
{
	close(fd);
}

int SimpleCanDevicePCI_1680::sendMessage(Message*msg)
{
	int res;

//	printf("snd1: fd: %i\n",fd);

	if(msg->len>8)
	{
		msg->len=8;
	}

	canmsg_t a_msg;
	memset(&a_msg,0,sizeof(canmsg_t));

	a_msg.length = msg->len;
	a_msg.id     = msg->cob_id.w;
	a_msg.flags |= (msg->rtr & 0x01) ? MSG_RTR : 0;
	a_msg.flags |= (protocol == CAN_20B || ffmt == ffExt) ? MSG_EXT : 0;

	for(int i = 0; i < a_msg.length; i++)
	{
		a_msg.data[i] = msg->data[i];
	}
	
//	printf("send: fd: %i\n",fd);
	if(waiting)
	{
		setBlocking(true);
		res = write(fd,&a_msg,1);
		setBlocking(false);
	}
	else
	{
		setBlocking(false);
		res = write(fd,&a_msg,1);
	}

//	printf("res: %i\n",res);
	if(res <= 0)
	{
		// compatibility check "res == 0 || " with errno
		if( res == 0 || errno == EAGAIN )
			throw CanTimeOut();

		throwErrno(res, "CAN device write error #");
	}

	if( sendPause_usec > 0 )
		usleep(sendPause_usec);
	
	return res;
}

int SimpleCanDevicePCI_1680::receiveMessage(Message*msg)
{
	int res;
	canmsg_t _msg;

	if(waiting)
	{
		setBlocking(true);
		res = read(fd,&_msg,1);
		setBlocking(false);
	}
	else
	{
		setBlocking(false);
		res = read(fd,&_msg,1);
	}

//	printf("waiting: %i; res: %i\n",waiting,res);
	if(res <= 0)
	{
		// compatibility check "res == 0 || " with errno
		if( res == 0 || errno == EAGAIN )
			throw CanTimeOut();

		throwErrno(res, "CAN device read error #");
	}

	memset(msg,0,sizeof(Message));
	msg->cob_id.w = _msg.id;
	msg->rtr      = (_msg.flags && MSG_RTR) ? 0xff : 0x00;
	msg->len      = _msg.length;

	for(int i = 0; i < _msg.length; i++)
	{
		msg->data[i] = _msg.data[i];
	}

	return res;
}

void SimpleCanDevicePCI_1680::setBaudRate(int baudrate)
{
	Config_par_t  cfg;
	volatile Command_par_t cmd;

	cmd.cmd = CMD_STOP;
	checkIOCTL(ioctl(fd, CAN_IOCTL_COMMAND, &cmd));

	cfg.target = CONF_TIMING;
	cfg.val1   = baudrate;
	checkIOCTL(ioctl(fd, CAN_IOCTL_CONFIG, &cfg));

	cmd.cmd = CMD_START;
	checkIOCTL(ioctl(fd, CAN_IOCTL_COMMAND, &cmd));

	SimpleCanDevice::setBaudRate(baudrate);
}

void SimpleCanDevicePCI_1680::setAcceptanceFilter(int acceptanceCode,
						  int acceptanceMask)
{
	Config_par_t  cfg;
	volatile Command_par_t cmd;

	cmd.cmd = CMD_STOP;
	checkIOCTL(ioctl(fd, CAN_IOCTL_COMMAND, &cmd));

	cfg.target = CONF_ACC;
	cfg.val1   = acceptanceMask;
	cfg.val2   = acceptanceCode;
	checkIOCTL(ioctl(fd, CAN_IOCTL_CONFIG, &cfg));

	cmd.cmd = CMD_START;
	checkIOCTL(ioctl(fd, CAN_IOCTL_COMMAND, &cmd));

	SimpleCanDevice::setAcceptanceFilter(acceptanceCode,acceptanceMask);
}

void SimpleCanDevicePCI_1680::setBlocking(bool blocking)
{
	int res;

	if(blocking)
	{
		res = fcntl(fd,F_SETFL,0);
	}
	else
	{
		res = fcntl(fd,F_SETFL,O_NONBLOCK);
	}

	if(res < 0)
		throwErrno(res, "CAN device fcntl error #");
}

void SimpleCanDevicePCI_1680::resetController()
{
	volatile Command_par_t cmd;

	cmd.cmd = CMD_CLEARBUFFERS;
	checkIOCTL(ioctl(fd, CAN_IOCTL_COMMAND, &cmd));
}

void SimpleCanDevicePCI_1680::checkIOCTL(int res)
{
	if(res < 0)
		throwErrno(res, "CAN device ioctl error #");
}

void SimpleCanDevicePCI_1680::throwErrno(int res, const char* checkErr)
{
	string strErr;
	char strerrno[20];
	char strerrstr[256];

	strerrstr[0] = '\0';
	sprintf(strerrno, "%i", errno);
	strErr=strErr + checkErr + strerrno + " [Error: " +
		strerrstr + "]";

	throw CanException(strErr.c_str());
}
