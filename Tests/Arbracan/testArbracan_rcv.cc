#include <stdio.h>
#include <string>
#include <unistd.h>
#include "SimpleCanDeviceArbracan.h"

int main(int ac,char**av)
{
	std::string dev;
	int i;
	Message msg;
	bool rcvd=0;
	
	if(ac>1)
	{
		dev=av[1];
	}
	else
	{
		dev="/dev/can0";
	}
	
	SimpleCanDeviceArbracan canDev(dev);
	printf("Can device '%s' opened\n",dev.c_str());
	
	canDev.setBaudRate(250);
	canDev.setWaiting(false);
	printf("receiving...\n");
	for(;;)
	{
		try
		{
			canDev.receiveMessage(&msg);
			rcvd=true;
		}
		catch(...)
		{
			rcvd=false;
//			printf("catch...\n");
		}
		if(rcvd)
		{
//			printf("received\n");
		}
		if(rcvd)
		{
			printf("rcv: id: %04x; rtr: %i; data:",msg.cob_id.w,
			       msg.rtr);
			
			for(i=0;i<msg.len;i++)
			{
				printf(" %02x",msg.data[i]);
			}
			printf("\n");
		}
		if(!rcvd)
		{
			usleep(500000);
		}
	}
}
