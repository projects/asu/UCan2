#include "stdio.h"
#include "string"

#include "SimpleCanDeviceArbracan.h"

int main(int ac,char**av)
{
	std::string dev;
	Message msg;
	int i,j;
	
	if(ac>1)
	{
		dev=av[1];
	}
	else
	{
		dev="/dev/can0";
	}
	msg.len=8;
	msg.rtr=0;
	msg.cob_id.w=0x35f;
	
	SimpleCanDeviceArbracan canDev(dev);
	printf("Can device '%s' opened\n",dev.c_str());

	canDev.setBaudRate(250);
	canDev.setWaiting(false);
	for(i=0;/*i<256*/;i++)
	{
		printf("sending %i...\n",i);
		for(j=0;j<8;j++)
		{
			msg.data[j]=j;
		}
		msg.cob_id.w=i|(i<<8);
		msg.rtr=0;
		msg.len=i%8;
		try
		{
			canDev.sendMessage(&msg);
		}
		catch(...)
		{
			printf("catch...\n");
		}
		printf("sended: id: %04x; rtr: %i; data:",msg.cob_id.w,
		       msg.rtr);
			
		for(j=0;j<msg.len;j++)
		{
			printf(" %02x",msg.data[j]);
		}
		printf("\n");
	}
}
