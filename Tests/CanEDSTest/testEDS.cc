

#include "ObjectDictionary.h"
#include "CanHelpers.h"
#include "CanOpenMessages.h"

//#include "ConfParser.h"
#include "EDSParser.h"

#include <stdio.h>
#include <string>

using namespace CanOpen;

int main(int ac,char**av)
{
//	ConfParser conf("STANDARD.EDS");
//	string str;
	int k,z;
	ObjectDictionary od;
	std::string str;

	if(ac>1)
		str=av[1];
	else
		str="STANDARD.EDS";
	
	EDSParser conf(str);
	conf.fillOD(&od);

	od.printAll();
	
//	z=-1;
//	printf("sscanf: %i\n",sscanf("1000sub33","%isub%i",&k,&z));
//	printf("Index: %i, subindex: %i\n",k,z);
		
/*	for(k=0;;k++)
	{
		if((z=conf.getNextString(&str))!=ConfParser::EndOfFile)
		{
			switch(z)
			{
			case ConfParser::Section:
				printf("%i: getNextString: %i, %s\n",k,z,str.c_str());
				break;
			case ConfParser::KeyLine:
				{
					string key,value;
					conf.parseKeyLine(str,&key,&value);
					printf("%i: Key: %s, Value: %s\n",k,
					       key.c_str(),value.c_str());
				}
				break;
			default:
				break;
			}
		}
		else
			break;
	}*/
	
	return 0;
}
