#include <cstdlib>
#include <cstring>


#include "ObjectDictionary.h"
#include "CanHelpers.h"
#include "CanOpenMessages.h"

#include <stdio.h>

using namespace CanOpen;

int main(int ac,char**av)
{
	ObjectDictionary od;
	int k;
	int x;

	CobID cobID;
	memset(&cobID,0,sizeof(CanOpen::CobID));
	printf("1 CobID.cobID: %x\n",cobID.cobID);
	cobID.nodeID=0x33;
	printf("2 CobID.cobID: %x\n",cobID.cobID);
	cobID.pdoCobID.pdoNo=3;
	printf("2.5 CobID.cobID: %x\n",cobID.cobID);
	cobID.pdoCobID.pdoDirection=0x1;
	printf("3 CobID.cobID: %x\n",cobID.cobID);
//	cobID.functionCode=0x06;
//	printf("3 CobID.cobID: %x\n",cobID.cobID);
	x=0;
	CanHelpers::copyBits(&x,13,&cobID,3,16);
	printf("x: %x\n",x);
	printf("x>>7: %x\n",x>>10);
	
	printf("sizeof(CanOpen::CobID): %i\n",sizeof(CanOpen::CobID));
	printf("sizeof(CanOpen::SDO): %i\n",sizeof(CanOpen::SDO));
	printf("sizeof(CanOpen::PDO): %i\n",sizeof(CanOpen::PDO));
	
	printf("od.setNodeID(0x23);\n");
	od.setNodeID(0x23);
	
	for(k=1;k<5;k++)
	{
		printf("od.getRxPDO_COB_ID(%i): %x\n",k,od.getRxPDO_COB_ID(k));
	}

	for(k=1;k<5;k++)
	{
		printf("od.getTxPDO_COB_ID(%i): %x\n",k,od.getTxPDO_COB_ID(k));
	}
	
	printf("od.getNodeID(): %x\n",od.getNodeID());
	
	return 0;
}
