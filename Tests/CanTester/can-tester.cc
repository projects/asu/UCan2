#include <cstdlib>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include "CanExceptions.h"
#include "CanInterface.h"
#include "CanHelpers.h"
// --------------------------------------------------------------------------
static struct option longopts[] = {
	{ "help", no_argument, 0, 'h' },
	{ "read", no_argument, 0, 'r' },
	{ "write", required_argument, 0, 'w' },
	{ "rw", required_argument, 0, 'x' },
	{ "device", required_argument, 0, 'd' },
	{ "type", required_argument, 0, 't' },
	{ "verbose", no_argument, 0, 'v' },
	{ "baudrate", required_argument, 0, 'b' },
	{ "protocol", required_argument, 0, 'p' },
	{ "frame", required_argument, 0, 'f' },
	{ "msglen", required_argument, 0, 'l' },
	{ "id", required_argument, 0, 'i' },
	{ "no-wait", no_argument, 0, 'n' },
	{ "sleep", required_argument, 0, 's' },
	{ "max-read", required_argument, 0, 'm' },
	{ "max-write", required_argument, 0, 'e' },
	{ NULL, 0, 0, 0 }
};
// --------------------------------------------------------------------------
using namespace std;
using namespace CanOpen;
using namespace CanHelpers;
// --------------------------------------------------------------------------
enum Command
{
	cmdNOP,
	cmdRead,
	cmdWrite,
	cmdRW
} cmd;
// --------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	int optindex = 0;
	int opt = 0;
	string dev("/dev/can0");
	int data = 0;
	bool auto_data = false;
	cmd = cmdNOP;
	int verb = 0;
	CanInterface* can;
	CanType ctype = ctCanbus;
	int brate = 1000;
	bool canwait = true;
	unsigned long id = 1;
	int msecpause = 500000;
	SimpleCanDevice::CanFrameFormat ffmt = SimpleCanDevice::ffExt;
	SimpleCanDevice::CanProtocol prot = SimpleCanDevice::CAN_20B;
	int len = 8;
	int max_read = 300;
	int max_write = 1;
	
	
	while( (opt = getopt_long(argc, argv, "hrw:t:d:vb:i:s:p:f:l:x:m:e:",longopts,&optindex)) != -1 ) 
	{
		switch (opt) 
		{
			case 'h':	
				printf("-h|--help 		- this message\n");
				printf("[-w|--write] data      - write to can\n");
				printf("[-r|--read]            - read from can\n");
				printf("[-x|--rw] {data|auto}  - write and read\n");
				printf("[-m|--max-read] n - maximum number of reads (Default: %d)\n", max_read);
				printf("[-d|--device] dev	- use device dev. (Default: /dev/can0)\n");
				printf("[-t|--type] t		- can driver type. (Default: 3)\n");
				printf("    1 	- ArbraCan\n");
				printf("    2 	- PCI1680\n");
				printf("    3 	- Canbus\n");
				printf("    4 	- Can200\n");
				printf("\n");
				printf("[-b|--baudrate] val - set baudrate val Kbit.(Default: 1000 Kbit)\n");
				printf("[-p|--protocol] num - set protocol.(Default: CAN 2.B)\n");
				printf("    0 	- CAN 2.A\n");
				printf("    1 	- CAN 2.B\n");
				printf("\n");
				printf("[-f|--frame] val - set frame format.(Default: 11bit)\n");
				printf("    0 	- default\n");
				printf("    1 	- 11 bit\n");
				printf("    2 	- 29 Bit\n");
				printf("\n");
				printf("[--no-wait] - No waiting...\n");
				printf("[-i|--id] - set identifier message. Default: 1.\n");
				printf("[-s|--sleep] msec - send message pause. Default: 0.5 sec.\n");
				printf("[-l|--msglen] len - length of message [1....8]. Default: 8.\n");
				printf("[-v|--verbose] - verbose mode.\n");
				printf("\n");
			return 0;

			case 'r':	
				cmd = cmdRead;
			break;

			case 'w':
				data = atoi(optarg);
				cmd = cmdWrite;
			break;
			
			case 'e':
				max_write = atoi(optarg);
			break;

			case 'x':
				if (string(optarg).compare("auto"))
					data = atoi(optarg);
				else
					auto_data = true;
				cmd = cmdRW;
			break;

			case 'm':
				if (atoi(optarg) > 0)
					max_read = atoi(optarg);
			break;

			case 'd':	
				dev = optarg;
			break;

			case 't':	
				ctype = (CanType)atoi(optarg);
			break;

			case 'p':	
				prot = (SimpleCanDevice::CanProtocol)atoi(optarg);
			break;

			case 'f':	
				ffmt = (SimpleCanDevice::CanFrameFormat)atoi(optarg);
			break;

			case 'i':	
				id = atoi(optarg);
			break;

			case 'b':	
				brate = atoi(optarg);
			break;

			case 's':	
				msecpause = atoi(optarg);
			break;

			case 'l':	
				len = atoi(optarg);
			break;

			case 'n':	
				canwait = false;
			break;

			case 'v':	
				verb = 1;
			break;
			
			case '?':
			default:
				printf("? argumnet\n");
				return 0;
		}
	}
	
	
	if( cmd == cmdNOP )
	{
		cerr << "No command... Use -h for help" << endl;
		return -1;
	}

	try
	{
		can = new CanInterface(dev,ctype);
		can->setBaudRate(brate);
		can->setProtocol(prot);
		can->setWaiting(canwait);
		can->setFrameFormat(ffmt);
		can->setAcceptanceFilter(0xffffffff,0xffffffff);
//		can->resetController();

		if( verb )
		{
			fprintf(stdout, "init can:\n");
			fprintf(stdout, "dev: %s\n",dev.c_str());
			fprintf(stdout, "interface: %d\n",ctype);
			fprintf(stdout, "baudrate: %d\n",brate);
			fprintf(stdout, "protocol: %d\n",prot);
			fprintf(stdout, "frame: %d\n",ffmt);
		}

		switch( cmd )
		{
			case cmdRead:
			{
				int prev = -1;
				unsigned int count = 0;
				while(1)
				{
					Message msg;
					try
					{
						can->receiveMessage(&msg);
						cerr << "recv msg: " << msg << endl;
						if( msg.len > 0 )
						{
							if( msg.data[0] != prev + 1 )
								cerr << "expected " << prev + 1
									 << ", receive " << (int)msg.data[0] << endl;
							prev = (msg.data[0] != 255) ? msg.data[0] : -1;
							
							count++;
							cerr << "RECEIVE COUNT: " << count << endl;
						}
						else
						{
							cerr << "RECEIVE COUNT: " << count << endl;
							count = 0;
						}
					}
					catch( CanException& ex )
					{
						cerr << ex << endl;
						cerr << "RECEIVE COUNT: " << count << endl;
						count = 0;
					}
				}
			}
			break;
	
			case cmdWrite:
			{
				Message msg;

				msg.cob_id.w = id;
				msg.rtr	= 0;
				msg.len = len;
				for( int i=0; i<msg.len; i++ )
					msg.data[i] = (unsigned char)(data);

				int n=0;
				while(1)
				{
					msg.data[0] = n;
					try
					{
						for( int i=0; i<max_write; i++ )
						{
							cerr << "send msg: " << msg
								 << " res=" <<  can->sendMessage(&msg)
								 << endl;
						}
/*
						cerr << " read: ..." << endl;
						int res = can->receiveMessage(&rmsg);
						cerr << "read (res=" << res << " msg:	" << rmsg << endl;
*/
						++n;
					}
					catch( CanException& ex )
					{
						cerr << ex << endl;
					}

					usleep(msecpause);
				}
			}
			break;

			case cmdRW:
			{
				Message rmsg;
				Message msg;

				msg.cob_id.w = id;
				msg.rtr	= 0;
				msg.len = len;
				if( !auto_data )
					for( int i=0; i<msg.len; i++ )
						msg.data[i] = (unsigned char)(data);

				while(1)
				{
					int i = 0;
					while( i < 10 )
					{
						if( auto_data )
							for( int j=0; j<len; ++j )
								msg.data[j] = i + j;
						else
							msg.data[0] = i;
						try
						{
							can->sendMessage(&msg);
							cerr << "send msg: " << msg << endl;
							++i;
						}
						catch( CanException& ex )
						{
							cerr << ex << endl;
						}
					}
					int k = 0, read_num = max_read;
					while( read_num && k < 10 )
					{
						try
						{
							--read_num;
							can->receiveMessage(&rmsg);
							cerr << "read msg: " << rmsg << endl;
							if( auto_data )
								for( int j=1; j<len; ++j )
									if( rmsg.data[j] - rmsg.data[j - 1] != 1 )
									{
										cerr << "received bad data" << endl;
										break;
									}
							if( rmsg.data[0] == k )
							{
								++k;
							}
							else
							{
								cerr << "expected " << k << ", received "
								     << (int)rmsg.data[0] << endl;
								k = rmsg.data[0] + 1;
							}
							++read_num;
						}
						catch( CanException& ex )
						{
							cerr << ex << endl;
						}
					}
					cerr << endl;
					usleep(msecpause);
				}
			}
			break;

			default:
				cerr << endl << "Unknown command: '" << cmd << "'. Use -h for help" << endl;
				return -1;
			break;
		}
	}
	catch( CanException& ex )
	{
		cerr << ex << endl;
		return 1;
	}

	return 0;
}
// --------------------------------------------------------------------------
