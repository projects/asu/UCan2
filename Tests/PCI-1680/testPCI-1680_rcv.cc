// --------------------------------------------------------------------------
#include <string>
#include <iostream>
#include "CanExceptions.h"
#include "SimpleCanDevicePCI_1680.h"
// --------------------------------------------------------------------------
using namespace std;
using namespace CanOpen;
// --------------------------------------------------------------------------

int main(int ac,char**av)
{
	std::string dev;
	int i;
	Message msg;
	bool rcvd=0;
	
	if(ac>1)
	{
		dev=av[1];
	}
	else
	{
		dev="/dev/can0";
	}

	try
	{	
		SimpleCanDevicePCI_1680 canDev(dev);
		printf("Can device '%s' opened\n",dev.c_str());
	
		canDev.setBaudRate(1000);
		canDev.setWaiting(false);
		printf("receiving...\n");
		for(;;)
		{
			try
			{
				canDev.receiveMessage(&msg);
				rcvd=true;
			}
			catch(...)
			{
				rcvd=false;
//				printf("catch...\n");
			}
			
				if(rcvd)
				{
	//			printf("received\n");
				}

			if(rcvd)
			{
				printf("rcv: id: %04x; rtr: %i; data:",msg.cob_id.w,
				       msg.rtr);
			
				for(i=0;i<msg.len;i++)
				{
					printf(" %02x",msg.data[i]);
				}
				printf("\n");
			}
			else if(!rcvd)
			{
				usleep(500000);
			}
		}
	}
	catch( CanException& ex )
	{
		cerr << ex << endl;
		return 1;
	}
}
