#include <unistd.h>
#include <string>
#include <iostream>
#include "CanInterface.h"
#include "CanExceptions.h"
#include "Can.h"
#include "CanHelpers.h"

using namespace CanHelpers;

int main(int ac,char**av)
{
	std::string devName;
	if(ac>1)
		devName=av[1];
	else
		devName="/dev/can0";

	try
	{
		Message msg;
		CanInterface scDev(devName,ctArbra);
		int k;
	
		for(;;)
		{
			try
			{
				for(;;)
				{
					scDev.receiveMessage(&msg);
					printf("Rcv: ");
					for(k=0;k<sizeof(Message);k++)
					{
						printf("%02x ",((unsigned char*)&msg)[k]);
					}
					printf("\n");
					
					std::cout << "recv msg: " << msg << endl;
	//				scDev.processingReceivedMessage();
				}
			}
			catch( CanOpen::CanException& ex )
			{
				cerr << ex << endl;
			}
			catch(...)
			{
				printf("catch\n");
			}
	/*		int len;
			if(len=scDev.isImageReceived())
			{
				char*data=(char*)malloc(len);
				scDev.getReceivedImage(data);
				FILE*f=fopen("xxxx.jpg","w");
				fwrite(data,len,1,f);
				fclose(f);
					free(data);
			}*/
	
			usleep(10000);
		}
	}
	catch( CanOpen::CanException& ex )
	{
		cerr << ex << endl;
	}
	
	return 0;
}
	