
#include <unistd.h>
#include <string>
#include <iostream>


#include "CanOpenSlave.h"
#include "CanExceptions.h"
#include "SimpleCanDeviceArbracan.h"
#include "SimpleCanDeviceCanbus.h"

using namespace CanOpen;
using namespace std;

int main(int ac,char**av)
{
	int k;
	std::string str;
	std::string dev;
//	CanSlave slave("/dev/can1");

	if(ac>1)
		str=av[1];
	else
		str="STANDARD.EDS";


	if(ac>2)
		dev=av[2];
	else
		dev="/dev/can0";

//	SimpleCanDeviceArbracan device(dev);
	SimpleCanDeviceCanbus device(dev);
	CanOpenSlave slave(&device);
	

	slave.loadEDS(str);
	slave.setNodeID(0x4b);

	
	for(k=0;;k++)
	{
		try
		{
			slave.setIntValue(0x6000,0,k);
			slave.setIntValue(0x6000,1,k+1);
			slave.setIntValue(0x6000,2,k+2);
			slave.setIntValue(0x6000,3,k+3);
			slave.setIntValue(0x6000,4,k+4);
			slave.setIntValue(0x6000,5,k+5);
			slave.setIntValue(0x6000,6,k+6);
			slave.setIntValue(0x6000,7,k+7);

		
			slave.sendTxPDO(0x01);
			printf("SendTxPDO: ok\n");
//			printf()
			slave.setWaiting(false);
			slave.processingReceivedMessage();
			slave.setWaiting(true);
		}
		catch( CanException& ex )
		{
			cerr << ex << endl;
	//		return 1;
		}

		usleep(20000);
	}
	
	return 0;
}
