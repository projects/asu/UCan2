#! /bin/sh

set -x
aclocal || exit 1
autoheader || exit 1
libtoolize --copy || exit 1
automake --add-missing --include-deps --copy || exit 1
autoconf || exit 1
test -f Makefile && ./configure --enable-maintainer-mode --prefix=/usr
exit 0
