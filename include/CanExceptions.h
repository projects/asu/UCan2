
#ifndef _CAN_EXCEPTIONS_H_
#define _CAN_EXCEPTIONS_H_

#include <stdio.h>
#include <iostream>
#include <string>

namespace CanOpen
{
	class CanException
	{
	public:
		CanException():name("CanException"),text("")
		{
//			print();
		}
		CanException(std::string str):name("CanException"),text(str)
		{
//			print();
		}
		void print()
		{
			std::cout << name<<": "<< text << std::endl;
		}

		friend std::ostream& operator<<(std::ostream& os, CanException& ex )
		{
			return os << ex.name << ": " << ex.text;
		}

	protected:
		std::string name;
		std::string text;
	};


	class CanTimeOut:
		public CanException
	{
		public:
			CanTimeOut():CanException("TimeOut"){}
			CanTimeOut(std::string err):CanException(err){}
	};
	
	
	class SDOTransferActive:public CanException
	{
	public:
		SDOTransferActive()
		{
			name="SDOTransferActive";
			text="";
//			print();
		}
		SDOTransferActive(std::string str)
		{
			name="SDOTransferActive";
			text=str;
//			print();
		}
	};

	class ODEntryNotFound:public CanException
	{
	public:
		ODEntryNotFound(unsigned int index,unsigned int subindex)
		{
			char ix[10],sbix[10];
			sprintf(ix,"0x%04x",index);
			sprintf(sbix,"0x%02x",subindex);
			name="ODEntryNotFound";
			text="";
			text=text+"index: "+ix+"; subindex: "+sbix;
//			print();
		}
	private:
		ODEntryNotFound();
	};

	class ODValueNotAssigned:public CanException
	{
	public:
		ODValueNotAssigned(unsigned int index,unsigned int subindex)
		{
			char ix[10],sbix[10];
			sprintf(ix,"0x%04x",index);
			sprintf(sbix,"0x%02x",subindex);
			name="ODValueNotAssigned";
			text="";
			text=text+"index: "+ix+"; subindex: "+sbix;
//			print();
		}
	private:
		ODValueNotAssigned();
	};

	class IncompatibleTypes:public CanException
	{
	public:
		IncompatibleTypes(unsigned int type1,unsigned int type2)
		{
			char t1[10],t2[10];
			sprintf(t1,"0x%04x",type1);
			sprintf(t2,"0x%04x",type2);
			name="IncompatibleTypes";
			text="";
			text=text+t1+" and "+t2;
//			print();
		}
	private:
		IncompatibleTypes();
	};


	class UnexpectedSDOCommand:public CanException
	{
	public:
		UnexpectedSDOCommand(int cmd)
		{
			char ix[10];
			sprintf(ix,"0x%02x",cmd);
			name="UnexpectedSDOCommand";
			text="";
			text=text+"command: "+ix;
//			print();
		}
	private:
		UnexpectedSDOCommand();
	};

	class ObjectSizeIsVariable:public CanException
	{
	public:
		ObjectSizeIsVariable()
		{
//			this->type=type;
			name="ObjectSizeIsVariable";
			text="";
//			print();
		}
//	protected:
//		int type;
	};
};

#endif // _CAN_EXCEPTIONS_H_
