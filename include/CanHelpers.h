
#ifndef _CAN_HELPERS_H_
#define _CAN_HELPERS_H_

#include <sys/time.h>
#include <ostream>
#include "Can.h"

namespace CanHelpers
{
	timeval subTimeVals(timeval tv1,timeval tv2); // tv1 - tv2
	int cmpTimeVals(timeval tv1,timeval tv2);

	void copyBits(void*dstptr,int dstnbit,
		      void*srcptr,int srcnbit,int count);
	unsigned short calcCRC16(unsigned short crc,
				 unsigned char*ptr,int len);

	std::ostream& operator<<(std::ostream&, const Message& m);
	
}

#endif // _CAN_HELPERS_H_
