// $Id: CanInterface.h,v 1.4 2008/12/30 15:11:48 pv Exp $
// -------------------------------------------------------------------------
#ifndef _CAN_INTERFACE_H_
#define _CAN_INTERFACE_H_
// -------------------------------------------------------------------------
#include <string>
#include "SimpleCanDevice.h"
// -------------------------------------------------------------------------
enum CanType
{
	ctUnknown,
	ctArbra,
	ctPCI1680,
	ctCanbus,
	ctCan200
};
// -------------------------------------------------------------------------
class CanInterface
{
	public:
		CanInterface( const std::string device, CanType type );
		~CanInterface();

		void setBaudRate(int baudrate);
		void setAcceptanceFilter(int acceptanceCode, int acceptanceMask);
		void setWaiting(bool state);
		void setProtocol( SimpleCanDevice::CanProtocol prot );
		void setFrameFormat( SimpleCanDevice::CanFrameFormat fmt );
		
		int sendMessage(Message*msg);
		int receiveMessage(Message*msg);

		void resetController();

	protected:
		SimpleCanDevice* candev;

	private:
		CanInterface();
};
// -------------------------------------------------------------------------
#endif // _CAN_INTERFACE_H_
// -------------------------------------------------------------------------
