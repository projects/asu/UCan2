
#ifndef _CAN_OPEN_DEVICE_H_
#define _CAN_OPEN_DEVICE_H_

#include "SimpleCanDevice.h"

#include "CanOpenMessages.h"
#include "ObjectDictionary.h"

#include <string>

using namespace std;

class CanOpenDevice
{
public:
	CanOpenDevice(SimpleCanDevice*device);
	virtual ~CanOpenDevice();

	inline void setBaudRate(int baudrate)
	{
		simpleDevice->setBaudRate(baudrate);
	}
	inline void setAcceptanceFilter(int acceptanceCode, int acceptanceMask)
	{
		simpleDevice->setAcceptanceFilter(acceptanceCode,
						     acceptanceMask);
	}

	void processingReceivedMessage();

	void sendTxPDO(int pdoNo);
	void sendRxPDO(int pdoNo);

	void loadEDS(string edsFile);
	inline void setWaiting(bool state)
	{
		simpleDevice->setWaiting(state);
	}
	
	void setNodeID(int id);
	int getNodeID();

	// установка/получение 8-32-х битных значений в/из
	// Object Dictionary
	virtual int getIntValue(unsigned short index, unsigned char subindex);
	virtual void setIntValue(unsigned short index, unsigned char subindex,
				 int value);

	virtual bool getBitValue(unsigned short index, unsigned char subindex);
	virtual void setBitValue(unsigned short index, unsigned char subindex,
				 bool value);
	virtual CanOpen::DataType getDataType(unsigned short index,
					      unsigned char subindex);

	void setRxPDOCobID(int pdoNo,unsigned int cobID);
	void setTxPDOCobID(int pdoNo,unsigned int cobID);

protected:
	SimpleCanDevice*simpleDevice;
	CanOpen::ObjectDictionary od;

	unsigned char NMTtBit:1;
	CanOpen::NMTNodeState currentState;

	void mapRxPDOtoOD(CanOpen::PDO*pdo,CanOpen::ObjectDictionary*objD);
	void mapTxPDOtoOD(CanOpen::PDO*pdo,CanOpen::ObjectDictionary*objD);

	void assemblyRxPDO(int pdoNo,CanOpen::ObjectDictionary*objD,
			   CanOpen::PDO*pdo);
	void assemblyTxPDO(int pdoNo,CanOpen::ObjectDictionary*objD,
			   CanOpen::PDO*pdo);

	virtual void processingReceivedPDO(CanOpen::PDO*pdo)=0;
	virtual void processingReceivedSDO(CanOpen::SDO*sdo);

	virtual void processingNMT(CanOpen::NMTMessage nmt){};
	virtual void processingNMTNodeGuard(CanOpen::NMTGuardMessage*nmt);

	virtual void processingEmergencyObject
		(CanOpen::EmergencyObject*eObj){};
	virtual void checkSDOTimeouts(){};
	
	void sendSDO(CanOpen::SDO sdo, int direction);
	void sendPDO(CanOpen::PDO pdo, int pdoNo/*, int direction*/);

	void sendNMT(CanOpen::NMTGuardMessage nmt);
	void sendEmergencyObject(CanOpen::EmergencyObject eObj);
	
//	void processingReceivedSDO(CanOpen::SDO*sdo);
private:

};

#endif // _CAN_OPEN_DEVICE_H_
