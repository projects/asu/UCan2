
#ifndef _CAN_DEVICE_WITH_SDO_H_
#define _CAN_DEVICE_WITH_SDO_H_

#include "CanOpenDevice.h"
#include <string>
#include <queue>

#include "CanHelpers.h"

using namespace std;

class CanOpenDeviceWithSDO:public CanOpenDevice
{
public:
	CanOpenDeviceWithSDO(SimpleCanDevice*device);
//	~CanOpenDeviceWithSDO();

	virtual void processingReceivedSDO(CanOpen::SDO*sdo);

	// SDO
	// Download
	void clientSdoInitiateDomainDownload(int nodeID, unsigned short index,
					     unsigned char subindex,
					     unsigned int len,void*data);
	// Upload
	void clientSdoInitiateDomainUpload(int nodeID, unsigned short index,
					   unsigned char subindex);
	// If data received then returns data length
	// else 0 (zero)
	int clientSdoIsDataUploaded(int nodeID);
	// this function copy received data to ``data'',
	// delete received data and reset clientSDOUpload.isActive
	void clientSdoGetUploadedData(int nodeID,void*data);
protected:
	// SDO
	// Download
//	void clientSdoInitiateDomainDownload(int nodeID, unsigned short index,
//				     unsigned char subindex,
//				     unsigned int len,void*data);
	void clientSdoDownloadDomainSegment(CanOpen::SDO*rxSDO);

	void serverSdoInitiateDomainDownload(CanOpen::SDO*sdo);
	void serverSdoDownloadDomainSegment(CanOpen::SDO*sdo);
	// обработчик нестандартных SDO (слишком длинные данные и пр.)
	// по умолчанию ничего не делает
	void serverUnstandardDownloadSDO(CanOpen::SDO*sdo,void*data);

	// Upload
//	void clientSdoInitiateDomainUpload(int nodeID, unsigned short index,
//					   unsigned char subindex);
	void clientSdoInitiateDomainUploadAnswer(CanOpen::SDO*rxSDO);
	void clientSdoUploadDomainSegment(CanOpen::SDO*rxSDO);
	// функция, которя вызывается после получения (upload) данных
	// клиентом
	virtual void clientSdoOnDataUploaded(int nodeID){}
	// обработчик нестандартных SDO (слишком длинные данные и пр.)
	// по умолчанию ничего не делает
	virtual void clientUnstandardUploadSDO(CanOpen::SDO*sdo,void*data);

	virtual void checkSDOTimeouts();

	void serverSdoInitiateDomainUpload(CanOpen::SDO*sdo);
	void serverSdoUploadDomainSegment(CanOpen::SDO*sdo);
	// this function returns length (bytes) of unstandard data
	virtual int serverUnstandardSdoUpload(unsigned short index,
					      unsigned char subindex)
	{return 0;}
	virtual void serverSdoUploaded(unsigned short index,
				       unsigned char subindex){}
	virtual void serverSdoUploadDataAccepted(unsigned short index,
						 unsigned char subindex){}

	struct SDOTransfer
	{
		unsigned char isActive:1;
		unsigned char isSegmented:1;
		unsigned char isUnstandard:1;
		unsigned char t:1;
		unsigned char isDataReceived:1;
		unsigned int len;
		unsigned int currentPosition;
		unsigned short nodeID;
		unsigned short index;
		unsigned char subindex;
		void*data;
		timeval lastMsgTime;
		SDOTransfer()
		{
			isActive=false;
			data=NULL;
		}
	};
	static const timeval SDOTimeout;
	timeval prevSDOCheck;
	void checkSDOTimeout(SDOTransfer*sTrans);

	void checkSDOTimeout(map<int,SDOTransfer>*sTrans);
	
	// client's variables
	map<int,SDOTransfer> clientSDOUpload; // W!: index is NodeID
	SDOTransfer clientSDODownload;

	// server's variables
	SDOTransfer serverSDOUpload;
	std::queue<SDOTransfer> serverSDOUpQueue;
	SDOTransfer serverSDODownload;

	void initServerSDOUploadFromQueue();
private:

};


#endif // _CAN_DEVICE_WITH_SDO_H_
