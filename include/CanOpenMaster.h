
#ifndef _CAN_OPEN_MASTER_H_
#define _CAN_OPEN_MASTER_H_

#include "CanOpenDeviceWithSDO.h"
#include "ObjectDictionary.h"

#include <list>
#include <string>

class CanOpenMaster:public CanOpenDeviceWithSDO
{
public:
	CanOpenMaster(SimpleCanDevice*device);

	void sendSync();
	void sendNMT(int NodeID, CanOpen::NMTCommandSpecifier cs);
	void sendNodeGuard(int ID);

	void addSlave(int nodeID,std::string filename);
	std::list<int> getSlavesNodeIDs();
	
	int getSlavesIntValue(int nodeID, unsigned short index,
			      unsigned char subindex);
	void setSlavesIntValue(int nodeID, unsigned short index,
			       unsigned char subindex, int value);
	bool getSlavesBitValue(int nodeID, unsigned short index,
			      unsigned char subindex);
	void setSlavesBitValue(int nodeID, unsigned short index,
			       unsigned char subindex, bool value);
	CanOpen::DataType getSlavesDataType(int nodeID,
					    unsigned short index,
					    unsigned char subindex);
	struct SlaveState
	{
		unsigned char state;
		timeval timeStamp;
	};
	SlaveState getNodeState(int nodeID);
protected:
//	list<unsigned char> mySlaves;
	map<unsigned char,CanOpen::ObjectDictionary> slavesODs;
	map<unsigned char,SlaveState> slavesStates;
	
	map<int,int>myRxPDOCobID;
	map<int,int>myTxPDOCobID;

	virtual void processingReceivedPDO(CanOpen::PDO*pdo);
//	virtual void processingReceivedSDO(CanOpen::SDO*sdo);
	virtual void processingNMTNodeGuard(CanOpen::NMTGuardMessage*nmt);
};

#endif // _CAN_OPEN_MASTER_H_
