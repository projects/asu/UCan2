
#ifndef _CAN_OPEN_MESSAGES_H_
#define _CAN_OPEN_MESSAGES_H_

namespace CanOpen
{
	enum FunctionCodes
	{
		FcNMT=0x00,
		FcSYNC=0x01,
		FcEmergencyObject=0x01,
		FcTimeStamp=0x02,
		FcPDO1tx=0x03,
		FcPDO1rx=0x04,
		FcPDO2tx=0x05,
		FcPDO2rx=0x06,
		FcPDO3tx=0x07,
		FcPDO3rx=0x08,
		FcPDO4tx=0x09,
		FcPDO4rx=0x0a,
		FcSDOtx=0x0b,
		FcSDOrx=0x0c,
		FcNodeGuard=0x0e
	};
	
	enum SDOCommands
	{
		InitiateDomainDownloadRequest=0x01,
		InitiateDomainDownloadAnswer=0x03,
		DownloadDomainSegmentRequest=0x00,
		DownloadDomainSegmentAnswer=0x01,

		InitiateDomainUploadRequest=0x02,
		InitiateDomainUploadAnswer=0x02,
		UploadDomainSegmentRequest=0x03,
		UploadDomainSegmentAnswer=0x00,
		
		AbortDomainTransfer=0x04,
		
		InitiateBlockDownloadRequest=0x06,
		InitiateBlockDownloadAnswer=0x05,
		DownloadBlockSegmentAnswer=0x05,
		
		EndBlockDownloadRequest=0x06,
		EndBlockDownloadAnswer=0x05,

		InitiateBlockUploadRequest=0x05,
		InitiateBlockUploadAnswer=0x06,
		UploadBlockSegmentAnswer=0x06,
		
		EndBlockUploadRequest=0x06,
		EndBlockUploadAnswer=0x05
	};
	
	enum NMTCommandSpecifier
	{
		StartRemoteNode=1,
		StopRemoteNode=2,
		EnterPreoperationalState=128,
		ResetNode=129,
		ResetCommunication=130
	};
	
	enum NMTNodeState
	{
		Initialising=0,
		Disconnected=1,
		Connecting=2,
		Preparing=3,
		Stopped=4,
		Operational=5,
		PreOperational=127
	};
	
	enum AbortCodes
	{
		TBitError=			0x05030000,
		SDOTimeOut=			0x05040000,
		InvalidCommandSpec=		0x05040001,
		InvalidBlockSize=		0x05040002,
		InvalidSequenceNumber=		0x05040003,
		CRCError=			0x05030004, // так в книжке
		OutOfMemory=			0x05030005,
		UnsupportedAccess=		0x06010000,
		ReadOfWO=			0x06010001,
		WriteOfRO=			0x06010002,
		ObjectDoesnotExist=		0x06020000,
		CannotMapObject=		0x06040041,
		NumberOfMappedObjectsExceed=	0x06040042,
		ParameterIncompatibility=	0x06040043,
		InternalIncompatibility=	0x06040047,
		HardwareAccessError=		0x06060000,
		DataLengthDoesnotMatch=		0x06060010,
		DataLengthTooBig=		0x06060012,
		DataLengthTooSmall=		0x06060013,
		SubindexDoesnotExist=		0x06090011,
		OutOfRange=			0x06090030,
		OutOfRangeHi=			0x06090031,
		OutOfRangeLow=			0x06090032,
		MaxLessMin=			0x06090036,
		GeneralError=			0x08000000,
		CannotStoreData=		0x08000020,
		CannotStoreData_Control=	0x08000021,
		CannotStoreData_State=		0x08000022,
		ODGenerationFailed=		0x08000023
	};

	union CobID
	{
		struct
		{
			unsigned short nodeID:7;
			unsigned short functionCode:4;
		}__attribute__((packed));
		struct
		{
			unsigned short nodeID:7;
			unsigned short pdoDirection:1; // 1 - Tx; 0 - Rx
			unsigned short pdoNo:3;
		}__attribute__((packed)) pdoCobID;
		unsigned short cobID;
	}__attribute__((packed));

	struct SDO
	{
		CobID cobID;
		struct
		{
			union
			{
				struct
				{
					unsigned char s:1;
					unsigned char e:1;
					unsigned char n:2;
					unsigned char :1;
					unsigned char command:3;
				}__attribute__((packed))init;
				struct
				{
					unsigned char c:1;
					unsigned char n:3;
					unsigned char t:1;
					unsigned char command:3;
				}__attribute__((packed))segment;
				unsigned char commandSpec;
			}__attribute__((packed)) commandSpec;
			union
			{
				struct
				{
					unsigned short index;
					unsigned char subindex;
					union
					{
						unsigned char data[4];
						unsigned int length;
					}__attribute__((packed));
				}__attribute__((packed)) init;
				struct
				{
					unsigned char data[7];
				}__attribute__((packed)) segment;
			}__attribute__((packed));
		}__attribute__((packed)) body;
	}__attribute__((packed));
	
	struct PDO
	{
//		unsigned short ID;	//Node ID
		CobID cobID;
		char len;		//body len
		char data[8];
	}__attribute__((packed));

	struct NMTMessage
	{
		CobID cobID;
//		unsigned char len;
		unsigned char cs;
		unsigned char nodeID;
	}__attribute__((packed));

	struct NMTGuardMessage
	{
		CobID cobID;
		unsigned char rtr;
//		unsigned char len;
		union
		{
			struct
			{
				unsigned char t:1;
				unsigned char state:7;
			};
			unsigned char byte0;
		};
	}__attribute__((packed));

	struct EmergencyObject
	{
		CobID cobID;
		struct
		{
			unsigned short errorCode;
			unsigned char errorRegister;
			unsigned char msData[5];
		}__attribute__((packed)) body;
	}__attribute__((packed));
};

#endif // _CAN_OPEN_MESSAGES_H_
