
#ifndef _CAN_OPEN_SLAVE_H_
#define _CAN_OPEN_SLAVE_H_

#include "CanOpenDevice.h"

#include "CanOpenMessages.h"

class CanOpenSlave:public CanOpenDevice
{
public:
	CanOpenSlave(SimpleCanDevice*device);
	~CanOpenSlave();

protected:
	CanOpen::NMTNodeState currentState;
	bool nmtTBit;

	void sendNodeGuard(int nodeID,CanOpen::NMTNodeState state);
	virtual void processingReceivedPDO(CanOpen::PDO*pdo);
//	virtual void processingReceivedSDO(CanOpen::SDO*sdo);

//	void assemlyPDOforSend(int pdoNo, CanOpen::PDO*pdo);

	virtual void processingNMT(CanOpen::NMTMessage nmt);
	virtual void processingNMTNodeGuard(CanOpen::CobID cobID);

};

#endif // _CAN_OPEN_SLAVE_H_
