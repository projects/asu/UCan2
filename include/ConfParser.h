
#ifndef _CONF_PARSER_H_
#define _CONF_PARSER_H_

#include <string>
#include <stdio.h>

class ConfParser
{
public:
	ConfParser(std::string filename);
	~ConfParser();

	enum StringType
	{
		Section,
		KeyLine,
		EndOfFile
	};
	StringType getNextString(std::string*str);
	void parseKeyLine(std::string str,std::string*key,std::string*value);
protected:
	FILE*confFile;
	std::string getNextSignificantLine();
private:
	bool checkSignificanceOfLine(std::string str);
	ConfParser();

};

#endif // _CONF_PARSER_H_
