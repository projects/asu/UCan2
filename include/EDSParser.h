
#ifndef _EDS_PARSER_H_
#define _EDS_PARSER_H_

#include "ConfParser.h"
#include "ObjectDictionary.h"

#include <string>
#include <stdio.h>

class EDSParser:public ConfParser
{
public:
	EDSParser(std::string filename);
	void fillOD(CanOpen::ObjectDictionary*od);
protected:
	enum SectionType
	{
		NoSection,
		StringSection,
		IndexSection,
		IndexSubindexSection
		
	};
	
	CanOpen::ObjectDictionaryEntry*odEntry;
//	CanOpen::ObjectDescription odDesc;
	CanOpen::ObjectDictionary*curOD;

	std::string currentSection;
	SectionType currentSectionType;
	int subNumber;
	bool isDefaultValue;
	
	struct ParserFunctions
	{
		void(EDSParser::*secFunction)(std::string);
		void(EDSParser::*keyFunction)(std::string);
	};
	
	map<std::string,ParserFunctions>pFunctions;
	
	void processSectionName(std::string sName);
	void processEndOfSection();
	void processKeyLine(std::string str);
	
	void processFileInfo(std::string sName);
	void processDeviceInfo(std::string sName);
	void processStandardDataTypes(std::string sName);
	void processDummyUssage(std::string sName);
	void processMandatoryObjects(std::string sName);
	void processOptionalObjects(std::string sName);
	void processManufacturerObjects(std::string sName);

	void processFileInfoKey(std::string keyLine);
	void processDeviceInfoKey(std::string keyLine);
	void processStandardDataTypesKey(std::string keyLine);
	void processDummyUssageKey(std::string keyLine);
	void processMandatoryObjectsKey(std::string keyLine);
	void processOptionalObjectsKey(std::string keyLine);
	void processManufacturerObjectsKey(std::string keyLine);

	void processIndexSection(std::string keyLine);
	void processIndexSectionKey(std::string keyLine);
	void processIndexSubindexSectionKey(std::string keyLine);

	void processUnknownSection(std::string sName);

	int setValue(std::string str,CanOpen::DataType dType,void**value);
private:
	EDSParser();
};

#endif // _EDS_PARSER_H_
