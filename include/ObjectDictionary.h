
#ifndef _OBJECT_DICTIONARY_H_
#define _OBJECT_DICTIONARY_H_

#include <string>
#include <map>

using namespace std;

namespace CanOpen
{
	enum ObjectType
	{
		domain=2,
		deftype=5,
		defstruct=6,
		var=7,
		array=8,
		record=9
	};
	
	enum DataType
	{
		Boolean=0x0001,
		Integer8=0x0002,
		Integer16=0x0003,
		Integer32=0x0004,
		Unsigned8=0x0005,
		Unsigned16=0x0006,
		Unsigned32=0x0007,
		Real32=0x0008,
		VisibleString=0x0009,
		OctetString=0x000a,
		UnicodeString=0x000b,
		TimeOfDay=0x000c,
		TimeDifference=0x000d,
		BitString=0x000e,
		Domain=0x000f,
		Integer24=0x0010,
		Real64=0x0011,
		Integer40=0x0012,
		Integer48=0x0013,
		Integer56=0x0014,
		Integer64=0x0015,
		Unsigned24=0x0016,
		Unsigned40=0x0018,//(sic!)
		Unsigned48=0x0019,
		Unsigned56=0x001a,
		Unsigned64=0x001b
	};
	
	enum Attribute
	{
		rw,wo,ro,con
	};

	const int MaxPDONum=512;
	
	struct ObjectDictionaryEntry
	{
		ObjectDictionaryEntry()
		{
//			memset(this,0,sizeof(ObjectDictionaryEntry));
			size=0;
			value=NULL;
			defaultSize=0;
			defaultValue=NULL;
			objectName="";
			minimum=0;
			maximum=0;
			dataType=(DataType)0;
		}
		unsigned short index;
		unsigned char subindex;
		ObjectType objectType;
		string objectName;
		DataType dataType;
		Attribute attr;
		bool isPDOmap;
		unsigned int minimum;
		unsigned int maximum;
		int defaultSize;
		void*defaultValue;
		int size;
		void*value;
	};

	struct ObjectDescription
	{
		unsigned short index;
		int subNumber;
		ObjectType objectType;
		string objectName;
		DataType dataType;
		Attribute attr;
		bool isPDOmap;
	};

	
#warning Needed checking of byte order!!!!!!!
	struct PDOMapObject
	{
		union
		{
			struct
			{
				unsigned char numBits;
				unsigned char subIndex;
				unsigned short index;
			};
			unsigned int mapObject;
		};
	}__attribute__((packed));
		
	
	class ObjectDictionary
	{
	public:
		ObjectDictionary();
		void addEntry(ObjectDictionaryEntry entry);
		void deleteEntry(unsigned short index,
				 unsigned char subindex);
		void setValue(unsigned short index,
			      unsigned char subindex,
			      void*value,int size=0);

		void setObjectDescription(unsigned short index,
					  ObjectDescription desc);
		
		int getValueBitSize(unsigned short index,
				    unsigned char subindex);
		int getDataSize(unsigned short index,
				unsigned char subindex);
		void getValue(unsigned short index, unsigned char subindex,
			      void*value);
		DataType getDataType(unsigned short index,
				     unsigned char subindex);

		// Device configuration related functions
		
		void setNodeID(int nodeID);
		int getNodeID();
		
		// PDO functions


		unsigned int findTxPDONoByCobID(unsigned int cobID);
		unsigned int findRxPDONoByCobID(unsigned int cobID);

		// pdoNo [1..512]
		void setRxPDO_COB_ID(int pdoNo,unsigned int cobID);
		unsigned int getRxPDO_COB_ID(int pdoNo);
		unsigned int getRxPDOsNumberOfMappedObjects(int pdoNo);
		// objNo [0..255]
	        PDOMapObject getRxPDOsMappedObject(int pdoNo, int objNo);

		void setTxPDO_COB_ID(int pdoNo,unsigned int cobID);
		unsigned int getTxPDO_COB_ID(int pdoNo);
		unsigned int getTxPDOsNumberOfMappedObjects(int pdoNo);
		PDOMapObject getTxPDOsMappedObject(int pdoNo, int objNo);
		
		// SDO functions
		unsigned int getRxSDO_COB_ID(); // client 2 server
		unsigned int getTxSDO_COB_ID(); // server 2 client

		enum ODMainIndices
		{
			NodeID=0x100b,
				// ... skipped
			SDOServerParm=0x1200,
			RxPDOParm=0x1400,
			RxPDOMapping=0x1600,
			TxPDOParm=0x1800,
			TxPDOMapping=0x1a00
		};
		
		void printAll();
		int typeSize(DataType dataType);

		union ODIndices
		{
			struct 
			{
				unsigned char subindex;
				unsigned short index;
			}__attribute__((packed));
			unsigned int indices;
			ODIndices(){indices=0;}
		}__attribute__((packed));
	protected:
		enum DataSizes
		{OneBit=0x100,Variable=0x101,Unknown=-1};
		
//		map<ODIndices,ObjectDictionaryEntry> dictionary;
		map<unsigned int,ObjectDictionaryEntry> dictionary;
		map<unsigned short,ObjectDescription> objDescription;
		map<DataType,int>dataSize;
		ObjectDictionaryEntry*getEntry(unsigned short index,
					       unsigned char subindex);
	private:
		ODIndices lastIdx;
		ObjectDictionaryEntry*lastEntry;
	};
}

#endif // _OBJECT_DICTIONARY_H_
