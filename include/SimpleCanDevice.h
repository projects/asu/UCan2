
#ifndef _SIMPLE_CAN_DEVICE_H_
#define _SIMPLE_CAN_DEVICE_H_

#include "Can.h"

//#include "CanOpenMessages.h"
//#include "ObjectDictionary.h"

#include <string>

using namespace std;

class SimpleCanDevice
{
public:
	SimpleCanDevice(/*string device*/);
	virtual ~SimpleCanDevice();

	virtual void setBaudRate(int baudrate);
	virtual void setAcceptanceFilter(int acceptanceCode, int acceptanceMask);

	enum CanProtocol
	{
		CAN_20A = 0,
		CAN_20B	= 1
	};

	virtual void setProtocol(CanProtocol prot);

	enum CanFrameFormat
	{
		ffDef 	= 0,	// default
		ffStd	= 1, 	// 11 bit
		ffExt	= 2 	// 29 bit
	};

	virtual void setFrameFormat( CanFrameFormat ffmt );
	
	void setWaiting( bool state );
	
	virtual void resetController()=0;
	
	virtual int sendMessage(Message*msg)=0;
	virtual int receiveMessage(Message*msg)=0;
	
	inline void afterSendPause( int msec ){  sendPause_usec = msec*1000; }
	
protected:
//	int fd;
	bool waiting;

	AcceptanceFilter accFilter;
	int baudRate;
	CanProtocol protocol;
	CanFrameFormat ffmt;
	
	int sendPause_usec;

private:
//	SimpleCanDevice();
};


#endif // _SIMPLE_CAN_DEVICE_H_
