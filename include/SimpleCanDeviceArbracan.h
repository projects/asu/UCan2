
#ifndef _SIMPLE_CAN_DEVICE_ARBRACAN_H_
#define _SIMPLE_CAN_DEVICE_ARBRACAN_H_

#include "Can.h"
#include "SimpleCanDevice.h"

//#include "CanOpenMessages.h"
//#include "ObjectDictionary.h"

#include <string>

using namespace std;

class SimpleCanDeviceArbracan:
public SimpleCanDevice
{
public:
	SimpleCanDeviceArbracan(string device);
	virtual ~SimpleCanDeviceArbracan();

	virtual void setBaudRate(int baudrate);
	virtual void setAcceptanceFilter(int acceptanceCode, int acceptanceMask);

//	void processingReceivedMessage();

//	void setWaiting(bool state);

	virtual void resetController();

	virtual int sendMessage(Message*msg);
	virtual int receiveMessage(Message*msg);
protected:
	int fd;
//	bool waiting;

	void clearTxBuffer();
	void clearRxBuffer();
private:
	SimpleCanDeviceArbracan();
};


#endif // _SIMPLE_CAN_DEVICE_ARBRACAN_H_
