// -------------------------------------------------------------------------
#ifndef _SIMPLE_CAN_DEVICE_CAN200_H_
#define _SIMPLE_CAN_DEVICE_CAN200_H_
// -------------------------------------------------------------------------
#include <string>
#include <queue>
#include <sys/time.h>
#include "can_codes.h"
#include "Can.h"
#include "SimpleCanDevice.h"
// -------------------------------------------------------------------------
class SimpleCanDeviceCan200:
public SimpleCanDevice
{
	public:
		SimpleCanDeviceCan200( std::string device );
		~SimpleCanDeviceCan200();

		void setFrameFormat( CanFrameFormat ffmt );

		void setBaudRate( int baudrate );
		void setAcceptanceFilter( int acceptanceCode, int acceptanceMask );

		int sendMessage( Message *msg );
		int receiveMessage( Message *msg );

		void resetController();

		void getConfig( CAN_info_t *info );
		void getStatus( unsigned char *status );
		void setBusOn();

	protected:
		int rfd;  // a file descriptor for reading
		int wfd;  // a file descriptor for writing
		int readData( CAN_data *data, unsigned int count );
		void init();
		std::string dev;
		std::queue<CAN_data> buffer;

	private:
		SimpleCanDeviceCan200();
};
// -------------------------------------------------------------------------
#endif // _SIMPLE_CAN_DEVICE_CAN200_H_
// -------------------------------------------------------------------------
