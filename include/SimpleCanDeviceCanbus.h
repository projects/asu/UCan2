// $Id: SimpleCanDeviceCanbus.h,v 1.3 2008/12/30 15:11:48 pv Exp $
// -------------------------------------------------------------------------
#ifndef _SIMPLE_CAN_DEVICE_CANBUS_H_
#define _SIMPLE_CAN_DEVICE_CANBUS_H_
// -------------------------------------------------------------------------
#include <string>
#include <queue>
#include "Can.h"
#include "SimpleCanDevice.h"
// -------------------------------------------------------------------------
class SimpleCanDeviceCanbus:
public SimpleCanDevice
{
	public:
		SimpleCanDeviceCanbus( std::string device);
		~SimpleCanDeviceCanbus();

		void setFrameFormat( CanFrameFormat ffmt );

		void setBaudRate( int baudrate );
		void setAcceptanceFilter( int acceptanceCode, int acceptanceMask );
		void setProtocol(CanProtocol prot);

		int sendMessage(Message*msg);
		int receiveMessage(Message*msg);

		void resetController();
	protected:
		int fd;
		int ff; // frame format
		void setBlocking( bool blocking );
		void reopen();
		std::string dev;
		
		std::queue<Message> buffer;
		
		bool wait;

	private:
		SimpleCanDeviceCanbus();

};
// -------------------------------------------------------------------------
#endif // _SIMPLE_CAN_DEVICE_CANBUS_H_
// -------------------------------------------------------------------------
