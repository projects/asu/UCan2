
#ifndef _SIMPLE_CAN_DEVICE_PCI_1680_H_
#define _SIMPLE_CAN_DEVICE_PCI_1680_H_

#include "Can.h"
#include "SimpleCanDevice.h"

//#include "CanOpenMessages.h"
//#include "ObjectDictionary.h"

#include <string>

using namespace std;

class SimpleCanDevicePCI_1680:
public SimpleCanDevice
{
public:
	SimpleCanDevicePCI_1680(string device);
	~SimpleCanDevicePCI_1680();

	void setBaudRate(int baudrate);
	void setAcceptanceFilter(int acceptanceCode, int acceptanceMask);

	int sendMessage(Message*msg);
	int receiveMessage(Message*msg);

	void resetController();
protected:
	int fd;

	void setBlocking(bool blocking);

	void checkIOCTL(int res);
	void throwErrno(int res, const char* checkErr);
private:
	SimpleCanDevicePCI_1680();
};


#endif // _SIMPLE_CAN_DEVICE_PCI_1680_H_
