%define oname UCan2

Name: libUCan2
Version: 0.4
Release: eter31

Summary: UCan2
Summary(ru_RU.UTF-8): Библиотека для работы с CAN

License: GPL
Group: System/Libraries

URL: http://sourceforge.net/ucan2

Packager: Pavel Vainerman <pv@altlinux.ru>

Source: /var/ftp/pvt/Etersoft/Ourside/mb745/sources/%name-%version.tar

# Automatically added by buildreq on Mon Mar 27 2006
BuildRequires: gcc-c++ libstdc++-devel canbus4linux-devel

%description
This package provides libraries to use UCan.

%description -l ru_RU.UTF-8
Библиотека содержит в себе универсальные интерфейсы для работы с CAN.

%package devel
Group: Development/C++
Summary: Libraries needed to develop for %name
Requires: %name = %version-%release

%description devel
Libraries needed to develop for UCan.

%description devel -l ru_RU.UTF-8
Библиотеки, требуемые для разработки с UCan.

%package -n %oname-utils
Summary: utils for test UCan
Group: Development/Tools
Requires: %name = %version-%release

%description -n %oname-utils
Utils for test UCan

%prep
%setup -q

%build
%autoreconf
%configure
%make_build

%install
%makeinstall_std

%files
%_libdir/*.so*

%files devel
%_includedir/%name/
%_pkgconfigdir/*
#%_docdir/%name-%version

%files -n %oname-utils
%_bindir/can-tester

%changelog
* Wed Aug 17 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.4-eter30
- bump release due simple backprorts rebuild
- change spec ecnoding from koi8-r to utf-8

* Mon Apr 18 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.4-eter29
- merge 29bit branch
- fix SimpleCanDevicePCI_1680 for new advcan-2.10 driver support

* Fri Mar 04 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-eter28.29bit
- new build

* Thu Mar 03 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-eter27.29bit
- new version

* Wed Mar 02 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-eter26.29bit
- test build (add buffer for canbus interface)

* Wed Mar 02 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-eter25.29bit
- new build

* Sat Feb 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-eter24.29bit
- fixed bug in initialization Canbus device interface

* Sat Feb 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-eter23.29bit
- new build

* Wed Mar 17 2010 Ilya Shpigor <elly@altlinux.org> 0.4-eter21
- new build

* Tue Dec 08 2009 Ilya Shpigor <shpigor@etersoft.ru> 0.4-eter20
- fix stack smashing errors

* Mon Sep 07 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-eter12
- new build

* Mon Sep 07 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-eter11
- new build

* Mon Sep 07 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-eter10
- new build

* Sat Sep 05 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-setri9
- add 2 can-tester print errno

* Sat Sep 05 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-setri8
- new build

* Sat Sep 05 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-setri6
- new build

* Fri Sep 04 2009 Pavel Vainerman <pv@etersoft.ru> 0.4-setri5
- fixed bug in recv message

* Tue Jul 14 2009 Vitaly Lipatov <lav@altlinux.ru> 0.3-eter20
- cleanup spec

* Sun Apr 05 2009 Pavel Vainerman <pv@altlinux.ru> 0.3-setri19
- new build

* Fri Feb 06 2009 Pavel Vainerman <pv@server> 0.3-setri18
- new build

* Fri Dec 19 2008 Pavel Vainerman <pv@maxselect> 0.3-setri4
- new interfaces

* Thu Oct 18 2007 Pavel Vainerman <pv@altlinux.ru> 0.2-setri1
- new version

* Thu Oct 18 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-setri6
- remove assert`s

* Fri Sep 14 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-setri5
- remove debug printf

* Wed Sep 05 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-setri4
- new build

* Wed Aug 29 2007 guest@stand 0.1-setri3
- new build

* Sat Jul 07 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-setri0.C30.1
- buld for C30

* Sat Jul 07 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-setri1
- new build

* Sat Jul 07 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-alt1
- new build

* Sat Jul 07 2007 Pavel Vainerman <pv@altlinux.ru> 0.1-alt0.1
- first build

