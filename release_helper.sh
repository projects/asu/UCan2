#!/bin/sh

ETERBUILDVERSION=163
# ��������������� ������ ��� ���������� � ������ rpm-������ � ��������
. /usr/share/eterbuild/eterbuild
load_mod spec

SPECNAME=libUCan2.spec
REL=eter
MAILDOMAIN=server
RPMBINDIR=$RPMDIR/RPMS/$DEFAULTARCH
FTPDIR=/var/ftp/pub/Ourside/i586/RPMS.UCan2
PROJECT=libUCan2
GEN=/var/ftp/pub/Ourside/i586/genb.sh
BACKUPDIR=$FTPDIR/backup

fatal()
{
	echo "Error: $@"
	exit 1
}

function send_notify()
{
	export EMAIL="$USER@$MAILDOMAIN"
	CURDATE=`date`
	MAILTO="devel@$MAILDOMAIN"
# FIXME: �������� ��������
mutt $MAILTO -s "[$PROJECT] New build: $BUILDNAME" <<EOF
������ ����� ������: $BUILDNAME
-- 
your $0
$CURDATE
EOF
echo "inform mail sent to $MAILTO"
}

function cp2ftp()
{
	mkdir -p $BACKUPDIR
	mv -f $FTPDIR/*UCan2* $BACKUPDIR/
	mv -f $RPMBINDIR/*UCan2* $FTPDIR/
	chmod 'a+rw' $FTPDIR/$PROJECT*
	$GEN
}

# Run inside project dir (named as name) (arg: local for noncvs build)
prepare_tarball()
{
	build_rpms_name $SPECNAME

	NAMEVER=$BASENAME-$VERSION
	WDPROJECT=$(pwd)
	TARNAME=$NAMEVER.tar
	DESTDIR=$TMPDIR/$NAMEVER
	RET=0

	mkdir -p $DESTDIR
	rm -rf $DESTDIR/*
	cp -r $WDPROJECT/* $DESTDIR/
	cd 	$DESTDIR/
		make distclean
		[ -a ./autogen.sh ] && ./autogen.sh
		rm -rf autom4te.cache/
		rm -rf .git
		rm -rf .gear
	
		echo "Make tarball $TARNAME ... from $DESTDIR"
		mkdir -p $RPMSOURCEDIR/
		$NICE tar cf $RPMSOURCEDIR/$TARNAME ../$NAMEVER $ETERTARPARAM || RET=1
		rm -rf $DESTDIR
	cd -

	[ $RET ] && echo "build ok" || fatal "Can't create tarball"
}

add_changelog_helper()
{
    tty -s || { echo "skip changelog fixing" ; return 1 ; }

	#FIXME HACK!! ���������� ������ ������ POSIX, ����� ���� � changelog
	# ���������� �� ����. �����. ����� ţ �� ���������� rpmbb
	L="$LC_ALL"
	export LC_ALL=POSIX
	add_changelog -e "$@"
	R=$?
	export LC_ALL="$L"

	if [ "$R" = "0" ]; then
		shift
		for SPEC in "$@" ; do
			N=`grep -n '^%changelog' $SPEC | head -n 1 | sed s!:.*!!g`
			# +1 -- comment with date and packager name
			# +2 -- place for edit comments
			vim +$(($N + 2)) $SPEC
		done
	fi
	return $R
}

# ------------------------------------------------------------------------

add_changelog_helper "- new build" $SPECNAME

#prepare_tarball || fatal "Can't prepare tarball"

rpmbb $SPECNAME || fatal "Can't build"

cp2ftp

rpmbs $SPECNAME

#send_notify

# ����������� ����� � ���������� ���� ����� �������� ������
inc_release $SPECNAME
#cvs commit -m "Auto updated by $0 for $BUILDNAME" $SPECNAME || fatal "Can't commit spec"

# Note: we in topscrdir
#TAG=${BUILDNAME//./_}
#echo "Set tag $TAG ..."
#cvs tag $TAG || fatal "Can't set build tag"
